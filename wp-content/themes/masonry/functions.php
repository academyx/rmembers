<?php
/**
 * Masonry functions and definitions
 *
 * @package Masonry
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

define('__DIR_INC___',get_stylesheet_directory_uri()."/inc");


function rh_user_login()
{
   set_session_cookie();
   die;
}

add_action('wp_ajax_user_login', 'rh_user_login');


function redirect_login_page() {
  
  session_start();	
  

  if($_REQUEST['logout']){

	session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);

	wp_clear_auth_cookie();
	wp_logout();


	wp_redirect_mgs("http://members.ibusiness.co.kr/","로그아웃되었습니다.");
	exit;
  }

  if( !is_super_admin() ){
    if( get_current_user_id() && !$_COOKIE['rh_user_login'] ){
	if( !$_COOKIE['rh_user_login'] ){
		wp_clear_auth_cookie();
		wp_logout();
		home_url( '/' );
		exit;
	}else{
		set_session_cookie();
	}
 	
  }

  }

  $login_page  = home_url( '/login' );
  $page_viewed = basename($_SERVER['REQUEST_URI']);
  $url_array = explode("/",$_SERVER['REQUEST_URI']); 

	if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		$login_page = $login_page."?redirect_to=".urlencode($_REQUEST[redirect_to]);
		wp_redirect($login_page);
		exit;
	  } 
  
}
add_action('init','redirect_login_page');


function rh_add_query_vars($aVars) {
$aVars[] = "msds_pif_cat"; // represents the name of the product category as shown in the URL
return $aVars;
}
 
// hook add_query_vars function into query_vars
add_filter('query_vars', 'rh_add_query_vars');

function rh_add_rewrite_rules($aRules) {
	$aNewRules = array('/members/([a-z]+)/?$' => 'index.php?pagename=members&sub=$matches[1]');
	$aRules = $aNewRules + $aRules;
	return $aRules;
}
 
// hook add_rewrite_rules function into rewrite_rules_array
add_filter('rewrite_rules_array', 'rh_add_rewrite_rules');


add_filter(
    'page_template',
    function ($template) {
        global $post;
		
        if ($post->post_parent) {

            // get top level parent page
            $parent = get_post(
               reset(array_reverse(get_post_ancestors($post->ID)))
            );

            // or ...
            // when you need closest parent post instead
            // $parent = get_post($post->post_parent);

            $child_template = locate_template(
                [
                    $parent->post_name . '/page-' . $post->post_name . '.php',
                    $parent->post_name . '/page-' . $post->ID . '.php',
                    $parent->post_name . '/page.php',
                ]
            );

            if ($child_template) return $child_template;
        }
        return $template;
    }
);

function login_failed() {
  $login_page  = home_url( '/login' );
  $page_viewed = basename($_SERVER['REQUEST_URI']);
  if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
	$login_page = $login_page."?redirect_to=".urlencode($_REQUEST[redirect_to]);
	wp_redirect( $login_page);
	exit;
  }  
}
add_action( 'wp_login_failed', 'login_failed' );
 
function verify_username_password( $user, $username, $password ) {
  $queryString = "";

  foreach( $_GET as $key=>$p ){
	$queryString .= $key ."=".$p."&";
  }
  
  $login_page  = home_url( '/login' );
    if( $username == "" || $password == "" ) {
		$login_page = $login_page."?".$queryString;
        wp_redirect( $login_page);
        exit;
    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);
add_filter('show_admin_bar', '__return_false');




function frm_allow_multiple_email($errors, $field, $value){
  if(isset($_POST) and isset($_POST['frm_register']) and !empty($value) and isset($errors['field'.$field->id])){
    if($errors['field'.$field->id] == 'This email address is already registered.')
       unset($errors['field'.$field->id]);
  }
  return $errors;
}
add_filter('frm_validate_field_entry', 'frm_allow_multiple_email', 30, 3);



function rh_meta_display($mode , $value){
	$returnValue = $value;
	if(!$value){
		return $value;
	}
	switch($mode){
		case "birth":
			$returnValue = date("Y-m-d" , strtotime($value));
		break;
		case "phone":

			$value = str_replace("-","",$value);
		if( strlen($value) == 8 ){
			$returnValue = substr($value,0,4)."-". substr($value,4,4);
		}else if( substr($value,0,2) != '01' ){
			if( substr($value,0,2) == '02'){
				$returnValue = substr($value,0,2)."-". substr($value,3,3)."-". substr($value,6,4);

				if( strlen($value) == 10 ){
					$returnValue = substr($value,0,2)."-". substr($value,2,4)."-". substr($value,6,4);
				}else{
					$returnValue = substr($value,0,2)."-". substr($value,2,3)."-". substr($value,5,4);
				}

			}else if( substr($value,0,3) == '050' || substr($value,0,3) == '070' || substr($value,0,3) == '030'){

				if( strlen($value) == 10 ){
					$returnValue = substr($value,0,3)."-". substr($value,3,3)."-". substr($value,6,4);
				}else{
					$returnValue = substr($value,0,3)."-". substr($value,3,4)."-". substr($value,7,4);
				}

			}else{

				if( strlen($value) == 11 ){
					$returnValue = substr($value,0,3)."-". substr($value,3,3)."-". substr($value,6,4);
				}else{
					$returnValue = substr($value,0,3)."-". substr($value,3,4)."-". substr($value,7,4);
				}
			}


		}else if( strlen($value) == 10 ){
			$returnValue = substr($value,0,3)."-". substr($value,3,3)."-". substr($value,6,4);
		}else if(  strlen($value) == 11 ){
			$returnValue = substr($value,0,3)."-". substr($value,3,4)."-". substr($value,7,4);
		}else{

		}
		$returnValue = $returnValue;
		
		break;

		case "date":
			if( $value=='0000-00-00 00:00:00'){
				$returnValue = "";
			}else{
				$returnValue = str_replace("-",".",substr($value,0,10));
			}

		break;

		case "datetime":
			if( $value=='0000-00-00 00:00:00'){
				$returnValue = "";
			}else{
				$returnValue = substr($value,0,16);
			}
		break;

		case "datetime2":
			if( $value=='0000-00-00 00:00:00'){
				$returnValue = "";
			}else{
				$returnValue = str_replace("-",".",substr($value,5,11));
			}
		break;

		case "min":
			if( $value=='0000-00-00 00:00:00'){
				$returnValue = "";
			}else{
				$returnValue = substr($value,0,16);
			}
		break;
		case "day":
			if( $value=='0000-00-00 00:00:00'){
				$returnValue = "";
			}else{
				$returnValue = str_replace("-",".",substr($value,5,5));
			}

		break;
	}

	return $returnValue;
}

function wp_redirect_mgs($url , $msg){
	echo '<script type="text/javascript">
	<!--
		alert("'.$msg.'");
		location.replace("'.$url.'");
	//-->
	</script>';	
	exit;
}

function wpb_hidetitle_class($classes) {

	if ( is_single() || is_page() ) :

	$classes[] = 'hidetitle';

	return $classes;

	endif;

	return $classes;

}

add_filter('post_class', 'wpb_hidetitle_class');

function set_session_cookie($id = ''){
	$_id = $id?$id:get_current_user_id();
	setcookie ('rh_user_login',$_id , current_time('timestamp') + 45 , '/', '.ibusiness.co.kr');
	//echo $_id ;
}


add_action( 'login_form_register', 'wpse45134_catch_register' );

function wpse45134_catch_register()
{
    wp_redirect( home_url( '/member/join/' ) );
    exit(); 
}

add_action( 'password_reset', 'my_password_reset', 10, 2 );

function my_password_reset( $user, $new_pass ) {
      // Do something before password reset.
	  
}

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
			width: 100%;
		    background-size: 191px 54px;
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

function _rand(){
	return rand();
}

//require_once( __DIR_INC___ . '/member-ajax.php');
require get_template_directory() . '/member/member-ajax.php';
require get_template_directory() . '/member/R_user.php';
require get_template_directory() . '/service/function.php';
require get_template_directory() . '/store/function.php';
require get_template_directory() . '/customer/function.php';
require get_template_directory() . '/utils.php';


