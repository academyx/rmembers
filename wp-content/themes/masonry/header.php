<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Masonry
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?
$my_company = rh_my_company_init();
//$r_user = new R_user();
?>
<?php wp_head(); ?>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=2, user-scalable=yes" /> -->
<meta name="author" content="" />
<meta name="keywords" content="Rehoboth Membership" />
<meta name="copyright" content="Rehoboth Membership" />
<meta name="description" content="Rehoboth Membership" />
<link rel="shortcut icon" href="//www.ibusiness.co.kr/wp-content/uploads/2016/04/rehoboth_logo_favicon_.png" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/css/hg_common.css?v=<?=_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/css/hg_layout.css?v=<?=_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/css/hg_style.css?v=<?=_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/css/hg_popup.css?v=<?=_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/css/font-awesome.min.css?v=<?=_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/style.css?v=<?=_rand()?>" />

<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<script src="/js/respond_min.js"></script>
<![endif]-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=get_stylesheet_directory_uri();?>/js/hg_lib.js?v=<?=_rand()?>"></script>
<script type="text/javascript" src="<?=get_stylesheet_directory_uri();?>/js/hg_common.js?v=<?=_rand()?>"></script>

</head>
<script type="text/javascript">
<!--
	var ajax_url = "<?=admin_url( 'admin-ajax.php' )?>";
//-->
</script>
<body <?php body_class(); ?>>


<!--<div class='css_loader uil-ring-css' style='transform:scale(0.44);'><div></div></div>-->

<!--[if lte IE 8]>
<div class="alert-brw-update">
	<span class="bg"></span>
	<div class="update_cts">
		사용 중인 브라우저는 보안에 취약한 구 버전의 브라우저입니다.<br />
		웹 브라우저를 <a href="http://www.microsoft.com/korea/ie" class="alert-link" target="_blank">업그레이드</a>하시거나, 다른 브라우저를 사용해보시기 바랍니다.<br />
		<a href="http://www.google.com/chrome?hl=ko" class="alert-link" target="_blank">구글 크롬 바로가기</a>,
		<a href="http://www.mozilla.or.kr/ko/firefox/" class="alert-link" target="_blank">파이어폭스 바로가기</a>

		<div class="hid_check">
			<input type="checkbox" name="hid_pop" id="hid_pop" /> <label for="hid_pop">오늘하루 보이지 않기</label>
		</div>
	</div>
	<button type="button" class="btn-update-close">&times;</button>
</div>
<![endif]-->

<!-- wrap -->
<div id="wrap">
	<!-- skip navi -->
	<div id="accessibility"><a href="#container">본문 바로가기</a></div>
	<!-- //skip navi -->

	<!-- header -->
	<header id="header">
		<!-- header_top -->
		<div class="header_top">
			<section class="inner">
				<!-- sns_list -->
				<ul class="sns_list">
					<li><a class="fusion-social-network-icon fusion-facebook fusion-icon-facebook" href="https://www.facebook.com/rehoboth.korea" target="_blank" title="페이스북"><span class="hid">Facebook</span></a></li>
					<li><a class="fusion-social-network-icon fusion-youtube fusion-icon-youtube" href="https://www.youtube.com/user/RehobothBI" target="_blank" title="유튜브"><span class="hid">Youtube</span></a></li>
					<li><a class="fusion-social-network-icon fusion-mail fusion-icon-mail" href="mailto:biz@ibusiness.co.kr" target="_self" title="메일" ><span class="hid">Email</span></a></li>
					<li><a class="" href="http://blog.naver.com/rehoboth2009" target="_blank" title="네이버 블로그"><span class="hid">Naver Blog</span><img src="http://www.ibusiness.co.kr/wp-content/uploads/2015/02/naver__.png" alt="Naver Blog" /></a></li>
				</ul>
				<!-- //sns_list -->

				<!-- util -->
				<div class="util_list">
					<ul>
						<? if(!get_current_user_id()){ ?>
						<li><a href="/member/join">Join</a></li>
						<li><a href="/login/"><span class="fc_org1">Login</span></a></li>
						<?}else{?>
						<li><a href="/member/modify">Mypage</a></li>
						<li><a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="fc_org1">Logout</span></a></li>
						
						<? if(is_super_admin()){?>
						
						<li><a href="/CI/sys_admin/authSys" target="blank">관리자</a></li>

						<?}?>
						

						<?}?>

					</ul>
				</div>
				<!-- //util -->
			</section>
		</div>
		<!-- //header_top -->

		<!-- gnb -->
		<div id="gnb_wrap">
			<div class="inner">
				<h1 class="logo"><a href="/"><img src="<?=get_stylesheet_directory_uri();?>/images/logo.png" alt="Rehoboth Members" /></a></h1>
				<ul id="gnb">
					<li><a href="/member/use_guide">R;멤버십</a>
						<ul class="dep2">
							<li><a href="/member/use_guide">이용안내</a></li>
							<!-- <li><a href="#">등급안내</a></li> -->
							<li><a href="/member/benefit">혜택안내</a></li>
						</ul>
					</li>
					<li><a href="/service/point-charge-step1">R;서비스</a>
						<ul class="dep2">
							<li><a href="#" onclick="rh_alert('준비중입니다');return false"><font color="#cccccc">회의실 상담예약</font></a></li>
							<li><a href="#" onclick="rh_alert('준비중입니다');return false"><font color="#cccccc">사무실예약</font></a></li>
							<li><a href="/service/point-charge-step1">포인트충전</a></li>
						</ul>
					</li>
					<li><a href="/store/list">R;스토어</a></li>
					<li><a href="/member/modify">마이페이지</a>
						<ul class="dep2">
							<li><a href="/member/modify">회원정보 수정</a></li>
							<? if($my_company){?>
							<li><a href="/member/my_company">회사정보 수정</a></li>
							<?}?>
							<li><a href="/member/my_coupon">마이쿠폰</a></li>
							<li><a href="/member/my_point">마이포인트</a></li>
							<li><a href="/member/event_list">구매/취소내역</a></li>
						</ul>
					</li>
					<li><a href="#">고객센터</a>
						<ul class="dep2">
							<li><a href="/customer/notice">공지사항</a></li>
							<li><a href="/customer/faq">자주묻는질문</a></li>
							<li><a href="/customer/qna">묻고답하기</a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="subm_bg"></div>
		</div>
		<!-- //gnb -->
	</header>
	<!-- //header -->


		<div id="container">
		<!-- contents -->
		<div id="contents">