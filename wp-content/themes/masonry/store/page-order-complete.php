<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

$info = get_order_info($_REQUEST['o']);

get_header(); 

?>

<!-- sub_article -->

			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">신</span>청내역 및 정보 안내</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box mb20">
					<!-- article inner -->
					<article class="inner">
						<!--  prd_result_wrap -->
						<div class="prd_result_wrap">
							<div class="prd_result_top">
								<img src="<?=get_stylesheet_directory_uri();?>/images/ico_cart.png" />
								<p class="txt1">신청해 주셔서 감사드립니다</p>
								<p class="txt2">상품 신청후 수령까지의 자세한 단계는 상품마다 조금씩 다를수 있습니다. <br />자세한 사항은 <span class="fc_org1">마이페이지>>구매/취소내역</span>에서 <span class="fc_org1">확인</span>하실수 있습니다.</p>

								<? if( $info['prduct_info']->c_order_finish_content ){?>
								<p class="txt_box">
									<?=stripslashes($info['prduct_info']->c_order_finish_content)?>
								</p>
								<?}?>

							</div>
						</div>
						<!-- //prd_result_wrap -->
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200px;" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal">이름</th>
									<td><?=$info['prduct_info']->c_nm_pr?></td>
								</tr>
								<? if($info['order_info']->c_option){?>
								<tr>
									<th scope="row" class="tal">선택옵션</th>
									<td>
										<ul id="select_option_list">
											<? 
											
											$options = explode("^^" , $info['order_info']->c_option );
											
											foreach ( $options as $o ){
												$item = explode(":" , $o);
											?>
											<li>
												<span class="option_name"><?=$item[2]?></span> / <?=number_format($item[3])?> x <?=$item[1]?>												
												<span class="option_total_price"><span class="point"><?=number_format($item[4])?></span> R;포인트 </span>
											</li>
											<?}?>
										</ul>
									</td>
								</tr>
								<?}?>
								<tr>
									<th scope="row" class="tal">총수량</th>
									<td><?=number_format($info['order_info']->c_count)?></td>
								</tr>
								<tr>
									<th scope="row" class="tal">사용R;포인트</th>
									<td><span id="order_total_price"><?=number_format($info['order_info']->c_payment_price)?></span>  R;포인트</td>
								</tr>
								
							</tbody>
						</table>

						<div class="ta_btn_area">
						
							<a href="/member/event_list/" class="hgbtn grey01 wsize2" style="width:250px">구매 / 신청내역 확인</a>
							<? if($info['order_info']->order_state == '0'){?>
							<a class="hgbtn org01 wsize2" style="width:250px;background:#cccccc;border:none;">취소완료</a>
							<?}else{?>
							<a href="#" class="hgbtn org01 wsize2 cancelBtn" style="width:250px">취소하기</a>
							<?}?>
						
							
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>	
			
			<script type="text/javascript">
			<!--
				
				$(document).ready(function(){

					$(".cancelBtn").on("click" ,function(){
							var cancel_id = "<?=$_REQUEST['o']?>";

							if(confirm("취소하시겠습니까?")){

								$.ajax({
								type: 'POST',
								dataType: 'json',
								url: ajax_url,
								data: { 
									'action': 'rh_cancel_prd_order', //calls wp_ajax_nopriv_ajaxlogin
									'cancel_id': cancel_id 
								},
								success: function(data){
									//	console.log(data);
									rh_alert(data.msg,function(){
										location.href = '/member/event_list/';
									});
																	
								}});

							}

							return false;


				});


				});
				


			//-->
			</script>
			

<?php get_footer(); ?>