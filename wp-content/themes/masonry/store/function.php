<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

function get_product_info($prdNo){
	global $wpdb, $current_user , $current_user_extra;
	get_user_info();
	
	$prdNo = $prdNo?$prdNo:$_REQUEST[prdNo];
	
	$info = $wpdb->get_row( "SELECT * FROM rm_product_mst WHERE c_idx_pr = '".$prdNo."'");	
	if($info){
		$options = $wpdb->get_results( " SELECT * FROM rm_product_option_mst where 	prd_idx_pr='".$info->c_idx_pr."' order by 	option_id asc"); 
	}

	if( floor($current_user_extra->c_state) < floor($info->c_apply_level) ){

		if( $info->c_apply_level == '4'){
			wp_redirect_mgs("/store" , "르호봇 입주사에게만 허용된 내용입니다.");
		}else if( $info->c_apply_level == '3'){
			wp_redirect_mgs("/store" , "기업고객에게만 허용된 내용입니다.");
		}else if( $info->c_apply_level == '2'){
			wp_redirect_mgs("/store" , "본인인증 회원에게만 허용된 내용입니다.");
		}else if( $info->c_apply_level == '1'){
			wp_redirect_mgs("/store" , "로그인 한 회원에게만 허용된 내용입니다.");
		}
		
	}
		
	$result['product'] = $info;
	$result['options'] = $options;
	return $result;

}

function get_order_info($ordNo){
	global $wpdb;
	$current_user = get_currentuserinfo();
	$ordNo = $ordNo?$ordNo:$_REQUEST[o];

	$order_info = $wpdb->get_row( "SELECT * FROM rm_order_mst WHERE c_od_num = '".$ordNo."' and c_idx_member='".$current_user->ID."'");	

	//var_dump($order_info);

	if($order_info){
		$prduct_info = $wpdb->get_row( "SELECT * FROM rm_product_mst WHERE c_idx_pr = '".$order_info->c_idx_pr."'");	
	}
	$info['order_info'] = $order_info ;
	$info['prduct_info'] = $prduct_info ;
	return $info;
	

}


function rh_order_submit(){
	global $wpdb;	
	$current_user = get_currentuserinfo();

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-order-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
	/*
	c_idx_od 일련번호	c_idx_pr 주문상품 일련번호	c_idx_coupon 주문쿠폰	c_od_num 주문번호	c_prcode 주문상품	c_option 주문옵션	c_count 주문수량	c_id_member 주문자 아이디	c_nm_member 주문자명	c_post 배송지우편번호	c_addr1 배송지주소1	c_addr2 배송지주소2	c_reg_date 등록일	c_modi_date 수정일	c_del_date 삭제일
	O20161206172700001
	*/

	
	
	$prdNo = $_POST[prdNo];
	$info = $wpdb->get_row( "SELECT * FROM rm_product_mst WHERE c_idx_pr = '".$prdNo."'");

	if( $info->c_link ){
		
		
		$table = "rm_api_encrypt_mst";

		$key = anotherRandomIDGenerator();
		
		$api = array();	
		
		$data['state'] = 0;
		$where['user_idx'] = get_current_user_id();
		$where['product_idx'] = $prdNo;	

		$wpdb->update( $table, $data, $where );		
		
		$api['user_idx'] = get_current_user_id();
		$api['product_idx'] = $prdNo;	
		$api['state'] = 1;
		$api['key'] = $key;
		$api['coupon_idx'] = $_POST[coupon_id];
		$api['reg_date'] = current_time('mysql');



		$wpdb->insert($table,$api);
		
		$api['result'] = '2';		

		echo json_encode($api);

	
	}else{


		$order = array();
		$order['c_prcode'] = $info->c_prcode;
		$order['c_idx_pr'] = $prdNo;
		$order['c_od_num'] = "O".date("YmdHis").substr((string)microtime(), 2, 3);
		$order['c_idx_member'] = $current_user->ID;
		$order['c_id_member'] = $current_user->user_login;
		$order['c_nm_member'] = $current_user->display_name;
		$order['c_count'] = $_POST[count];
		$order['c_option'] = $_POST[options];
		$order['c_order_price'] = $_POST[order_price];
		$order['c_payment_price'] = $_POST[order_price];
		$order['c_reg_date'] = current_time('mysql');


		$bResul = use_point_by_order( $order );

		

		$result = array();
		if($bResul > 0 ){
			$result['result'] = '1';
			$result['o'] = $order['c_od_num'];
		}else if( $bResul == 0 ){
			$result[order] = $order;
			$result['result'] = '0';
			$result['msg'] = '포인트가 부족합니다. 포인트 충전 후 다시 이용해 주세요.';
		}

		echo json_encode($result);

	
	}


	

	die();
}

add_action('wp_ajax_rh_order_submit', 'rh_order_submit');

function use_point_by_order($order){
	
	global $wpdb;	

	$user_id = get_current_user_id();
	
	$mypoint = $wpdb->get_var("select c_point from rm_member_mst where c_idx_member = '".$user_id."'");

	$order_id = 0;
	
	if( $mypoint > 0 && $order['c_order_price'] <= $mypoint ){
		
		$order_id = $wpdb->insert('rm_order_mst' , $order);

		$pointdata = array();
		$pointdata['c_idx_member'] = $user_id;
		$pointdata['c_type'] = '2';
		$pointdata['c_amount'] = $order['c_order_price'];
		$pointdata['order_mst_id'] = $order_id;
		$pointdata['c_reg_date'] = current_time('mysql');
		$c_rest = $mypoint - $order['c_order_price'];
		$pointdata['c_rest'] = $c_rest;
	
	
		$wpdb->insert("rm_point_mst",$pointdata);

		$wpdb->update( 'rm_member_mst' , array( 'c_point' => $c_rest ) , array( 'c_idx_member' => $user_id ));


	}else{
	
	}
	

	return $order_id;



}

function get_prd_list(){

	global $wpdb;	
	
	$sort = $_REQUEST['sort']?$_REQUEST['sort']:'cnt';

	if( $sort =='high'){
		$orderby = 'c_price desc';
	}else if( $sort =='low' ){
		$orderby = 'c_price asc';
	}else if( $sort =='date' ){
		$orderby = 'c_idx_pr desc';
	}else{
		$orderby = 'cnt desc';
	}

	if($_REQUEST['searchStr']){
		$where = " and ( c_nm_pr like '%".$_REQUEST['searchStr']."%' or c_summary_pr like '%".$_REQUEST['searchStr']."%' ) ";
	}


	$sql = "SELECT * , ( select count(*) from rm_order_mst where rm_order_mst.c_idx_pr = rm_product_mst.c_idx_pr ) as cnt FROM  `rm_product_mst` 
			where c_show_sdate <= '".current_time('Y-m-d')."' and c_show_edate >= '".current_time('Y-m-d')."' ".$where.
			" order by ".$orderby;

	$query = $wpdb->get_results( $sql , ARRAY_A);

	$result = "<ul>";

	foreach( $query  as $key=> $row){

		$result .= '<li>
			<a href="/store/detail/?prdNo='.$row['c_idx_pr'].'" />
			<div class="pic"><img src="'.getProductImage($row['c_img']).'" alt="'.$row['c_nm_pr'].'"></div>
			<dl class="prd_info">			
			<dt class="tit">'.$row['c_nm_pr'].'</dt>
			<dd class="txt">'.$row['c_summary_pr'].'</dd>
			</dl></a>
		</li>';

	}
	

	$result .= "</ul>";

	return $result;

}

function get_prd_list_main(){

	global $wpdb;	
	
	$sql = "SELECT * FROM  `rm_product_mst` 
				where c_show_sdate <= '".current_time('Y-m-d')."' and c_show_edate >= '".current_time('Y-m-d')."' and c_main_yn ='Y' 
				order by c_idx_pr asc";

	$query = $wpdb->get_results( $sql , ARRAY_A);

	$result = "<ul>";

	foreach( $query  as $key=> $row){

		$result .= '<li>
			<a href="/store/detail/?prdNo='.$row['c_idx_pr'].'" />
			<div class="pic"><img src="'.getProductImage($row['c_img']).'" alt="'.$row['c_nm_pr'].'"></div>
			<dl class="prd_info">			
			<dt class="tit">'.$row['c_nm_pr'].'</dt>
			</dl></a>
		</li>';

	}
	

	$result .= "</ul>";

	return $result;
	
}

function getProductImage($idx){
	global $wpdb;	
	$fileName = $wpdb->get_var("select c_file_newname from rm_file where c_file_idx = '".$idx."'");
	return "/CI/asset/uploadImage/".$fileName;
}


function anotherRandomIDGenerator() {
    // Copyright: http://snippets.dzone.com/posts/show/3123
    $len = 25;
    $base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz';
    $max=strlen($base)-1;
    $activatecode='';
    mt_srand((double)microtime()*1000000);
    while (strlen($activatecode)<$len+1)
    $activatecode.=$base{mt_rand(0,$max)};
    return $activatecode;
}

function calcuate_price(){
	
}