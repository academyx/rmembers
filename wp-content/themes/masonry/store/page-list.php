<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

get_header(); 

$_REQUEST['sort'] = $_REQUEST['sort']?$_REQUEST['sort']:'cnt';

?>
<style type="text/css">
	#container{padding-top:10px;}
	.ip_list1 li{line-height:35px;padding:0 5px !important;}
	.ip_list1 li a{color:#777777;}
	.ip_list1 li a.on{color:#000000;font-weight:bold;}
</style>
<!-- sub_article -->
			<section id="sub_article">
				<!-- store_prd_list -->
				<div class="store_prd_list">
					<!-- sort sh -->
					<div class="sort_box">
						<div class="fl">
							<ul class="ip_list1">
								<li><a href="#" onclick="fn_sort('cnt');return false" <?if($_REQUEST['sort']=='cnt'){?> class="on" <?}?> >·&nbsp; 인기순</a></li>								
								<li><a href="#" onclick="fn_sort('low');return false" <?if($_REQUEST['sort']=='low'){?> class="on" <?}?> >·&nbsp; 낮은 가격순</a></li>								
								<li><a href="#" onclick="fn_sort('high');return false" <?if($_REQUEST['sort']=='high'){?> class="on" <?}?> >·&nbsp; 높은 가격순</a></li>
								<li><a href="#" onclick="fn_sort('date');return false" <?if($_REQUEST['sort']=='date'){?> class="on" <?}?> >·&nbsp; 등록일순</a></li>
							</ul>
						</div>
						<div class="fr">
							<form method="get" action="" id="store">

							<input type="hidden" id="sort" name="sort" value="<?=$_REQUEST['sort']?>">	
							
							<dl class="ip_dl_list1">
								<!-- <dt>결과 내 재검색</dt>
								<dd>
									<select name="" class="sel01">
										<option value="">가격순</option>
									</select>
								</dd> -->
								<dd>
									<label for="re_sh"><input type="text" name="searchStr" id="re_sh" value="<?=$_REQUEST['searchStr']?>" placeholder="검색어를 입력해 주세요" class="ip02" /></label>
								</dd>
								<dd>
									<button type="submit" class="hgbtn grey01 btn_smit">검색</button>
								</dd>
							</dl>

							</form>
						</div>
					</div>
					<!-- //sort sh -->

					<!-- 상품 리스트 -->
					<div class="prd_list">
						<?=get_prd_list()?>
					</div>
					<!-- //상품 리스트 -->

					<!-- 페이징 -->
					<div class="pagenate">
						<div class="page">
							<a href="" title="맨처음페이지" class="page_btn first">&lt;&lt;</a>
							<a href="" title="이전리스트" class="page_btn prev">&lt;</a>
							<span class="page_num"><a class="on">1</a></span>
							
							<a href="" title="다음리스트" class="page_btn next">&gt;</a>
							<a href="" title="맨 끝페이지" class="page_btn end">&gt;&gt;</a>
						</div>
					</div>
					<!-- //페이징 -->
				</div>
				<!-- //store_prd_list -->
			</section>
			<!-- //sub_article -->

			<script type="text/javascript">
			<!--
				function fn_sort(sort){
					$("#sort").val(sort);
					$("#store").submit();
				}
			//-->
			</script>

			

<?php get_footer(); ?>