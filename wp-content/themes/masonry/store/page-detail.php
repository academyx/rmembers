<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
$prd_info = get_product_info();
get_header(); 

?>

<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_directory_uri();?>/js/stepper/jquery.stepper.min.css" />
<script type="text/javascript" src="<?=get_stylesheet_directory_uri();?>/js/stepper/jquery.stepper.min.js"></script>
<!-- sub_article -->

			<section id="sub_article">
				<!-- prd_detail_wrap -->
				<div class="prd_detail_wrap mb20">
					<!-- 상세 본문 -->
					<article class="prd_detail_cts">
						<div class="prd_detail_section">
							<?= stripslashes($prd_info['product']->c_content) ?>
						</div>					

						<?php wp_nonce_field( 'ajax-order-nonce', 'security' ); ?>

					</article>
					<!-- //상세 본문 -->
				</div>

				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200px;">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal">이름</th>
									<td><?=$prd_info['product']->c_nm_pr ?></td>
								</tr>
								<? if($prd_info['product']->c_link) {?>
								<tr>
									<th scope="row" class="tal">옵션선택</th>									
									<td>
										제휴사이트에서 직접 접속해서 진행하실 수 있습니다.
									</td>
								</tr>
								<?}else{?>


								<? if($prd_info['options']) { ?>
								<tr>
									<th scope="row" class="tal">옵션선택</th>
									<td>
										<select name="" class="sel01" id="option_selecter">
											<option value="" selected>선택해주세요</option>
											<? foreach( $prd_info['options'] as $o ){?>

											<option value="<?=$o->option_id?>" data-name="<?=$o->option_name?>" data-price="<?=$o->option_price?>"><?=$o->option_name?> / <?=number_format($o->option_price)?> </option>

											<? } ?>

										</select>

										<ul id="select_option_list" class="hide">
											
											
										</ul>
									</td>
								</tr>

								<tr>
									<th scope="row" class="tal">사용R;포인트</th>
									<td><span id="order_total_price">0</span>  R;포인트</td>
								</tr>

								<?}else{?>

								<tr>
									<th scope="row" class="tal">신청수량</th>
									<td>

									<span class="option_total_stepper"><input type="text" size="3" class="stepper" min="1" maxlength="2" /></span>
									
									</td>
								</tr>


								<tr>
									<th scope="row" class="tal">사용R;포인트</th>
									<td><span id="order_total_price"><?=number_format($prd_info['product']->c_price)?></span>  R;포인트</td>
								</tr>

								<? } ?>

								<?}?>

								
								
								
								
							</tbody>
						</table>

						<div class="ta_btn_area">
							<a href="store" class="hgbtn grey01 wsize2" style="width:200px">목록으로</a>
							
							<? if($prd_info['product']->c_link) {
								$order_confirm = "제휴사이트로 이동하시겠습니까?";
							?>
							<a href="#" id="order-complete" class="hgbtn org01 wsize2" style="width:200px">제휴 사이트 이동</a>
							<?}else{
								$order_confirm = "신청하시겠습니까?";
							?>
							<a href="#" id="order-complete" class="hgbtn org01 wsize2" style="width:200px">신청하기</a>
							<?}?>
							


						</div>
					</article>
					<!-- //article inner -->
				</div>

				<!-- //prd_detail_wrap -->
			</section>		

		<? if($prd_info['options']) { ?>
			<div style="display:none" id="dummy">

			<li>
				<span class="option_name"></span>
				<span class="option_total_stepper"><input type="text" size="3" class="stepper" min="1" maxlength="2" /></span>
				<span class="option_total_price"><span class="point">1000</span> R;포인트 </span>
				<i class="fa fa-times" aria-hidden="true"></i>
			</li>
			</div>
		<?}?>

			<script type="text/javascript">
			<!--

				var optionData = [];
				var optionString = "";
				var total_counts = 0;
				var total_price = 0;


				( function( $ ) {
					
					
					$(document).ready(function(){	
						
						$("#select_option_list").on("click","i",function(){
							del_option(this);
						});

						$("#option_selecter").change(function(e) {
							//rh_alert($(this).find("option:selected").data("price"));
							add_option( $(this).find("option:selected") );
						});
						
						$("#order-complete").bind("click",function(e){
							
							<? if($prd_info['options']) {?>
							if( !optionString ){
								rh_alert("옵션을 선택해 주세요");
								return false;
							}
							<?}?>
							
							if(confirm('<?=$order_confirm?>')){
								
								$.ajax({
								type: 'POST',
								dataType: 'json',
								url: ajax_url,
								data: { 
									'action': 'rh_order_submit', //calls wp_ajax_nopriv_ajaxlogin
									'security': $('#security').val() ,
									'options' : optionString,
									'count' : total_counts,
									'order_price' : total_price,
									'prdNo': '<?=$prd_info[product]->c_idx_pr?>'
								},
								success: function(data){

									if(data.result == '1'){
										var params = {'o' : data.o , 'prdNo' : '<?=$_REQUEST[prdNo]?>' };
										
										rh_redirect( '/store/order-complete' , params);
									}else if(data.result == '2'){

										var params = {'key' : data.key };
										
										rh_redirect( "<?=$prd_info['product']->c_link?>?key="+data.key , params , "blank");

									}else{
										if (data.msg){
											rh_alert(data.msg);
										}else{
											rh_alert("신청 오류 입니다.\n 관리자에 문의 또는 다시 신청해 주세요");
										}
										
									}

								}});
								
							}													

							e.preventDefault();

						});

						<? if(!$prd_info['options']) {?>
						$(".stepper").stepper({ limit: [1, 100] , onStep: stepCallback }).val(1);
						<?}?>

						});


			} )( jQuery );

				function stepCallback( val, up){
					//console.log(val+":"+up+":"+ $(this).closest("li").data("price") );

					<? if($prd_info['options']) {?>

						var _o = $(this).closest("li");
						var price = val*_o.data("price");
						_o.find(".option_total_price").text(price.format());
						_o.data("total_price" , price );

					<?}else{?>

					<?}?>

					

					caculate();
					
				}
			
				function add_option(t){
					
					var option_id = $(t).val();
					var option_name = $(t).data("name");
					var option_price = $(t).data("price");

					if(option_id && !check_exists(option_id)){

						var _o = $("#dummy li").clone(false);
						
						_o.data("idx" , option_id );
						_o.data("price" , option_price );
						_o.data("total_price" , option_price );
						
						_o.find(".option_name").text(option_name);
						_o.find(".option_total_price").text(option_price.format());
						_o.find(".stepper").val(1).bind("change",function(e){
							console.log($(this).val());

							if( Math.floor($(this).val()) > 0 ){
							}else{
								$(this).val(1);
							}

								$(this).val(Math.floor($(this).val()));
								
								var price = $(this).val()*_o.data("price");
								_o.find(".option_total_price").text(price.format());
								_o.data("total_price" , price );
								
								caculate();

							

						});
						$("#select_option_list").append(_o);
						_o.find(".stepper").stepper({ limit: [1, 100] , onStep: stepCallback });

						
						caculate();
					}
				
				}

				function del_option(t){
					var _o = $(t).closest("li");
					_o.remove();
					caculate();
				}

				function caculate(){
					
					optionData = [];
					total_price = 0;
					total_counts = 0;

					<? if($prd_info['options']) {?>

					$("#select_option_list li").each(function(){
						total_price += $(this).data("total_price");

						console.log( $(this).data("total_price") );

						optionData.push($(this).data("idx")+":"+$(this).find(".stepper").val()+":"+$(this).find(".option_name").text()+":"+$(this).data("price")+":"+$(this).data("total_price"));

						total_counts += Math.floor($(this).find(".stepper").val());

					});

					optionString = optionData.join("^^");

					


					

					if($("#select_option_list li").length > 0){
						$("#select_option_list").show();
					}else{
						$("#select_option_list").hide();
					}

					<?}else{?>

						total_counts = $(".stepper").val();
						total_price = <?=$prd_info['product']->c_price?> * total_counts;
						

					<?}?>


					$("#order_total_price").text(total_price.format()); 


				
				}

				function check_exists(id){
					var _exists = false;
					$("#select_option_list li").each(function(element , index){
						if($(this).data("idx") == id){
							_exists = true
							return false;
						}
					});

					return _exists;
				}


			//-->
			</script>




			

<?php get_footer(); ?>