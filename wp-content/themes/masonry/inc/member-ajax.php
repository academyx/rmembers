<?php


function ajax_login_init(){

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/ajax-login-script.js', array('jquery') ); 
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}


function ajax_login(){
	
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );
	

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
	
    if ( is_wp_error($user_signon) ){       
	   echo json_encode(array('loggedin'=>false, 'message'=>__('아이디 패스워드가 일치하지 않습니다.')));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    }

    die();
}


function rh_ajax_register(){	
	global $wpdb;
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );
		
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['user_id']) ;
    $info['user_pass'] = sanitize_text_field($_POST['user_pass']);
	$info['user_email'] = sanitize_email( $_POST['email']);
	$info['display_name'] = $_POST['display_name'];
	
	// Register the user
    $user_register = wp_insert_user( $info );
	
 	if ( is_wp_error($user_register) ){	
		$error  = $user_register->get_error_codes()	;
		
		if(in_array('empty_user_login', $error))
			echo json_encode(array('loggedin'=>false, 'message'=>__($user_register->get_error_message('empty_user_login'))));
		elseif(in_array('existing_user_login',$error))
			echo json_encode(array('loggedin'=>false, 'message'=>__('This username is already registered.')));
		elseif(in_array('existing_user_email',$error))
        echo json_encode(array('loggedin'=>false, 'message'=>__('This email address is already registered.')));
    } else {
		
		$wpdb->delete( 'rm_member_mst', array( 'user_login' => $info['user_nicename'] ) );

		$wpdb->insert( 'rm_member_mst' , array( 'c_idx_member' => $user_register ,'user_login' => $info['user_nicename'] , 'c_email_yn' => $_POST['c_email_yn'] , 'c_reg_date' => date("Y-m-d H:i:s")) );

		echo json_encode(array('loggedin'=>true, 'message'=>__('회원가입 성공.'), 'redirect'=>'/member/join_ok' ));

	  //auth_user_login($info['nickname'], $info['user_pass'], 'Registration');       
    }
 
    die();
}


add_action( 'wp_ajax_nopriv_ajax_register', 'rh_ajax_register' );



function rh_user_id_exists()
{
    global $wpdb;

	$user = $_POST[user_id];
	
    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE user_login = %s", $user));
	$result = array();
	$result['user_id'] = $user;
    if($count == 1){ 
		$result['result'] = '1';
		echo json_encode($result); 
	}else{ 
		$result['result'] = '0';
		echo json_encode($result); 
	}
    die;

}

add_action('wp_ajax_nopriv_user_id_exists', 'rh_user_id_exists');