<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header(); 

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">R;</span>포인트 충전</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="pt_charge_wrap">
							<!-- 포인트 충전 타이틀 -->
							<div class="top_tit">
								<h4 class="tit"><span class="fc_org1">R;</span>포인트 충전</h4>
								<p class="txt1">르호봇 비지니스센터의 입주기업의 회원이라면 필요시 마다 멤버십사이트를 통해 R;포인트 충전이 가능합니다.</p>
							</div>
							<!-- //포인트 충전 타이틀 -->

							<!-- 충전 스텝 -->
							<div class="point_charge_step">
								<ol>
									<li class="">
										<div class="step1">
											<p class="tit">01. 로그인</p>
											<div class="line"></div>
											<p class="txt">멤버십 사이트 로그인</p>
										</div>
									</li>
									<li class="on">
										<div class="step2">
											<p class="tit">02. 결제정보 입력</p>
											<div class="line"></div>
											<p class="txt">충전금액, 결제수단등을 입력하세요</p>
										</div>
									</li>
									<li class="">
										<div class="step3">
											<p class="tit">03. 결제</p>
											<div class="line"></div>
											<p class="txt">결제를 진행해 주세요</p>
										</div>
									</li>
									<li class="">
										<div class="step4">
											<p class="tit">04. 충전완료</p>
											<div class="line"></div>
											<p class="txt">충전된 포인트 금액을<br />확인해 주세요</p>
										</div>
									</li>
								</ol>
							</div>
							<!-- //충전 스텝 -->

							<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="200px;" />
									<col width="" />
								</colgroup>

								<tbody>
									<tr>
										<th scope="row" class="tal">상품명</th>
										<td>르호봇 멤버십 R;포인트</td>
									</tr>
									
									<tr>
										<th scope="row" class="tal">결제금액</th>
										<td>
											<select name="" class="sel01" style="width:200px;" id="price">
												<option value="">선택해 주세요 </option>
												<option value="5000">5,000</option>
												<option value="10000">10,000</option>
												<option value="15000">15,000</option>
												<option value="20000">20,000</option>
												<option value="25000">25,000</option>
												<option value="30000">30,000</option>
												<option value="35000">35,000</option>
												<option value="40000">40,000</option>
												<option value="45000">45,000</option>
												<option value="50000">50,000</option>							
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal">충전포인트</th>
										<td><span id="selected_price">0</span>  R;포인트</td>
									</tr>
									<tr>
										<th scope="row" class="tal">결제선택</th>
										<td>
											<input type="radio" name="pay_method" id="pay_type1" class="ip01 " value="card" checked />
											<label for="pay_type1">신용카드</label>
											
											
											
											<!-- <input type="radio" name="pay_method" id="pay_type2" class="ip01 ml10" value="trans" />
											<label for="pay_type2">실시간 계좌이체</label> -->

											<input type="radio" name="pay_method" id="pay_type3" class="ip01 ml10" value="HPP" />
											<label for="pay_type3">핸드폰 결제</label>
											
										</td>
									</tr>
								</tbody>
							</table>

							<p class="tac mt50">※  결제를 정상적으로 완료하지 않을 경우  <span class="fc_org1">R;포인트 충전이 취소</span>될 수 있습니다.</p>

							<div class="ta_btn_area">
								<a href="service/point-charge-step1" class="hgbtn grey01 wsize1">이전</a>
								<a href="#" onclick="send_payment();return false" class="hgbtn org01 wsize1 ml10">결제하기</a>
							</div>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>

			<!-- 이니시스 표준결제 js -->
			<? if( get_current_user_id() == '50'){ ?>
			<script language="javascript" type="text/javascript" src="https://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
			<?}else{?>
			<script language="javascript" type="text/javascript" src="https://stgstdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
			<?}?>
			
			<!--<script language="javascript" type="text/javascript" src="https://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>-->

			<script type="text/javascript">
				function pay() {
					INIStdPay.pay('SendPayForm_id');
				}
			</script>


			<script type="text/javascript">
			<!--
				<? if (!$current_user_extra->c_ci){?>

				rh_alert("포인트 충전은 본인 확인 후 하실 수 있습니다.", function(){
					location.href = "/member/modify/";
				});
				//location.href = "/member/modify/";

				<?}?>

				var pay_ing = false;

				var pay_point = 0;

				function send_payment(){
					
					if(pay_ing)return;

					var price = $("#price").val();

					if(price > 0 && pay_point > 0 ){
					
					}else{
						rh_alert("결제할 금액을 선택해 주세요");
						return false;
					}
					
					o("gopaymethod").val($("input[name=pay_method]:checked").val());
					o("price").val($("#price").val());
					
					pay_ing = true;

					$.ajax({
							type: 'POST',
							dataType: 'json',
							url: ajax_url,
							data: { 
								'action': 'rh_init_payment', //calls wp_ajax_nopriv_ajaxlogin
								'price': price ,
								'point': pay_point 
							},
							success: function(data){
								
								//console.log(data);

								o("signature").val(data.sign);
								o("oid").val(data.oid);
								o("mKey").val(data.mKey);
								o("mid").val(data.mid);
								o("timestamp").val(data.timestamp);
								

								pay();
								
							}});

					


				
				}

				( function( $ ) {

					

					$(document).ready(function(){	
						$("#price").bind("change", function(){				
							
							$("#selected_price").text($(this).val().format());
							pay_point = $(this).val();

						});

					});
				
				} )( jQuery );

			//-->
			</script>



			<form id="SendPayForm_id" name="" method="POST" style="display:none">

                                                <!-- 필수 -->
                                                <br/><b>***** 필 수 *****</b>
                                                <div style="border:2px #dddddd double;padding:10px;background-color:#f3f3f3;">

                                                    <br/><b>version</b> :
                                                    <br/><input  style="width:100%;" name="version" value="1.0" >


                                                    <br/><b>mid</b> :
                                                    <br/><input  style="width:100%;" name="mid" value="<?php echo $mid ?>" >

                                                    <br/><b>goodname</b> :
                                                    <br/><input  style="width:100%;" name="goodname" value="르호봇 멤버십 R;포인트" >

                                                    <br/><b>oid</b> :
                                                    <br/><input  style="width:100%;" name="oid" value="<?php echo $orderNumber ?>" >

                                                    <br/><b>price</b> :
                                                    <br/><input  style="width:100%;" name="price" value="<?php echo $price ?>" >

                                                    <br/><b>currency</b> :
                                                    <br/>[WON|USD]
                                                    <br/><input  style="width:100%;" name="currency" value="WON" >

                                                    <br/><b>buyername</b> :
                                                    <br/><input  style="width:100%;" name="buyername" value="<?=$current_user->display_name?>" >

                                                    <br/><b>buyertel</b> :
                                                    <br/><input  style="width:100%;" name="buyertel" value="<?=$current_user_extra->c_hp?>" >

                                                    <br/><b>buyeremail</b> :
                                                    <br/><input  style="width:100%;" name="buyeremail" value="<?=$current_user->user_email?>" >



                                                    <!-- <br/><b>timestamp</b> : -->
                                                    <input type="text"  style="width:100%;" name="timestamp" value="<?php echo $timestamp ?>" >


                                                    <!-- <br/><b>signature</b> : -->
                                                    <input type="hidden" style="width:100%;" name="signature" value="<?php echo $sign ?>" >


                                                    <br/><b>returnUrl</b> :
                                                    <br/><input  style="width:100%;" name="returnUrl" value="<?=get_site_url()?>/service/point-charge-submit/" >

                                                    <input type="hidden"  name="mKey" value="<?php echo $mKey ?>" >

													<input type="hidden"  name="gopaymethod" value="card" >
													<input type="hidden"  name="acceptmethod" value="HPP(1)" >

													
                                                </div>

                                               

                                                
                                            </form>
	
			<!-- //sub_article -->
	<?php get_footer(); ?>