<?php

function rh_init_payment(){
	global $wpdb;
	require_once($_SERVER[DOCUMENT_ROOT].'/stdpay/libs/INIStdPayUtil.php');
	$SignatureUtil = new INIStdPayUtil();
	//############################################
	// 1.전문 필드 값 설정(***가맹점 개발수정***)
	//############################################
	// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정

	if( get_current_user_id() == '50'){
		$mid = "ibusiness0";  // 가맹점 ID(가맹점 수정후 고정)	// real id ibusiness0		
		$signKey 		= "Nk1xS0RmWFdKVFV0OGprYzYxN3RnZz09";
	}else{
		$mid = "INIpayTest";  // 가맹점 ID(가맹점 수정후 고정)	// real id ibusiness0		
		$signKey 		= "SU5JTElURV9UUklQTEVERVNfS0VZU1RS";
	}	
	
	
	$timestamp = $SignatureUtil->getTimestamp();   // util에 의해서 자동생성

	$orderNumber = "order_".$SignatureUtil->getTimestamp(); // 가맹점 주문번호(가맹점에서 직접 설정)
	$price = $_POST['price'];        // 상품가격(특수기호 제외, 가맹점에서 직접 설정)

	$cardNoInterestQuota = "11-2:3:,34-5:12,14-6:12:24,12-12:36,06-9:12,01-3:4";  // 카드 무이자 여부 설정(가맹점에서 직접 설정)
	$cardQuotaBase = "2:3:4:5:6:11:12:24:36";  // 가맹점에서 사용할 할부 개월수 설정
	//###################################
	// 2. 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)
	//###################################
	$mKey = $SignatureUtil->makeHash($signKey, "sha256");

	$params = array(
		"oid" => $orderNumber,
		"price" => $price,
		"timestamp" => $timestamp
	);
	$sign = $SignatureUtil->makeSignature($params, "sha256");

	/* 기타 */
	$siteDomain = "http://".$_SERVER['HTTP_HOST']; //가맹점 도메인 입력
	// 페이지 URL에서 고정된 부분을 적는다. 
	$params['siteDomain'] = $siteDomain;
	$params['mid'] = $mid;
	$params['sign'] = $sign;
	$params['mKey'] = $mKey;
	$params['timestamp'] = $timestamp;



	$user = get_user_info();
	$paydata = array();
	$paydata['c_idx_member'] = $user[ID];
	$paydata['c_paynum'] = $orderNumber;	
	$paydata['c_payamount'] = $price;
	$paydata['c_paypoint'] = $_POST['point'];
	$paydata['c_reg_date'] = current_time('mysql');					
						
	$wpdb->insert("rm_pay_mst", $paydata );	


	

	echo json_encode($params);
	
	die();
}

add_action('wp_ajax_rh_init_payment', 'rh_init_payment');


function rh_submit_payment(){
	global $wpdb , $success_order_no , $pg_id ;	
	require_once($_SERVER[DOCUMENT_ROOT].'/stdpay/libs/INIStdPayUtil.php');
    require_once($_SERVER[DOCUMENT_ROOT].'/stdpay/libs/HttpClient.php');

    $util = new INIStdPayUtil();

	try {

            //#############################
            // 인증결과 파라미터 일괄 수신
            //#############################
            //		$var = $_REQUEST["data"];

            //#####################
            // 인증이 성공일 경우만
            //#####################
            if (strcmp("0000", $_REQUEST["resultCode"]) == 0) {

                //echo "####인증성공/승인요청####";
                //echo "<br/>";

                //############################################
                // 1.전문 필드 값 설정(***가맹점 개발수정***)
                //############################################;

                $mid 			= $_REQUEST["mid"];     					// 가맹점 ID 수신 받은 데이터로 설정
                
				if( get_current_user_id() == '50'){
					$signKey 		= "Nk1xS0RmWFdKVFV0OGprYzYxN3RnZz09";
				}else{
					$signKey 		= "SU5JTElURV9UUklQTEVERVNfS0VZU1RS";
				}

				//$signKey 		= "Nk1xS0RmWFdKVFV0OGprYzYxN3RnZz09"; 		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
                $timestamp 		= $util->getTimestamp();   					// util에 의해서 자동생성
                $charset 		= "UTF-8";        							// 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
                $format 		= "JSON";        							// 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)

                $authToken 		= $_REQUEST["authToken"];   				// 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
                $authUrl 		= $_REQUEST["authUrl"];    					// 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                $netCancel 		= $_REQUEST["netCancelUrl"];   				// 망취소 API url(수신 받은f값으로 설정, 임의 세팅 금지)

                $mKey 			= hash("sha256", $signKey);					// 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)

                //#####################
                // 2.signature 생성
                //#####################
                $signParam["authToken"] 	= $authToken;  	// 필수
                $signParam["timestamp"] 	= $timestamp;  	// 필수
                // signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
                $signature = $util->makeSignature($signParam);


                //#####################
                // 3.API 요청 전문 생성
                //#####################
                $authMap["mid"] 			= $mid;   		// 필수
                $authMap["authToken"] 		= $authToken; 	// 필수
                $authMap["signature"] 		= $signature; 	// 필수
                $authMap["timestamp"] 		= $timestamp; 	// 필수
                $authMap["charset"] 		= $charset;  	// default=UTF-8
                $authMap["format"] 			= $format;  	// default=XML




                try {

                    $httpUtil = new HttpClient();

                    //#####################
                    // 4.API 통신 시작
                    //#####################

                    $authResultString = "";
                    
                    if ($httpUtil->processHTTP($authUrl, $authMap)) {
                        $authResultString = $httpUtil->body;
						echo "<p><b>RESULT DATA :</b> $authResultString</p>";
                        		//PRINT DATA
                    } else {
                        echo "Http Connect Error\n";
                        echo $httpUtil->errormsg;

                        throw new Exception("Http Connect Error");
                    }

                    //############################################################
                    //5.API 통신결과 처리(***가맹점 개발수정***)
                    //############################################################
                    

                    $resultMap = json_decode($authResultString, true);
					
                    //var_dump($resultMap);
                    
                    /*************************  결제보안 추가 2016-05-18 START ****************************/ 
                    $secureMap["mid"]		= $mid;							//mid
                    $secureMap["tstamp"]	= $timestamp;					//timestemp
                    $secureMap["MOID"]		= $resultMap["MOID"];			//MOID
                    $secureMap["TotPrice"]	= $resultMap["TotPrice"];		//TotPrice

					$pg_id = $secureMap["MOID"];
                    
                    // signature 데이터 생성 
                    $secureSignature = $util->makeSignatureAuth($secureMap);
                    /*************************  결제보안 추가 2016-05-18 END ****************************/

					if ($resultMap["resultCode"] == '0000'){	//결제보안 추가 2016-05-18
					   /*****************************************************************************
				       * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.  
					   
						 [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
								처리중 에러 발생시 망취소를 한다.
				       ******************************************************************************/

					    $user_id = get_current_user_id();

						$current_user = wp_get_current_user();
			
						$current_user_extra = $wpdb->get_row( "SELECT a.*, b.c_idx_relay, b.c_idx_resi, b.c_level, b.c_state as c_accept_state FROM rm_member_mst a LEFT OUTER JOIN rm_relay_mst b ON a.c_idx_member = b.c_idx_member WHERE a.c_idx_member = '".$user_id."'" );
						
						if( $resultMap["payMethod"] == "HPP"){
							$resultMap["CARD_PurchaseName"] = "휴대폰결제";
						}else if( $resultMap["payMethod"] == "Vbank"){
							$resultMap["CARD_PurchaseName"] = "가상계좌";
						}

						$paydata = array();
						$paydata['c_idx_member'] = $user_id;
						$paydata['c_paynum'] = $resultMap["MOID"];
						$paydata['c_paytype'] = $resultMap["payMethod"];
						$paydata['c_payamount'] = $resultMap["TotPrice"];
						$paydata['c_modi_date'] = current_time('mysql');
						$paydata['pg_tid'] = $resultMap["tid"];
						$paydata['buyer_name'] = $resultMap["buyerName"];
						$paydata['buyer_email'] = $resultMap["buyerEmail"];
						$paydata['buyer_tel'] = $resultMap["buyerTel"];
						$paydata['card_name'] = $resultMap["CARD_PurchaseName"];
						$paydata['pay_status'] = $resultMap["resultCode"];
						$paydata['pay_data'] = $authResultString;


						$payInfo = $wpdb->get_row( "select * from rm_pay_mst where c_idx_member ='".$user_id."' and  c_paynum ='".$resultMap["MOID"]."'");

						if( $payInfo ){

							$wpdb->update("rm_pay_mst",$paydata , array( 'c_paynum' => $resultMap["MOID"]) );

							$pay_mst_id = $payInfo->c_idx_pay;
							$pointdata = array();

							$pointdata['c_idx_member'] = $user_id;
							$pointdata['c_type'] = '1';
							$pointdata['c_amount'] = $payInfo->c_paypoint;
							$pointdata['pay_mst_id'] = $pay_mst_id;
							$pointdata['c_reg_date'] = current_time('mysql');
							$c_rest = $current_user_extra->c_point + $payInfo->c_paypoint;
							$pointdata['c_rest'] = $c_rest;
							
							
							$wpdb->insert("rm_point_mst",$pointdata);

							$wpdb->update( 'rm_member_mst' , array( 'c_point' => $c_rest , 'c_last_charge_point_sum' => $c_rest ) , array( 'c_idx_member' => $user_id ));

	//						wp_redirect( '/service/point-charge-complete?o='.$paydata['c_paynum'] );

							$success_order_no = $paydata['c_paynum'];

							/*아이디	##USER_ID##
							이름	##USER_NAME##
							결제수단	##PAY_METHOD##
							주문번호	##ORDER_NO##
							결제금액	##PAY_PRICE##
							충전포인트	##PAY_POINT##
							내 포인트	##MY_POINT##*
							결제시간 ##REG_TIME##
							*/
							

							sendEmail("6" , array("USER_ID"=>$current_user->user_login , "USER_NAME" => $current_user->display_name , "REG_TIME" => date("Y.m.d H:i:s") , "PAY_METHOD"=> $resultMap["CARD_PurchaseName"] , "ORDER_NO"=> $resultMap["MOID"] , "PAY_PRICE"=> number_format($paydata['c_payamount']) , "PAY_POINT" => number_format($pointdata['c_amount']) , "MY_POINT"=>number_format($c_rest) ,"receiveMailAddr" => $current_user->user_email ) , array( "PAY_POINT"=> number_format($pointdata['c_amount']) , "recipientNo"=> str_replace("-","",$current_user_extra->c_hp)) );
						
						
						}else{
						
						
						}


						
						

						


                        
					} else {
						
                        
                        
                    }
					
					

                    

                    if (isset($resultMap["payMethod"]) && strcmp("VBank", $resultMap["payMethod"]) == 0) { //가상계좌
                        
							
                    } else if (isset($resultMap["payMethod"]) && strcmp("DirectBank", $resultMap["payMethod"]) == 0) { //실시간계좌이체
                        
                    } else if (isset($resultMap["payMethod"]) && strcmp("HPP", $resultMap["payMethod"]) == 0) { //휴대폰
                       
                    } else if (isset($resultMap["payMethod"]) && strcmp("KWPY", $resultMap["payMethod"]) == 0) { //뱅크월렛 카카오
                        
                    } else { //카드
                                                
                    }

                    

                    // 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
                    // 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시
                    // payViewType을 popup으로 해서 결제를 하셨을 경우
                    // 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요
                    //throw new Exception("강제 Exception");
                } catch (Exception $e) {
                    // $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
                    //####################################
                    // 실패시 처리(***가맹점 개발수정***)
                    //####################################
                    //---- db 저장 실패시 등 예외처리----//
                    $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
                    echo $s;

                    //#####################
                    // 망취소 API
                    //#####################

                    $netcancelResultString = ""; // 망취소 요청 API url(고정, 임의 세팅 금지)
                    
                    if ($httpUtil->processHTTP($netCancel, $authMap)) {
                        $netcancelResultString = $httpUtil->body;
                    } else {
                        echo "Http Connect Error\n";
                        echo $httpUtil->errormsg;

                        throw new Exception("Http Connect Error");
                    }

					echo "<br/>## 망취소 API 결과 ##<br/>";
					
					/*##XML output##*/
					//$netcancelResultString = str_replace("<", "&lt;", $$netcancelResultString);
					//$netcancelResultString = str_replace(">", "&gt;", $$netcancelResultString);
					
                    // 취소 결과 확인
                    echo "<p>". $netcancelResultString . "</p>";
                }
            } else {
				$paydata['pay_status'] = '9990';
				$paydata['c_modi_date'] = current_time('mysql');
				$paydata['pay_data'] = json_encode($_REQUEST);

				$wpdb->update("rm_pay_mst",$paydata , array( 'c_paynum' => $_REQUEST['orderNumber']) );
                
                echo "<script type='text/javascript'>
				<!--
					alert('결제가 취소 되었습니다');
					location.href='/service/point-charge-step2/';
				//-->
				</script>";
				exit;
            }
        } catch (Exception $e) {
            $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
            echo $s;
    }

	
}


function get_payment_info($o){
	
	global $wpdb;	

	$order_no = $o?$o:$_REQUEST['o'];

	$pay_result = $wpdb->get_row( "SELECT * FROM rm_pay_mst WHERE c_paynum = '".$order_no."' and c_idx_member = '".get_current_user_id()."'" , ARRAY_A  );	

	if( $pay_result['c_paytype'] == 'phone'){
		$pay_result['card_name'] = "휴대폰소액결제";
	}else if( $pay_result['c_paytype'] == 'trans'){
		$pay_result['card_name'] = "실시간계좌이체";
	}else{
		$pay_result['card_name'] = $pay_result['card_name'] ? $pay_result['card_name']  : "신용카드";
	}

	return $pay_result;

}

$point_mode = array("1"=>"포인트충전", "2"=>"포인트사용" , "3"=>"결제취소/차감" , "4"=>"구매취소/환불");

function rh_get_point_list(){	
	global $wpdb , $point_mode;	
    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-user_point-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
    $pagenation_div = 10;
	$post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
	$page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
	$offset = ( $page * $post_per_page ) - $post_per_page;
	$offset_limit = ( $page* $post_per_page ) - 1;
	$limit= " limit {$offset} , {$post_per_page} ";

	if($_REQUEST[c_type]){
		$where = " and c_type='".$_REQUEST[c_type]."' ";
	}

	$wpdb->get_results("SELECT * FROM rm_point_mst WHERE c_idx_member ='".get_current_user_id()."' ".$where." ");
    $rowcount = $wpdb->num_rows;

	$sql = "SELECT * FROM rm_point_mst where c_idx_member ='".get_current_user_id()."' ".$where." order by c_idx_point desc ".$limit;
	
	$query = $wpdb->get_results( $sql , ARRAY_A);
	$list = array();
	foreach( $query  as $key=> $row){
		$_row = array();
		$_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
		$_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
		$_row['amount'] = number_format($row['c_amount']);
		$_row['rest'] = number_format($row['c_rest']);
		$_row['gubun'] = $point_mode[$row['c_type']];
		$_row['giho'] = $row['c_type'] == '2'||$row['c_type'] == '3'?'-':'+';
		$_row['c_type'] = $row['c_type'];
		$_row['usedetail'] = "-";
		$list[] = $_row;
	}
	$pageinfo['total'] = $rowcount;
	$pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
	$pageinfo['per_page']	= $post_per_page;
	$pageinfo['page']	= $page;
	$pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
	$last = $page * $post_per_page;
	$pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];


	$result = array();
	$result['list'] = $list;
	$result['page'] = $pageinfo;

	echo json_encode($result);

	die();
}	


add_action('wp_ajax_rh_get_point_list', 'rh_get_point_list');


function rh_get_point_payment_list(){	
	global $wpdb , $point_mode;	
    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-user_point-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
    $pagenation_div = 10;
	$post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
	$page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
	$offset = ( $page * $post_per_page ) - $post_per_page;
	$offset_limit = ( $page* $post_per_page ) - 1;
	$limit= " limit {$offset} , {$post_per_page} ";

	$wpdb->get_results("SELECT * FROM rm_pay_mst WHERE c_idx_member ='".get_current_user_id()."'");
    $rowcount = $wpdb->num_rows;

	$sql = "SELECT * FROM rm_pay_mst where c_idx_member ='".get_current_user_id()."' order by c_idx_pay desc ".$limit;
	
	$query = $wpdb->get_results( $sql , ARRAY_A);

	$last_order = $wpdb->get_row( "SELECT * FROM rm_order_mst WHERE c_idx_member = '".get_current_user_id()."' and  order_state > 0 order by c_idx_od  desc" , ARRAY_A  );

	$list = array();
	foreach( $query  as $key=> $row){
		$_row = array();
		$_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
		$_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
		$_row['amount'] = number_format($row['c_payamount']);
		$_row['paymethod'] = $row['card_name']?$row['card_name']:" - ";
		$_row['c_paynum'] = $row['c_paynum'];
		$_row['pg_tid'] = $row['pg_tid'];

		if( "0000" == $row['pay_status']){
			$_row['state'] = '결제완료';
			$_row['cancelBtn'] = "취소불가";
		}else if( "9999" == $row['pay_status']){
			$_row['state'] = '결제 후 취소완료';
			$_row['cancelBtn'] = '<a href = "#" class="hgbtn grey01 delBtn" data-cancel-id="'.$row[c_paynum].'">삭제</button>';
		}else if( "9990" == $row['pay_status']){
			$_row['state'] = '결제 중 취소';
			$_row['cancelBtn'] = '<a href = "#" class="hgbtn grey01 delBtn" data-cancel-id="'.$row[c_paynum].'">삭제</button>';
		}else if(!$row['pay_status']){
			$_row['state'] = '결제오류';
			$_row['cancelBtn'] = '<a href = "#" class="hgbtn grey01 delBtn" data-cancel-id="'.$row[c_paynum].'">삭제</button>';
		}else{
			$_row['state'] = '';
		}
		if( "0000" == $row['pay_status'] && substr($row['c_reg_date'],0,10) == current_time("Y-m-d") && ( !$last_order || $last_order[c_reg_date] < $row['c_reg_date'] )){
			$_row['cancelBtn'] = '<a href = "#" class="hgbtn grey01 cancelBtn" data-cancel-id="'.$row[pg_tid].'">취소요청</button>';
		}else{
			//$_row['cancelBtn'] = $last_order[c_reg_date].":".$row['c_reg_date'];
		}
		

		
		
		
		$list[] = $_row;
	}
	$pageinfo['total'] = $rowcount;
	$pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
	$pageinfo['per_page']	= $post_per_page;
	$pageinfo['page']	= $page;
	$pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
	$last = $page * $post_per_page;
	$pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];
 

	$result = array();
	$result['list'] = $list;
	$result['page'] = $pageinfo;

	echo json_encode($result);

	die();
}	


add_action('wp_ajax_rh_get_point_payment_list', 'rh_get_point_payment_list');




function rh_get_order_list(){	
	global $wpdb , $point_mode;	
	$current_user = get_currentuserinfo();
    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-user_point-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
    $pagenation_div = 10;
	$post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
	$page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
	$offset = ( $page * $post_per_page ) - $post_per_page;
	$offset_limit = ( $page* $post_per_page ) - 1;
	$limit= " limit {$offset} , {$post_per_page} ";

	$wpdb->get_results("SELECT * FROM rm_order_mst WHERE c_idx_member ='".$current_user->ID."'");
    $rowcount = $wpdb->num_rows;

	$sql = "SELECT o.* , p.c_nm_pr , p.c_option as opt FROM rm_order_mst o left join rm_product_mst p on o.c_idx_pr = p.c_idx_pr where c_id_member ='".$current_user->user_login."' order by c_idx_od desc ".$limit;
	
	$query = $wpdb->get_results( $sql , ARRAY_A);
	$list = array();
	foreach( $query  as $key=> $row){
		$_row = array();
		$_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
		$_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
		$_row['amount'] = number_format($row['c_amount']);
		$_row['c_nm_pr'] = $row['c_nm_pr'];
		$_otionName = "";
		if( $row['c_option'] ){
			$options = explode("^^" , $row['c_option'] );

			if(sizeof($options) > 1 ){
				
				foreach ( $options as $o ){
					$item = explode(":" , $o);
					$_otionName = $item[2];
					break;
				}
				$_otionName .= " 외 ".(sizeof($options)-1);
			}else{
				$item = explode(":" , $row['c_option'] );
				$_otionName = $item[2];
			}
		}
		$_row['option'] = $_otionName;
		$_row['order_no'] = $row['c_od_num'];
		$_row['c_payment_price'] = $row['c_payment_price'];
		if($row['order_state'] == '0'){
			$_row['statusLabel'] = '취소완료';
		}else if($row['order_state'] == '1'){
			$_row['statusLabel'] = '주문완료';
		}else{
			$_row['statusLabel'] = $row['order_state'];
		}
		
		$_row['order_state'] = $row['order_state'];
		
		
		$list[] = $_row;
	}
	$pageinfo['total'] = $rowcount;
	$pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
	$pageinfo['per_page']	= $post_per_page;
	$pageinfo['page']	= $page;
	$pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
	$last = $page * $post_per_page;
	$pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];


	$result = array();
	$result['list'] = $list;
	$result['page'] = $pageinfo;

	echo json_encode($result);

	die();
}	


add_action('wp_ajax_rh_get_order_list', 'rh_get_order_list');

function rh_cancel_point_order(){	
	global $wpdb , $point_mode;	
	$user = get_user_info();
	$cancel_id = $_POST['cancel_id'];
	$del = $_POST['del'];

	$result = array();

	if($del){		
		$wpdb->delete( 'rm_pay_mst', array( 'c_paynum' => $cancel_id ) );
		$result[msg] = "삭제 되었습니다.";
		echo json_encode($result);
		die();
	}


	
	if($user[point] != $user[point2]){
		$result[msg] = "충전후 사용내역이 없는 경우에 한하여 취소하실 수 있습니다.";
	}else{
		$sql = "SELECT * FROM  `rm_pay_mst` where c_idx_member='".$user[ID]."' and pg_tid='".$cancel_id."'";
		$pay_info = $wpdb->get_row($sql);
		if($pay_info){
			$sql = "SELECT * FROM  rm_point_mst where c_idx_member='".$user[ID]."' and pay_mst_id='".$pay_info->c_idx_pay."'";
			$point_info = $wpdb->get_row($sql);
		}

		if( $pay_info && $point_info ){
			
			require_once($_SERVER[DOCUMENT_ROOT].'/stdpay/libs/INILib.php');

			$inipay = new INIpay50;

			$mid = "INIpayTest";
			$tid = $cancel_id;

			/* * *******************
			 * 3. 취소 정보 설정 *
			 * ******************* */
			$inipay->SetField("inipayhome", "/var/www/html_members/stdpay"); // 이니페이 홈디렉터리(상점수정 필요)
			$inipay->SetField("type", "cancel");                            // 고정 (절대 수정 불가)
			$inipay->SetField("debug", "true");                             // 로그모드("true"로 설정하면 상세로그가 생성됨.)
			$inipay->SetField("mid", $mid);                                 // 상점아이디
			/* * ************************************************************************************************
			 * admin 은 키패스워드 변수명입니다. 수정하시면 안됩니다. 1111의 부분만 수정해서 사용하시기 바랍니다.
			 * 키패스워드는 상점관리자 페이지(https://iniweb.inicis.com)의 비밀번호가 아닙니다. 주의해 주시기 바랍니다.
			 * 키패스워드는 숫자 4자리로만 구성됩니다. 이 값은 키파일 발급시 결정됩니다.
			 * 키패스워드 값을 확인하시려면 상점측에 발급된 키파일 안의 readme.txt 파일을 참조해 주십시오.
			 * ************************************************************************************************ */
			$msg = "고객 단순변심";
			$inipay->SetField("admin", "1111");
			$inipay->SetField("tid", $tid);                                 // 취소할 거래의 거래아이디
			$inipay->SetField("cancelmsg", $msg);                           // 취소사유

			/* * **************
			 * 4. 취소 요청 *
			 * ************** */
			$inipay->startAction();

			$result = array();
			$result['code'] = $inipay->getResult('ResultCode');
			if( $result['code'] == '00' ){
				$result['msg'] ='취소되었습니다.';
				
				$paydata = array();
				$paydata['pay_status'] ='9999';
				$paydata['c_modi_date'] = current_time("Y-m-d");
				

				$wpdb->update("rm_pay_mst",$paydata , array( 'pg_tid' => $cancel_id ) );

				$pointdata = array();
				$pointdata['c_idx_member'] = $user[ID];
				$pointdata['c_type'] = '3';
				$pointdata['c_amount'] = $pay_info->c_paypoint;
				$pointdata['pay_mst_id'] = $pay_info->c_idx_pay;
				$pointdata['c_reg_date'] = current_time('mysql');
				$c_rest = $user[point] - $pay_info->c_paypoint;
				$pointdata['c_rest'] = $c_rest;
							
							
				$wpdb->insert("rm_point_mst",$pointdata);

				$wpdb->update( 'rm_member_mst' , array( 'c_point' => $c_rest , 'c_last_charge_point_sum' => $c_rest ) , array( 'c_idx_member' => $user[ID] ));




			}else{
				$result['msg'] ='취소요청 실패 ('.$result['code'].')';
			}
			//$result['msg'] = $inipay->getResult('ResultMsg');

			//echo $inipay->getResult('ResultMsg');
			




		}else{
			$result[msg] = "충전 내역을 찾을 수 없습니다.관리자에게 문의해 주세요";
		}
	
	}
	

	echo json_encode($result);


	die();
}
add_action('wp_ajax_rh_cancel_point_order', 'rh_cancel_point_order');

function rh_cancel_prd_order(){	

	global $wpdb , $point_mode;	
	$user = get_user_info();
	$cancel_id = $_POST['cancel_id'];
	$result = array();
	$order = $wpdb->get_row( "SELECT * FROM rm_order_mst WHERE c_idx_member = '".$user[ID]."' and c_od_num='".$cancel_id."' order by c_idx_od  desc" , ARRAY_A  );

	if($order){
		if( $order['order_state'] == '0'){
			$result[msg] = "이미 취소한 주문입니다.";
		}else if( $order['order_state'] != '1' ){
			$result[msg] = "취소 할수 없습니다.\고객센터로 문의 바랍니다.";
		}else if( $order['api_key'] ){
			$result[msg] = "제휴 업체를 통해 구매한 상품은 제휴업체 사이트에서 취소 하실수 있습니다.";
		}else{
			
			$wpdb->update("rm_order_mst",array('c_modi_date'=>current_time('mysql') , 'order_state'=>'0'),array( 'c_idx_od'=> $order[c_idx_od] ) );

			$pointdata = array();
			$pointdata['c_idx_member'] = $user[ID];
			$pointdata['c_type'] = '4';
			$pointdata['c_amount'] = $order['c_payment_price'];
			$pointdata['order_mst_id'] = $order['c_idx_od'];
			$pointdata['c_reg_date'] = current_time('mysql');
			$c_rest = $user[point] + $order['c_payment_price'];
			$pointdata['c_rest'] = $c_rest;

			$wpdb->insert("rm_point_mst",$pointdata);

			$wpdb->update( 'rm_member_mst' , array( 'c_point' => $c_rest , 'c_last_charge_point_sum' => $c_rest ) , array( 'c_idx_member' => $user[ID] ));

			$result[msg] = "주문 취소가 완료 되었습니다.";

		}
	}else{
		$result[msg] = "주문내역을 찾을 수 없습니다.";
	}

	echo json_encode($result);

	die();


}

add_action('wp_ajax_rh_cancel_prd_order', 'rh_cancel_prd_order');