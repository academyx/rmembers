<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
$payinfo = get_payment_info($_REQUEST['o']);
get_header(); 

?>

<!-- 이니시스 표준결제 js -->
        <script language="javascript" type="text/javascript" src="https://stgstdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
        <!-- <script language="javascript" type="text/javascript" src="https://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script> -->

        <script type="text/javascript">
            function pay() {
                INIStdPay.pay('SendPayForm_id');
            }
        </script>


			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">R;</span>포인트 충전</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="pt_charge_wrap">
							<!-- 포인트 충전 타이틀 -->
							<div class="top_tit">
								<h4 class="tit"><span class="fc_org1">R;</span>포인트 충전</h4>
								<p class="txt1">르호봇 비지니스센터의 입주기업의 회원이라면 필요시 마다 멤버십사이트를 통해 R;포인트 충전이 가능합니다.</p>
							</div>
							<!-- //포인트 충전 타이틀 -->

							<!-- 충전 스텝 -->
							<div class="point_charge_step">
								<ol>
									<li class="">
										<div class="step1">
											<p class="tit">01. 로그인</p>
											<div class="line"></div>
											<p class="txt">멤버십 사이트 로그인</p>
										</div>
									</li>
									<li class="">
										<div class="step2">
											<p class="tit">02. 결제정보 입력</p>
											<div class="line"></div>
											<p class="txt">충전금액, 결제수단등을 입력하세요</p>
										</div>
									</li>
									<li class="">
										<div class="step3">
											<p class="tit">03. 결제</p>
											<div class="line"></div>
											<p class="txt">결제를 진행해 주세요</p>
										</div>
									</li>
									<li class="on">
										<div class="step4">
											<p class="tit">04. 충전완료</p>
											<div class="line"></div>
											<p class="txt">충전된 포인트 금액을<br />확인해 주세요</p>
										</div>
									</li>
								</ol>
							</div>
							<!-- //충전 스텝 -->

							<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="200px;" />
									<col width="" />
								</colgroup>

								<tbody>
									<tr>
										<th scope="row" class="tal">상품명</th>
										<td>르호봇 멤버십 R;포인트</td>
									</tr>
									
									<tr>
										<th scope="row" class="tal">결제금액</th>
										<td><?=number_format($payinfo[c_payamount])?> 원</td>
									</tr>

									<tr>
										<th scope="row" class="tal">충전포인트</th>
										<td><?=number_format($payinfo[c_payamount])?>  R;포인트</td>
									</tr>

									<tr>
										<th scope="row" class="tal">결제방법</th>
										<td><?=$payinfo[card_name]?></td>
									</tr>
								</tbody>
							</table>

							<p class="fs18 tac mt50">정상적으로 충전이 완료되었습니다.</p>
							
							<div class="ta_btn_area">
								<a href="/member/my_point/" class="hgbtn grey01 wsize1">My 포인트</a>
								<a href="/store/list" class="hgbtn org01 wsize1 ml10">R;스토어로 가기</a>
							</div>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
<?php get_footer(); ?>