<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

get_header(); 

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">R;</span>포인트 충전</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="pt_charge_wrap">
							<!-- 포인트 충전 타이틀 -->
							<div class="top_tit">
								<h4 class="tit"><span class="fc_org1">R;</span>포인트 충전</h4>
								<p class="txt1">르호봇 비지니스센터의 입주기업의 회원이라면 필요시 마다 멤버십사이트를 통해 R;포인트 충전이 가능합니다.</p>
							</div>
							<!-- //포인트 충전 타이틀 -->

							<!-- 충전 스텝 -->
							<div class="point_charge_step">
								<ol>
									<li class="on">
										<div class="step1">
											<p class="tit">01. 로그인</p>
											<div class="line"></div>
											<p class="txt">멤버십 사이트 로그인</p>
										</div>
									</li>
									<li class="">
										<div class="step2">
											<p class="tit">02. 결제정보 입력</p>
											<div class="line"></div>
											<p class="txt">충전금액, 결제수단등을 입력하세요</p>
										</div>
									</li>
									<li class="">
										<div class="step3">
											<p class="tit">03. 결제</p>
											<div class="line"></div>
											<p class="txt">결제를 진행해 주세요</p>
										</div>
									</li>
									<li class="">
										<div class="step4">
											<p class="tit">04. 충전완료</p>
											<div class="line"></div>
											<p class="txt">충전된 포인트 금액을<br />확인해 주세요</p>
										</div>
									</li>
								</ol>
							</div>
							<!-- //충전 스텝 -->

							<div class="chargr_info">
								<dl class="info1">
									<dt><span class="fc_org1">R;</span>포인트 충전 가능 결제 수단</dt>
									<dd>신용카드, 실시간 계좌이체, 핸드폰 소액결재 (3만원 이하)</dd>
								</dl>

								<dl class="info2">
									<dt><span class="fc_org1">R;</span>포인트 충전시 가산 포인트</dt>
									<dd>
										100,000원 이상 충전시 R;포인트 10% 가산<br />
										즉, 100,000원 충전시 110,000R포인트 충전
									</dd>
								</dl>

								<dl class="info3">
									<dt><span class="fc_org1">R;</span>포인트 결제 취소 및 환불 안내</dt>
									<dd>
										R;포인트 충전후 사용내역이 없는 경우에 한하여 ,충전당일 직접 충전을 취소 하실 수 있습니다. <br>
										<span class="fc_org1">충전취소 – 마이페이지 >> 마이포인트 에서 충전 취소 가능</span><br>
										환불 – R;포인트 최종 충전후 합계 잔액의 60% 이상을 사용한 경우에 한하여 환불 신청가능하고 신청후 1주일 이내 지정한 계좌로 입금해 드립니다.<br>
										<span class="fc_org1">환불 신청 – 마이페이지>>마이포인트>>환불신청</span>
									</dd>
								</dl>
							</div>

							<div class="tac">
								<a href="point-charge-step2" class="hgbtn grey01 wsize2">충전하러 가기</a>
							</div>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
<?php get_footer(); ?>