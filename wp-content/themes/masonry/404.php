<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

get_header(); 


?>


<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">4</span>04 Error</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- login_wrap -->
				<div class="login_wrap">

					<!-- login_box -->
					<div class="login_box">
						
						<p style='text-align:center'>페이지를 찾을 수 없습니다.</p>
						
					
					</div>
					<!-- //login_box -->
				</div>
				<!-- //login_box -->

				

			</section>
			<!-- //sub_article -->

		
		

	
<?php get_footer(); ?>