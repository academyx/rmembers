/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
		$(document).ready(function(){
    // Perform AJAX login on form submit
		
		$('form#login').bind('submit', function(e){
			
			

			var user_id = $('form#login #username').val();
			var user_pass = $('form#login #password').val();

			if(!user_id){
				rh_alert("아이디를 입력해 주세요!" , function(){ $('form#login #username').focus() });
				return false;
			}

			if(!user_pass){
				rh_alert("비밀번호를 입력해 주세요!" , function(){ $('form#login #password').focus() });
				return false;
			}


			$('form#login p.status').show().text(ajax_login_object.loadingmessage);
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_login_object.ajaxurl,
				data: { 
					'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
					'sns': $('form#login #sns').val(),
					'sns_id': $('form#login #sns_id').val(),
					'username': $('form#login #username').val(),
					'password': $('form#login #password').val(), 
					'security': $('form#login #security').val() },
				success: function(data){
					//$('form#login p.status').text(data.message);
					//console.log(data);
					if (data.loggedin == true){
						//alert(data.sns_user);
						document.location.href = ajax_login_object.redirecturl;
					}else{
						rh_alert("입력하신 로그인 정보가 일치하지 않습니다.");
					}
				}
			});
			e.preventDefault();
		});

		});


	
} )( jQuery );