/*
* autoLogoff.js
*
* Every valid navigation (form submit, click on links) should
* set this variable to true.
*
* If it is left to false the page will try to invalidate the
* session via an AJAX call
*/
var validNavigation = false;

/*
* Invokes the servlet /endSession to invalidate the session.
* No HTML output is returned
*/
function endSession() {
   //$.get("http://members.ibusiness.co.kr/wp-admin/admin-ajax.php?action=ajax_logout");
}

function wireUpEvents() {
  /*
  * For a list of events that triggers onbeforeunload on IE
  * check http://msdn.microsoft.com/en-us/library/ms536907(VS.85).aspx
  */
  window.onbeforeunload = function(e) {
      if (!validNavigation) {
         //endSession();
		 
		 if ( window.XMLHttpRequest )
    {
        console.log("before"); //alert("before");
        var request = new XMLHttpRequest();
        request.open("POST", "http://members.ibusiness.co.kr/wp-admin/admin-ajax.php?action=ajax_logout", true); 
        request.onreadystatechange = function () {
            if ( request.readyState == 4 && request.status == 200 )
            {
                console.log("Succes!"); 
				
            }
        };
        //request.send();
    }

		 //return false;
      }
  }
 
  // Attach the event keypress to exclude the F5 refresh
  window.onkeypress = function(e) {
    //if (e.keyCode == 116){
      validNavigation = true;
	  console.log("keypress !!");
    //}
  }
 
  // Attach the event click for all links in the page
  $("a").bind("click", function() {
    validNavigation = true;
  });
 
  // Attach the event submit for all forms in the page
  $("form").bind("submit", function() {
    validNavigation = true;
  });
 
  // Attach the event click for all inputs in the page
  $("input[type=submit]").bind("click", function() {
    validNavigation = true;
  });
   
}

// Wire up the events as soon as the DOM tree is ready
$(document).ready(function() {
    //wireUpEvents();  
	///console.log("autologoff init!!");
});