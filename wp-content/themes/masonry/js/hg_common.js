//lnb가 window height 크기로 나와야 할 경우
$(window).load(function(){
	$(".css_loader").fadeOut();
	$("#wrap").css({
		opacity : 1
	});

	// console.log($(".article_wrap").outerHeight());
	$(".lnb_wrap").css({
		"min-height" : $(window).height()
	});
});
$(function(){
/* start */
	//브라우저 업데이트 얼럿 노출
	brwAlert();
	fnHeader();

	if($(".faq_fable").length){
		fnFaq();
	}

	// 홀수/짝수
	$('.odd_list :odd').addClass("odd");
	$('.even_list :even').addClass("even");

	$('.ttg_list li:nth-child(3n)').addClass("nth3");

	//한글만 입력
	$(".onlyKo").keyup(function(event){
		var regexp = /[a-z0-9]|[ \[\]{}()<>?|`~!@#$%^&*-_+=,.;:\"'\\]/g;
		var inputVal = $(this).val();
		if( regexp.test(inputVal) ) {
			$(this).val(inputVal.replace(regexp,''));
		}
	});
	//영문만 입력
	$(".onlyEn").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z]/gi,''));
		}
	} );

	//숫자만 입력
	$(".onlyNum").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^0-9]/gi,''));
		}
	});
	//대문자 변경
	$(".uppercase").keyup(function(event){
		$( this ).val($( this ).val().toUpperCase());
	});

	$('.btn_top').on('click', function(e){
		e.preventDefault();

		var pos = $('#wrap').offset().top;

		$('html, body').animate({
			scrollTop:pos
		}, 500);
	});

/* end */
});

function fnHeader(){
	var $header = $("#header");
	var $dep1_btn = $("#gnb>li");
	var $dep2 = $("#gnb .dep2");
	var $dep2_btn = $("#gnb .dep2 > li");
	var $subm_bg = $(".subm_bg");

	$dep1_btn.on("mouseenter", function(){
		$dep2.fadeIn(250);
		$subm_bg.fadeIn(250);
	});
	$header.on("mouseleave", function(){
		$dep2.fadeOut(250);
		$subm_bg.fadeOut(250);
	});
};

// 텝
function tab1(){
	var ontab = $(".tab_btn ul > li.on a").attr("href");

	$(ontab).show();

	$(".tab_btn ul > li > a").on("click", function(e){
		e.preventDefault();

		var cts = $(this).attr("href");

		$(".tab_btn ul > li").removeClass("on");
		$(this).closest("li").addClass("on");

		$(".tab_cts").hide();
		$(cts).show();
	});
}
// 브라우저 업데이트 얼럿창
function brwAlert(){
	var $alertPop = $(".alert-brw-update");
	var $close = $(".btn-update-close");
	var $hid_check = $("input#hid_pop");

	$close.click(function(){
		$alertPop.slideUp(300);
	});

	$hid_check.click(function(){
		setCookie("alert-brw-update", "done", 1);

		$alertPop.slideUp(300);
	});
	checkCookie("alert-brw-update");
}
//쿠키 저장
function setCookie( name, value, expiredays ) {
	var todayDate = new Date();
	todayDate.setDate( todayDate.getDate() + expiredays );
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}
//쿠키 체크
function checkCookie(name){
	cookiedata = document.cookie;
	if ( cookiedata.indexOf(name+"=done") < 0 ){
		$("."+name).show();
	}
	else {
		$("."+name).hide();
	}
}

//ios 기기 체크
function iOS() {
	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];
	if (!!navigator.platform) {
		while (iDevices.length) {
			if (navigator.platform === iDevices.pop()){
				$("body").addClass("ios");
			}
		}
	}
	return false;
}

// 브라우저 체크
var Browser = {
	chk : navigator.userAgent.toLowerCase()
}
Browser = {
	ie : Browser.chk.indexOf('msie') != -1,
	ie6 : Browser.chk.indexOf('msie 6') != -1,
	ie7 : Browser.chk.indexOf('msie 7') != -1,
	ie8 : Browser.chk.indexOf('msie 8') != -1,
	ie9 : Browser.chk.indexOf('msie 9') != -1,
	ie10 : Browser.chk.indexOf('msie 10') != -1,
	opera : !!window.opera,
	safari : Browser.chk.indexOf('safari') != -1,
	safari3 : Browser.chk.indexOf('applewebkir/5') != -1,
	mac : Browser.chk.indexOf('mac') != -1,
	chrome : Browser.chk.indexOf('chrome') != -1,
	firefox : Browser.chk.indexOf('firefox') != -1
}
/*if (Browser.ie7) {
	console.log("ie");
};*/

// 모바일 기기 체크
/*var mfilter = "win16|win32|win64|mac|macintel";
if( navigator.platform  ){
	if( mfilter.indexOf(navigator.platform.toLowerCase())>0 ){
		// window.location.href='/';
	}else{
		// window.location.href='/';
	}
}*/

function ovlOpen(opacity, color){
	if (!opacity || opacity==null) {
		opacity = 5;
	};
	if (!color || color==null) {
		color = "000";
	};
	if ($(".ovl").length) {
		return false;
	};
	$("body").append("<div class='ovl'></div>");

	$(".ovl").css({
		"position": "fixed",
		"top": 0,
		"left": 0,
		"width": "100%",
		"height": "100%",
		"background": "#"+color,
		"opacity": "0."+opacity,
		"*filter": "alpha(opacity="+opacity+"0)",
		"-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity="+opacity+"0)",
		"z-index": 150
	});
}
function ovlClose(){
	$(".ovl").animate({
		opacity : 0
	}, function(){
		$(this).remove();
	});
}

// 레이어 팝업 오픈
function lypopOpen(id){
	ovlOpen();
	$(id).popCenter().show();
}
// 레이어 팝업 클로즈
function lypopClose(id){
	if (id == null) {
		$('.lypop').hide();
		ovlClose();
	}else {
		$(id).hide();
	};
}
// ajax 레이어 팝업 클로즈
function ajax_popClose(id){
	ovlClose();
	$(id).hide();
	$(id).remove();
}
function ajax_lypop(id){
	$(".ajax_loader").fadeIn(250);
	$.ajax({
		url: '/ajax_pop.html',
		type: 'GET',
		dataType: 'html',
		data: {pop_name: id}
	})
	.done(function(html) {
		$(".ajax_loader").fadeOut(250);
		if (!$(id).length) {
			$(html).appendTo("body");
		};
		ovlOpen();
		$(id).popCenter().show();

		if ($(id).outerHeight() > $(window).height()) {
			var pos = $(id).position().top;
			$("body, html").animate({
				scrollTop : pos
			});
		};
		$(window).resize(function(){
			$(id).popCenter();
		});
	})
	.fail(function() {
		console.log("error");
	});
}
/*
	레이어팝업 센터
	$("div.pop").popCenter().show();

	브라우저가 다 로드되고 실행 해야 됨
*/
jQuery.fn.popCenter = function () {
	this.css("position","absolute");
	this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
	this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");

	var pos = $(this).position().top;
	if ($(window).height() <= $(this).outerHeight()) {
		$("html, body").stop().animate({
			scrollTop : pos
		}, 250);
	};

	objThis = this;

	// 레이어 팝업 가로중앙 정렬
	$(window).resize(function(){
		$(objThis).css("left", Math.max(0, (($(window).width() - $(objThis).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
	});

	return this;
}

/*FAQ*/
function fnFaq(){
	var $q = $(".faq_fable tr.q");
	var $a = $(".faq_fable tr.a");

	$a.eq(0).show();
	$q.on("click", function(e){
		e.preventDefault();
		$a.hide();
		$(this).next("tr.a").show();
	});
}

;(function($){
	$.fn.lypop = function(options, callback){
		var opts = $.extend({}, $.fn.lypop.defaults, options);
		var $this = $(this);
		var deviceCheck = $.fn.lypop.deviceCheck();

		var objThis = this;

		if (typeof opts.callback == 'function') {
			opts.callback.call(this);
		}
		$.lypopFn = {};

		//단위 값에 문자열이 있으면 잘라서 리턴
		$.lypopFn.changeStr = function(key){
			var val = [];
			if(typeof(key) === "string"){
				$.each([ "em", "px", "%", "pt" ], function(idx, unit){
					if ( key.indexOf( unit ) > 0 ) {
						val = [parseFloat(key), unit];
					}
				});
				return val[0];
			}else {
				return key;
			}
		}

		//팝업 센터 정렬
		$.lypopFn.popCenter = function(el, w, h){
			var x = $.lypopFn.changeStr(w);
			var y = $.lypopFn.changeStr(h);

			el.css({
				top :  Math.max(0, ( ($(window).height() - y) / 2) + $(window).scrollTop()) + "px",
				left :  Math.max(0, ( ($(window).width() - x) / 2) + $(window).scrollLeft()) + "px"
			});
		};

		//팝업 센터 리사이즈
		$.lypopFn.popResize = function(el, w, h){
			// console.log(deviceCheck);
			var x = $.lypopFn.changeStr(w);
			var y = $.lypopFn.changeStr(h);

			if(deviceCheck == "pc"){
				el.css({
					top :  Math.max(0, ( ($(window).height() - y) / 2) + $(window).scrollTop()) + "px",
					left :  Math.max(0, ( ($(window).width() - x) / 2) + $(window).scrollLeft()) + "px"
				});
			}else {
				el.css({
					left :  Math.max(0, ( ($(window).width() - x) / 2) + $(window).scrollLeft()) + "px"
				});
			}
		};

		// 팝업이 브라우저보다 크면 스크롤
		$.lypopFn.posMove = function(el, h){
			//팝업이 브라우저 창보다 크면 상단으로 이동
			// console.log("sdlkfjsdlkfj");
			//console.log(h);
			if(h > $(window).height()){
				var pos = el.position().top;
				$("html, body").stop().animate({
					scrollTop : pos
				}, 250);
			}
		}
		//오버레이 오픈
		$.lypopFn.ovlOpen = function(opacity, color){
			if ($(".ovl").length) {
				return false;
			};
			$("body").append("<div class='ovl'></div>");

			$(".ovl").css({
				"position": "fixed",
				"top": 0,
				"left": 0,
				"width": "100%",
				"height": "100%",
				"background": "#"+opts.overlayColor,
				"opacity": "0."+opts.overlayOpacity,
				"*filter": "alpha(opacity="+opts.overlayOpacity+"0)",
				"-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity="+opts.overlayOpacity+"0)",
				"z-index": 150
			});
		};
		return this.each(function(){
			var $objThis = this;
			var popup_id = $(this).attr("class");

			//아이프레임 팝업
			if(opts.type == "iframe"){
				var $iframe = "";
				$iframe += '<div class="pop_ifrm_wrap" style="-webkit-overflow-scrolling: touch;">';
					$iframe += '<iframe class="iframe_'+popup_id+'" name="lypop_ifrm" title="" scrolling="'+opts.frameScroll+'" src="'+opts.src+'" width="100%;" height="100%" marginwidth="0" marginheight="0"  frameborder="0"></iframe>';
				$iframe += '</div>';
				$iframe += '<button type="button" data-pop="'+popup_id+'" class="hgbtn btn_lypop_close">팝업 닫기</button>';

				//닫기 버튼 바인드
				$(this).on("click", ".btn_lypop_close", function(){
					$.fn.lypop.lyClose(this, true);
					opts.closed();
				});

				//dom에 선택 팝업이 없는 경우
				if(!$(".lypop."+popup_id).length){
					$(this).appendTo("body").addClass("lypop");
					$(this).append($iframe);
				}else {
				//dom에 선택 팝업이 있는경우, 완전 삭제 하지 않은 경우
					$(".lypop."+popup_id).css({
						"opacity" : 1,
						"display" : "block"
					});
				}

				// 아이프레임 자동 height
				if(opts.frameAutoHeight != false){
					$this.css({
						"width" : opts.popWidth,
						"height" : opts.popHeight,
						"opacity" : 0
					});
					$("iframe.iframe_"+popup_id).load(function(){
						var iframeHeight = $("iframe.iframe_"+popup_id).contents().find('body')[0].scrollHeight;
						// console.log(iframeHeight);
						// console.log($(this));

						if(opts.scrollBox != false){
							$(".lypop."+popup_id).find(".pop_ifrm_wrap").css({
								"overflow-x": "hidden",
								"overflow-y": "auto",
								"width" : "100%",
								"height" : "100%"
							});
							$(this).attr("height", iframeHeight);
							$(".lypop."+popup_id).css({
								"height" : opts.popHeight
							});
						}else {
							$(this).attr("height", iframeHeight);
							$(".lypop."+popup_id).css({
								"height" : iframeHeight+10
							});
						}

						// 팝업 중앙정렬 실행
						var thisW = $this.outerWidth();
						var thisH = $this.outerHeight();
						if(opts.center != false){
							$.lypopFn.popCenter($this, thisW, thisH);

							var resizeEnd;
							$(window).resize(function(){
								// var iframeHeight = $("iframe.iframe_"+popup_id).contents().find('body')[0].scrollHeight;
								var iframeHeight2 = $("iframe.iframe_"+popup_id).contents().find('.pop_wrap').outerHeight();
								var thisW = $this.outerWidth();
								var thisH = $this.outerHeight();

								if(opts.scrollBox == false){
									clearTimeout(resizeEnd);

									// console.log($objThis);
									resizeEnd = setTimeout(function(){
										/*console.log("resize end");
										console.log("iframeHeight1 = "+iframeHeight+" / iframeHeight2 = "+iframeHeight2);*/
										$($objThis).css({
											"height" : iframeHeight2+10
										});
										$($objThis).find("iframe").attr({
											"height" : iframeHeight2
										});
									},100);
								}
								$.lypopFn.popResize($this, thisW, thisH);
							});
						}
						$.lypopFn.posMove($this, thisH);

						$(".lypop."+popup_id).css({
							"opacity" : 1,
							"display" : "block"
						});
					});
				}else {
					$this.css({
						"width" : opts.popWidth,
						"height" : opts.popHeight,
						"opacity" : 1
					});

					var thisW = $this.outerWidth();
					var thisH = $this.outerHeight();
					if(opts.center != false){
						$.lypopFn.popCenter($this, thisW, thisH);
						$(window).resize(function(){
							var thisW = $this.outerWidth();
							var thisH = $this.outerHeight();

							$.lypopFn.popResize($this, thisW, thisH);
						});
					}
					$.lypopFn.posMove($this, thisH);

					$(".lypop."+popup_id).css({
						"opacity" : 1,
						"display" : "block"
					});
				}

			}else if(opts.type == "normal"){
			// normal 팝업
				// console.log(opts.type);
				$(this).css({
					"width" : opts.popWidth,
					"height" : opts.popHeight,
					"display" : "block"
				}).show();

				// 팝업 중앙정렬 실행
				var thisW = $this.outerWidth();
				var thisH = $this.outerHeight();
				if(opts.center != false){
					$.lypopFn.popCenter($this, thisW, thisH);
					$(window).resize(function(){
						var thisW = $this.outerWidth();
						var thisH = $this.outerHeight();

						$.lypopFn.popResize($this, thisW, thisH);
					});
				}
				$.lypopFn.posMove($this, thisH);
			}

			if(opts.overlay  != false){
				$.lypopFn.ovlOpen(opts.overlayOpacity, opts.overlayColor);
			}
			opts.opened();
		});
	};

	$.fn.lypop.lyClose = function(el, remover){
		// console.log(el+"/"+remover);
		var $pop = $(".lypop");
		var $pop_length = ($pop.length - $(".lypop:hidden").length); // display: none인 팝업 제외

		// console.log($pop_length);

		if(remover != true){
			$(el).closest(".lypop").addClass("remove-false").hide();
		}else {
			$(el).closest(".lypop").hide(0, function(){
				$(this).remove();
			});
		}
		if($pop_length <= 1){
			$.fn.lypop.ovlClose();
		}
	};

	//오버레이 삭제
	$.fn.lypop.ovlClose = function(){
		$('.ovl', parent.document).css({
			opacity : 0
		}).remove();
	};

	// 모바일 기기 체크
	$.fn.lypop.deviceCheck = function(){
		var mfilter = "win16|win32|win64|mac|macintel";
		var device;
		if(navigator.platform){
			if(mfilter.indexOf(navigator.platform.toLowerCase())>0){
				device = "pc";
			}else{
				device = "m";
			}
		}
		return device;
	}
	//반응형
	$.fn.lypop.defaults = {
		type : "iframe",
		popWidth : "100%",
		popHeight : "auto",
		center : true,
		scrollBox : false,
		frameScroll : "no",
		frameAutoHeight : true,
		overlay : true,
		overlayOpacity : 5,
		overlayColor : "000",
		opened : function() {},
		closed : function() {}
	}
})(jQuery);

Number.prototype.format = function(){
    if(this==0) return 0;

    var reg = /(^[+-]?\d+)(\d{3})/;
    var n = (this + '');

    while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

    return n;
};

// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
String.prototype.format = function(){
    var num = parseFloat(this);
    if( isNaN(num) ) return "0";

    return num.format();
};

String.prototype.toNumber = function(){

    var num = Number(this.replace(/[^0-9\.]+/g,""));
    if( isNaN(num) ) return "0";

    return num;
};

function rh_redirect(action , params , target){
	if(params){
		$.each(params, function(index, value) {

			$("#redirectForm").append("<input type='hidden' name='"+index+"' value='"+value+"' />");
		});
	}

	if( target ){
		$("#redirectForm").attr("target", target );
	}

	$("#redirectForm").attr("action", action ).submit();
}


function set_list_page(pageinfo){

	//if( pageinfo.totalpage > 1 ){
		console.log(pageinfo);
		var pageDiv = $(".pagenate .page");

		pageDiv.html("");
		pageDiv.append('<a href="" title="맨처음페이지" class="page_btn first" data-page="1">&lt;&lt;</a>');
		var prevpage =  pageinfo.page-1 < 1 ? 1 : pageinfo.page-1;
		pageDiv.append('<a href="" title="이전리스트" class="page_btn prev"  data-page="'+prevpage+'" >&lt;</a>');

		for(var i = pageinfo.start ; i <= pageinfo.end ; i++ ){
			console.log(i+":"+pageinfo.page);
			if( i == pageinfo.page){
				pageDiv.append('<span class="page_num"><a class="on">'+i+'</a></span>');
			}else{
				pageDiv.append('<span class="page_num"><a href="#" class="" data-page="'+i+'" >'+i+'</a></span>');
			}


		}

		var nextpage = pageinfo.page+1 > pageinfo.totalpage ? pageinfo.totalpage :pageinfo.page+1;

		pageDiv.append('<a href="" title="다음리스트" class="page_btn next" data-page="'+nextpage+'">&gt;</a>');
		pageDiv.append('<a href="" title="맨 끝페이지" class="page_btn end" data-page="'+pageinfo.totalpage+'">&gt;&gt;</a>');

	//}


}

function rh_alert(msg , callback){
	
	$(".pop_alert").lypop({
		type : "normal",
		popWidth : 412
	}).find(".pop_msg").text(msg);
	$(".pop_alert").find(".alert_ok").focus()
	$(".pop_alert").find(".done").one("click",function(){
		if (callback)
		{
			if(callback == "reload"){			
				location.reload();
			}else {
				callback.call();
			}
		}
	});	

}

function rh_confirm(msg , callback){


}

function o(name){
	return $("input[name="+name+"]");
}