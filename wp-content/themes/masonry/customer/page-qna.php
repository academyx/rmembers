<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_check_init();

get_header(); ?>
            <!-- sub_article -->
            <section id="sub_article">
                <!-- 페이지 타이틀 -->
                <div class="page_tit_area">
                    <h2 class="sub_tit1"><span class="fc_org1">묻</span>고답하기</h2>
                </div>
                <!-- //페이지 타이틀 -->

                <!-- white box -->
                <div class="wh_box">
                    <!-- article inner -->
                    <article class="inner">
                        <!-- 게시판 검색 -->
                        <div class="bd_sh_box">
                            <ul class="ip_list1">
                                <li style="width:15%;">
                                    <select id="selSearchType" name="selSearchType" class="sel01" style="width:98%;">
                                        <option value="all">전체</option>
                                        <option value="title">제목</option>
                                    </select>
                                </li>
                                <li><input type="text" id="strSearchValue" name="strSearchValue" class="ip01" style="width:99%;" /></li>
                                <li style="width:15%;"><button type="button" id="btnSearch" name="btnSearch" class="hgbtn grey01 btn_smit" onclick="search_qna_list();">검색</button></li>
                            </ul>
                        </div>
                        <!-- //게시판 검색 -->
                        <form method="get" id="formQnaList">
                            <input type="hidden" id="idx" name="idx"/>
                            <table cellpadding="0" cellspacing="0" border="0" class="type1 list1" summary="" style="width:100%;">
                                <caption></caption>
                                <colgroup>
                                    <col width="80px" />
                                    <col width="250px" />
                                    <col width="" />
                                    <col width="120px" />
                                    <col width="120px" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">구분</th>
                                        <th scope="col">제목</th>
                                        <th scope="col">작성자</th>
                                        <th scope="col">등록일</th>
                                    </tr>
                                </thead>

                                <tbody id="ajax-result-list">
                                </tbody>
                            </table>
                        </form>

                        <!-- 페이징 -->
                        <div class="pagenate">
                            <div class="page">
                            </div>
                        </div>
                        <!-- //페이징 -->
                        <div>
                            <button type="button" id="btnWriteQna" name="btnWriteQna" class="hgbtn org01 wsize1 fr">글쓰기</button>
                        </div>
                    </article>
                    <!-- //article inner -->
                </div>
                <!-- //white box -->
            </section>
            <!-- //sub_article -->

<script>

    $(function (){

        get_qna_list(1);

        $( ".pagenate" ).on( "click", "a", function(e) {
            e.preventDefault();
            var _page = $(this).data("page");
            if( _page ){
                if( $("#ajax-result-list").data("page") != _page ) get_qna_list(_page);
            }
        });

        $("#btnWriteQna").click(function(){
            $(location).attr("href", "qna_write");
        });

    });

    function get_qna_list(page){

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_url,
            data: {
                'action': 'rh_get_qna_list',
                'page': page,
                'rType' : $("#selSearchType").val(),
                'rValue' : $("#strSearchValue").val()
            },
            success: function(data){
                var list_html = "";
                var reply_icon = "";
                
                if(data.list.length < 1) {
                    list_html += '<tr><td colspan="5">묻고답하기 내역이 존재하지 않습니다.</td></tr>';
                }
                else {
                    for (var i = 0; i < data.list.length; i++) {
                        if(data.list[i].reply_yn == "Y") 
                            reply_icon = "<span style='display:block; width:70px; border:1px #4d88ff solid; background:#4d88ff; text-align:center; float:right; margin-left:20px; color:#fff'>답변완료</span>";

                        list_html += '<tr>'
                            + '<td class="">' + data.list[i].rowIndex + '</td>'
                            + '<td class="">' + data.list[i].category + '</td>'
                            + '<td class="tac pl20 pr20"><a href="qna_view/?idx='+data.list[i].idx+'" style="float:left">' + data.list[i].title + reply_icon + '</a></td>'
                            + '<td class="">' + data.list[i].writer + '</td>'
                            + '<td class="">' + data.list[i].date + '</td>'
                            + '</tr>';

                        reply_icon = "";
                    }
                }

                $("#ajax-result-list").html(list_html);
                $("#ajax-result-list").data("page",page);
                set_list_page(data.page);


            }
        });
    }
</script>

<?php get_footer(); ?>