<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_get_notice_one();
get_header();
?>

    <!-- sub_article -->
    <section id="sub_article">
        <!-- 페이지 타이틀 -->
        <div class="page_tit_area">
            <h2 class="sub_tit1"><span class="fc_org1">공</span>지사항</h2>
        </div>
        <!-- //페이지 타이틀 -->

        <!-- white box -->
        <div class="wh_box">
            <!-- article inner -->
            <article class="inner">
                <form method="post" id="formNoticeView">
                    <input type="hidden" id="idx" name="idx" value="<?=$board_info->c_idx_board?>"/>
                    <!-- 게시판 타이틀 -->
                    <div class="bd_view_tit">
                        <h4 class="tit"><?=$board_info->c_board_title?></h4>
                    </div>
                    <!-- //게시판 타이틀 -->
                    <table cellpadding="0" cellspacing="0" border="0" class="type1 view1" summary="" style="width:100%;">
                        <caption></caption>
                        <colgroup>
                            <col width="120px" />
                            <col width="" />
                            <col width="120px" />
                            <col width="400px" />
                        </colgroup>


                        <tbody>
                        <tr>
                            <th scope="row" class="">작성자</th>
                            <td class="tal"><?=$board_info->c_writer_id?></td>
                            <th scope="row" class="">작성일</th>
                            <td class=""><?=date("Y-m-d", strtotime($board_info->c_reg_date))?></td>
                        </tr>
                        <tr>
                            <td class="bd_cts" colspan="4">
                                <div style="width:100%; min-height:150px !important;">
                                    <?=stripslashes($board_info->c_board_context)?>
                                </div>
                            </td>
                        </tr>
    <?php
            if($board_info->c_file_idx1 != null)
            {
    ?>
                <tr>
                    <th scope="row" class="">첨부파일1</th>
                    <td class="tal" colspan="3"><a href="#" onclick="file_down(<?=$board_info->c_file_idx1?>);"><?=$board_info->c_file_orgname1?></a></td>
                </tr>
    <?php
            }
    ?>
    <?php
            if($board_info->c_file_idx2 != null)
            {
    ?>
                <tr>
                    <th scope="row" class="">첨부파일2</th>
                    <td class="tal" colspan="3"><a href="#"><?=$board_info->c_file_orgname2?></a></td>
                </tr>
    <?php
            }
    ?>

                        </tbody>
                    </table>

                    <div class="ta_btn_area">
                        <div class="fl">
<?php
                            if($board_info->prev_idx != null)
                            {
?>
                            <a href="notice_view/?idx=<?=$board_info->prev_idx?>" class="hgbtn grey01 wsize1 btn_view_prev"><span class="ico_arl">이전글</span></a>
<?php
                            }
                            if($board_info->next_idx != null)
                            {
?>
                            <a href="notice_view/?idx=<?=$board_info->next_idx?>" class="hgbtn grey01 wsize1 btn_view_next"><span class="ico_arr">다음글</span></a>
<?php
                            }
?>
                        </div>
                        <div class="fr">
                            <a href="notice" class="hgbtn org01 wsize1">목록</a>
                        </div>
                    </div>
                </form>
            </article>
            <!-- //article inner -->
        </div>
        <!-- //white box -->
    </section>
    <!-- //sub_article -->


    <script>
        $(function(){

        });

    </script>
<?php get_footer(); ?>