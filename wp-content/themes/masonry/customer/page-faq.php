<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
get_header(); ?>
            <!-- sub_article -->
            <section id="sub_article">
                <!-- 페이지 타이틀 -->
                <div class="page_tit_area">
                    <h2 class="sub_tit1"><span class="fc_org1">자</span>주묻는질문</h2>
                </div>
                <!-- //페이지 타이틀 -->

                <!-- white box -->
                <div class="wh_box">
                    <!-- article inner -->
                    <article class="inner">
                        <!-- 게시판 검색 -->
                        <div class="bd_sh_box">
                            <ul class="ip_list1">
                                <li style="width:15%;">
                                    <select id="selSearchType" name="selSearchType" class="sel01" style="width:98%;">
                                        <option value="all">전체</option>
                                        <option value="title">제목</option>
                                    </select>
                                </li>
                                <li><input type="text" name="strSearchValue" id="strSearchValue" class="ip01" style="width:99%;" /></li>
                                <li style="width:15%;"><button type="button" id="btnSearch" name="btnSearch"  class="hgbtn grey01 btn_smit" onclick="search_faq_list();">검색</button></li>
                            </ul>
                        </div>
                        <!-- //게시판 검색 -->

                        <!-- faq tab -->
                        <div class="faq_tab">
                            <ul>
                                <li class="color1"><a href="#" onclick="get_faq_list(1, 0);return false">전체</a></li>
                                <li class="color2"><a href="#" onclick="get_faq_list(1, 1);return false">멤버스이용안내</a></li>
                                <li class="color3"><a href="#" onclick="get_faq_list(1, 2);return false">취소및환불</a></li>
                                <li class="color4"><a href="#" onclick="get_faq_list(1, 3);return false">회원가입및인증</a></li>
                                <li class="color5"><a href="#" onclick="get_faq_list(1, 4);return false">사이트이용</a></li>
                            </ul>
                        </div>
                        <!-- //faq tab -->

                        <table cellpadding="0" cellspacing="0" border="0" class="faq_fable" summary="" style="width:100%;">
                            <caption></caption>
                            <colgroup>
                                <col width="70px" />
                                <col width="200px" />
                                <col width="" />
                                <col width="120px" />
                            </colgroup>

                            <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">구분</th>
                                <th scope="col">제목</th>
                                <th scope="col">등록일</th>
                            </tr>
                            </thead>

                            <tbody id="ajax-result-list">


                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div class="pagenate">
                            <div class="page">
                            </div>
                        </div>
                        <!-- //페이징 -->

                    </article>
                    <!-- //article inner -->
                </div>
                <!-- //white box -->
            </section>
            <!-- //sub_article -->

<script>
    $(function(){

        get_faq_list(1, 0);

        $( ".pagenate" ).on( "click", "a", function(e) {
            e.preventDefault();
            var _page = $(this).data("page");
            var _cate = $(this).data("cate");


            if( _page ){
                if( $("#ajax-result-list").data("page") != _page ) get_faq_list(_page, _cate);
            }
        });

    });

    function get_faq_list(page, cate){

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_url,
            data: {
                'action': 'rh_get_faq_list',
                'page': page,
                'cate': cate,
                'rType' : $("#selSearchType").val(),
                'rValue' : $("#strSearchValue").val()
            },
            success: function(data){
                var list_html = "";

                if(data.list.length < 1) {
                    list_html += '<tr><td colspan="4">자주묻는 질문이 존재하지 않습니다.</td></tr>';
                }
                else {
                    for (var i = 0; i < data.list.length; i++) {
                        list_html += '<tr class="q" id="q_'+ data.list[i].rowIndex +'">'
                                        + '<td class="">' + data.list[i].rowIndex + '</td>'
                                        + '<td class="">' + data.list[i].category + '</td>'
                                        + '<td class="title pl20 pr20"><a href="#" onclick="show_answer(' + data.list[i].rowIndex + '); return false;" class="txt">' + data.list[i].title + '</a></td>'
                                        + '<td class="">' + data.list[i].date + '</td>'
                                    + '</tr>'
                                    + '<tr class="a" id="a_'+data.list[i].rowIndex +'">'
                                        + '<td class="">&nbsp;</td>'
                                        + '<td class=""><p class="a_tit tac">A</p></td>'
                                        + '<td class="" colspan="2"><div class="a_cts">' +  data.list[i].context + '</div></td>'
                                    + '</tr>';
                    }
                }

                $("#ajax-result-list").html(list_html);
                $("#ajax-result-list").data("page",page);
                $("#ajax-result-list").data("cate",cate);
                set_list_page(data.page);


            }
        });
    }

    function show_answer(num)
    {
       if($("#a_" + num).css("display") == "none")
       {
           $("#a_" + num).show();
       }
       else
       {
           $("#a_" + num).hide();
       }
    }

    function search_faq_list()
    {
        if($.trim($("#strSearchValue").val()) == "")
        {
            rh_alert("검색어를 입력하세요.");
        }
        else
        {
            var _cate = $(this).data("cate");

            get_faq_list(1, _cate);
        }
    }

</script>



<?php get_footer(); ?>