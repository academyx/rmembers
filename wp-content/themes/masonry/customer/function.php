<?php
// First check the nonce, if it fails the function will break
//check_ajax_referer( 'ajax-user_point-nonce', 'security' );
// Nonce is checked, get the POST data and sign user on
    function rh_user_check_init(){

        $user_id = get_current_user_id();

        if(!$user_id){
            wp_redirect( '/login/' );
            exit;
        }else{
            get_user_info();
        }
    }

    function rh_get_notice_list()
    {
        global $wpdb ;

        $pagenation_div = 10;
        $post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
        $page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
        $offset = ( $page * $post_per_page ) - $post_per_page;
        $offset_limit = ( $page* $post_per_page ) - 1;
        $limit= " limit {$offset} , {$post_per_page} ";

        $subSql = "(1=1 ";

        if($_POST['rValue'] != "")
        {
            if ($_POST['rType'] == "all")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else if ($_POST['rType'] == "title")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else
                $subSql .= "";
        }

        $subSql .= "  AND a.c_board_type = 'notice' AND a.c_del_date IS NULL) ";


        $wpdb->get_results("SELECT a.* FROM rm_board_mst a WHERE ".$subSql);
        $rowcount = $wpdb->num_rows;

        $sql = "SELECT a.* FROM rm_board_mst a WHERE " . $subSql . "ORDER BY a.c_idx_board DESC ".$limit;

        $query = $wpdb->get_results( $sql , ARRAY_A);
        $list = array();

        foreach( $query  as $key=> $row)
        {
            $_row = array();
            $_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
            $_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
            $_row['title'] = $row['c_board_title'];
            $_row['cnt'] = number_format($row['c_cnt']);
            $_row['noticeYN'] = $row['c_use_notice'];
            $_row['idx'] = $row['c_idx_board'];
            $list[] = $_row;
        }

        $pageinfo['total'] = $rowcount;
        $pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
        $pageinfo['per_page']	= $post_per_page;
        $pageinfo['page']	= $page;
        $pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
        $last = $page * $post_per_page;
        $pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];


        $result = array();
        $result['list'] = $list;
        $result['page'] = $pageinfo;

        echo json_encode($result);

        die();
    }

    add_action('wp_ajax_rh_get_notice_list', 'rh_get_notice_list');
    add_action('wp_ajax_nopriv_rh_get_notice_list', 'rh_get_notice_list');


    function rh_get_notice_one()
    {

        global $wpdb, $board_info;
        $board_info = $wpdb->get_row( "SELECT a.*,
                                        b.c_file_idx as c_file_idx1, b.c_file_url as c_file_url1, b.c_file_orgname as c_file_orgname1, b.c_file_newname as c_file_newname1,
                                        c.c_file_idx as c_file_idx2, c.c_file_url as c_file_url2, c.c_file_orgname as c_file_orgname2, c.c_file_newname as c_file_newname2
                                      FROM rm_board_mst a 
                                      LEFT OUTER JOIN rm_file b ON a.c_file1 = b.c_file_idx AND b.c_del_date IS NULL 
                                      LEFT OUTER JOIN rm_file c ON a.c_file2 = c.c_file_idx  AND c.c_del_date IS NULL
                                      WHERE a.c_idx_board = '".$_GET['idx']."'" );

        $arr_update = array();
        $arr_update['c_cnt'] = $board_info->c_cnt + 1;

        $wpdb->update( 'rm_board_mst' , $arr_update,  array( 'c_idx_board' => $_GET['idx'] ) );

        $board_prev = $wpdb->get_row( "SELECT a.c_idx_board
                                        FROM rm_board_mst a 
                                        WHERE a.c_idx_board < '".$board_info->c_idx_board."' AND
                                        a.c_board_type = 'notice' AND a.c_del_date IS NULL
                                        ORDER BY a.c_idx_board DESC LIMIT 1" );
        $board_next = $wpdb->get_row( "SELECT a.c_idx_board
                                        FROM rm_board_mst a 
                                        WHERE a.c_idx_board > '".$board_info->c_idx_board."' AND
                                        a.c_board_type = 'notice' AND a.c_del_date IS NULL
                                        ORDER BY a.c_idx_board ASC LIMIT 1" );

        $board_info->prev_idx = ($board_prev == null) ? null : $board_prev->c_idx_board;
        $board_info->next_idx = ($board_next == null) ? null : $board_next->c_idx_board;


    }

    add_action('wp_ajax_rh_get_notice_one', 'rh_get_notice_one');
    add_action('wp_ajax_nopriv_rh_get_notice_one', 'rh_get_notice_one');



    function rh_get_faq_list()
    {
        global $wpdb ;

        $pagenation_div = 10;
        $post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
        $page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
        $offset = ( $page * $post_per_page ) - $post_per_page;
        $offset_limit = ( $page* $post_per_page ) - 1;
        $limit= " limit {$offset} , {$post_per_page} ";

        $subSql = "(1=1 ";

        if($_POST['rValue'] != "")
        {
            if ($_POST['rType'] == "all")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else if ($_POST['rType'] == "title")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else
                $subSql .= "";
        }

        if($_POST['cate'] != 0)
        {
            $subSql .= " AND a.c_board_cate = '" . $_POST['cate'] . "'";
        }

        $subSql .= "  AND a.c_board_type = 'faq' AND a.c_del_date IS NULL) ";


        $wpdb->get_results("SELECT a.* FROM rm_board_mst a WHERE ".$subSql);
        $rowcount = $wpdb->num_rows;

        $sql = "SELECT a.* FROM rm_board_mst a WHERE " . $subSql . "ORDER BY a.c_idx_board DESC ".$limit;

        $query = $wpdb->get_results( $sql , ARRAY_A);
        $list = array();

        foreach( $query  as $key=> $row)
        {
            if($row['c_board_cate'] == 1)
                $strCate = "맴버스이용안내";
            else if($row['c_board_cate'] == 2)
                $strCate = "취소 및 환불";
            else if($row['c_board_cate'] == 3)
                $strCate = "회원가입 및 인증";
            else if($row['c_board_cate'] == 4)
                $strCate = "사이트이용";
            else
                $strCate = "기타";

            $_row = array();
            $_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
            $_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
            $_row['category'] = $strCate;
            $_row['title'] = $row['c_board_title'];
            $_row['context'] = stripslashes($row['c_board_context']);
            $_row['cnt'] = number_format($row['c_cnt']);
            $_row['idx'] = $row['c_idx_board'];
            $list[] = $_row;
        }

        $pageinfo['total'] = $rowcount;
        $pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
        $pageinfo['per_page']	= $post_per_page;
        $pageinfo['page']	= $page;
        $pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
        $last = $page * $post_per_page;
        $pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];


        $result = array();
        $result['list'] = $list;
        $result['page'] = $pageinfo;

        echo json_encode($result);

        die();
    }

    add_action('wp_ajax_rh_get_faq_list', 'rh_get_faq_list');
    add_action('wp_ajax_nopriv_rh_get_faq_list', 'rh_get_faq_list');


    function rh_get_qna_list()
    {
        global $wpdb ;
        $current_user = get_currentuserinfo();

        $pagenation_div = 10;
        $post_per_page = $_REQUEST['per_page']?$_REQUEST['per_page']:10;
        $page = isset( $_REQUEST['page'] ) ? abs( (int) $_REQUEST['page'] ) : 1;
        $offset = ( $page * $post_per_page ) - $post_per_page;
        $offset_limit = ( $page* $post_per_page ) - 1;
        $limit= " limit {$offset} , {$post_per_page} ";

        $subSql = "(1=1 ";

        if($_POST['rValue'] != "")
        {
            if ($_POST['rType'] == "all")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else if ($_POST['rType'] == "title")
                $subSql .= " AND a.c_board_title like '%" . $_POST['rValue'] . "%'";
            else
                $subSql .= "";
        }

        $subSql .= "  AND a.c_board_type = 'qna' AND a.c_del_date IS NULL) ";


        $wpdb->get_results("SELECT a.*, c.c_board_context as c_reply_context, c.c_idx_board as c_reply_idx FROM rm_board_mst a LEFT OUTER JOIN rm_board_mst c ON a.c_idx_board = c.c_parent_idx  WHERE ".$subSql);
        $rowcount = $wpdb->num_rows;

        $sql = "SELECT a.*, c.c_board_context as c_reply_context, c.c_idx_board as c_reply_idx FROM rm_board_mst a LEFT OUTER JOIN rm_board_mst c ON a.c_idx_board = c.c_parent_idx  WHERE " . $subSql . "ORDER BY a.c_idx_board DESC ".$limit;

        $query = $wpdb->get_results( $sql , ARRAY_A);
        $list = array();

        foreach( $query  as $key=> $row)
        {
            if($row['c_board_cate'] == 1)
                $strCate = "맴버스이용안내";
            else if($row['c_board_cate'] == 2)
                $strCate = "취소 및 환불";
            else if($row['c_board_cate'] == 3)
                $strCate = "회원가입 및 인증";
            else if($row['c_board_cate'] == 4)
                $strCate = "사이트이용";
            else
                $strCate = "기타";

            $_row = array();
            $_row['rowIndex'] = ($rowcount - $key) - ($post_per_page*($page-1));
            $_row['date'] = str_replace("-",".",substr($row['c_reg_date'],0,10));
            $_row['category'] = $strCate;
            $_row['title'] = $row['c_board_title'];
            $_row['writer'] = $row['c_writer_id'];
            $_row['idx'] = $row['c_idx_board'];
            $_row['reply_yn'] = ($row['c_reply_idx'] != null) ? "Y": "N";
            $list[] = $_row;
        }

        $pageinfo['total'] = $rowcount;
        $pageinfo['totalpage']	= ceil($rowcount/$post_per_page);
        $pageinfo['per_page']	= $post_per_page;
        $pageinfo['page']	= $page;
        $pageinfo['start']	= floor(($page-1)/$pagenation_div)*$pagenation_div + 1;
        $last = $page * $post_per_page;
        $pageinfo['end']	= $last < $pageinfo['totalpage'] ? $last : $pageinfo['totalpage'];


        $result = array();
        $result['list'] = $list;
        $result['page'] = $pageinfo;

        echo json_encode($result);

        die();
    }

    add_action('wp_ajax_rh_get_qna_list', 'rh_get_qna_list');


    function rh_get_qna_one()
    {

        global $wpdb, $board_info;
        $board_info = $wpdb->get_row( "SELECT a.*,
                                        CASE WHEN a.c_board_cate = 1 THEN '맴버스 이용안내'
                                         WHEN a.c_board_cate = 2 THEN '취소 및 환불'
                                         WHEN a.c_board_cate = 3 THEN '회원가입 및 인증'
                                         WHEN a.c_board_cate = 4 THEN '사이트 이용'
                                         ELSE '-' END as c_board_cate_str,
                                        b.c_file_idx as c_file_idx1, b.c_file_url as c_file_url1, b.c_file_orgname as c_file_orgname1, b.c_file_newname as c_file_newname1,
                                        c.c_board_context as c_reply_context, c.c_idx_board as c_reply_idx
                                      FROM rm_board_mst a 
                                      LEFT OUTER JOIN rm_file b ON a.c_file1 = b.c_file_idx AND b.c_del_date IS NULL 
                                      LEFT OUTER JOIN rm_board_mst c ON a.c_idx_board = c.c_parent_idx 
                                      WHERE a.c_idx_board = '".$_GET['idx']."'" );


        if($board_info->c_writer_id != wp_get_current_user()->user_login)
        {
            wp_redirect_mgs('/qna/', '열람할 권한이 없습니다.');
        }
        else
        {
            $wpdb->update( 'rm_board_mst' , array('c_cnt' => $board_info->c_cnt + 1 ),  array( 'c_idx_board' => $_GET['idx'] ) );
        }
    }



    function rh_set_qna_insert()
    {
        global $wpdb;
        // First check the nonce, if it fails the function will break
        check_ajax_referer( 'ajax_rh_set_qna_insert', 'security' );
        // Nonce is checked, get the POST data and sign user on

        $fileAttach = $_FILES['file_attach'];

        $arrQnaData = array(
            'c_board_type' => 'qna',
            'c_board_title' => $_POST['title'],
            'c_board_cate' => $_POST['cate'],
            'c_board_context' => $_POST['contents'],
            'c_pwd' => $_POST['pwd'],
            'c_writer_id' => wp_get_current_user()->user_login,
            'c_reg_date' => date("Y-m-d H:i:s"),
            'c_modi_date' => date("Y-m-d H:i:s")
        );

        $bResult = $wpdb->insert( 'rm_board_mst' , $arrQnaData);

        if($bResult == true)
        {
            sendEmail("7" ,
                            array(  "USER_ID" =>wp_get_current_user()->user_login ,
                                    "TITLE" => $_POST['title'] ,
                                    "REG_TIME" => date("Y.m.d H:i:s") ,
                                    "CONTENTS"=> $_POST['contents'] ,
                                    "receiveMailAddr" => get_bloginfo('admin_email') ) ,null);

            echo json_encode(array('result'=>true, 'message'=>__('등록되었습니다.')));
        }
        else
        {
            echo json_encode(array('result'=>false, 'message'=>__('등록에 실패했습니다. 잠시 후 다시 시도해 주세요.')));
        }

        die();
    }

    add_action('wp_ajax_rh_set_qna_insert', 'rh_set_qna_insert');



    function rh_set_qna_delete()
    {
        global $wpdb;

        $bResult = $wpdb->delete( 'rm_board_mst' , array('c_idx_board' => $_POST['idx']));

        if($bResult == true)
        {
            echo json_encode(array('result'=>true, 'message'=>__('삭제되었습니다.')));
        }
        else
        {
            echo json_encode(array('result'=>false, 'message'=>__('삭제에 실패했습니다. 잠시 후 다시 시도해 주세요.')));
        }

        die();
    }

    add_action('wp_ajax_rh_set_qna_delete', 'rh_set_qna_delete');



    function rh_set_qna_update()
    {
        global $wpdb;
        // First check the nonce, if it fails the function will break
        check_ajax_referer( 'ajax_rh_set_qna_update', 'security' );
        // Nonce is checked, get the POST data and sign user on
        $arrQnaData = array(
            'c_board_type' => 'qna',
            'c_board_title' => $_POST['title'],
            'c_board_cate' => $_POST['cate'],
            'c_board_context' => $_POST['contents'],
            'c_writer_id' => wp_get_current_user()->user_login,
            'c_reg_date' => date("Y-m-d H:i:s"),
            'c_modi_date' => date("Y-m-d H:i:s")
        );

        $board_pwd = $wpdb->get_var( "SELECT c_pwd FROM rm_board_mst WHERE c_idx_board = '".$_POST['idx']."'");

        if($board_pwd != $_POST['strQnaPwd'])
        {
            echo json_encode(array('result'=>false, 'message'=>__('비밀번호가 일치하지 않습니다.')));
        }
        else
        {
            $bResult = $wpdb->update( 'rm_board_mst' , $arrQnaData, array('c_idx_board' => $_POST['idx']));

            if($bResult == true)
            {
                echo json_encode(array('result'=>true, 'message'=>__('저장되었습니다.')));
            }
            else
            {
                echo json_encode(array('result'=>false, 'message'=>__('저장에 실패했습니다. 잠시 후 다시 시도해 주세요.')));
            }
        }

        die();
    }

    add_action('wp_ajax_rh_set_qna_update', 'rh_set_qna_update');
?>