<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
get_header(); ?>


<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">공</span>지사항</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<!-- 게시판 검색 -->
						<div class="bd_sh_box">
							<ul class="ip_list1">
								<li style="width:15%;">
									<select id="selSearchType" name="selSearchType" class="sel01" style="width:98%;">
										<option value="all">전체</option>
										<option value="title">제목</option>
									</select>
								</li>
								<li><input type="text" id="strSearchValue" name="strSearchValue" class="ip01" style="width:99%;" /></li>
								<li style="width:15%;"><button type="button" id="btnSearch" name="btnSearch" class="hgbtn grey01 btn_smit" onclick="search_notice_list();">검색</button></li>
							</ul>
						</div>
						<!-- //게시판 검색 -->
						<form method="get" id="formNoticeList">
							<input type="hidden" id="idx" name="idx"/>
							<table cellpadding="0" cellspacing="0" border="0" class="type1 list1" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="80px" />
									<col width="" />
									<col width="100px" />
									<col width="100px" />
								</colgroup>

								<thead>
									<tr>
										<th scope="col">No</th>
										<th scope="col">제목</th>
										<th scope="col">등록일</th>
										<th scope="col">조회</th>
									</tr>
								</thead>

								<tbody id="ajax-result-list">
									<!--<tr>
										<td class="">1</td>
										<td class="tal"><a href="notice_view.php" class="txt"><span class="txt_notice">[공지]</span>멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈멤버십 사이트 가오픈</a></td>
										<td class=""><a href="#"><img src="/images/ico_file.png" alt="첨부파일" /></a></td>
										<td class="">2016.10.10</td>
										<td class="">1111</td>
									</tr>-->
								</tbody>
							</table>
						</form>
						<!-- 페이징 -->
						<div class="pagenate">
							<div class="page">
							</div>
						</div>
						<!-- //페이징 -->
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>


<script>
	$(function(){

		get_notice_list(1);

		$( ".pagenate" ).on( "click", "a", function(e) {
			e.preventDefault();
			var _page = $(this).data("page");
			if( _page ){
				if( $("#ajax-result-list").data("page") != _page ) get_notice_list(_page);
			}
		});

	});

	function get_notice_list(page){

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajax_url,
			data: {
				'action': 'rh_get_notice_list',
				'page': page,
				'rType' : $("#selSearchType").val(),
				'rValue' : $("#strSearchValue").val()
			},
			success: function(data){
				var list_html = "";

				if(data.list.length < 1) {
					list_html += '<tr><td colspan="4">공지사항이 존재하지 않습니다.</td></tr>';
				}
				else {
					for (var i = 0; i < data.list.length; i++) {
						list_html += '<tr>'
							+ '<td class="">' + data.list[i].rowIndex + '</td>'
							+ '<td class="title pl20 pr20"><a href="notice_view/?idx='+data.list[i].idx+'">' + data.list[i].title + '</a></td>'
							+ '<td class="">' + data.list[i].date + '</td>'
							+ '<td class="">' + data.list[i].cnt + '</td>'
							+ '</tr>';
					}
				}

				$("#ajax-result-list").html(list_html);
				$("#ajax-result-list").data("page",page);
				set_list_page(data.page);


			}
		});
	}

	function search_notice_list()
	{
		if($.trim($("#strSearchValue").val()) == "")
		{
			rh_alert("검색어를 입력하세요.");
		}
		else
		{
			get_notice_list(1);
		}
	}

</script>


<?php get_footer(); ?>