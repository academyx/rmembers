<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header();

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">M</span>y 포인트</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box mypoint_result">
					<!-- article inner -->
					<article class="inner">
						<p class="pic"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_point.png" alt="" /></p>
						<?=$current_user->display_name?> 님 보유 R;포인트는
						<span class="txt_point fc_org1 point"><?=number_format($current_user_extra->c_point)?></span>
						R;포인트 입니다
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="ta_top_area">
							<h3 class="subj_tit1 fl">포인트 충전/사용 내역</h3>
							<div class="fr pt5">
								<label class="" for="sh_all"><input type="radio" id="sh_all" name="sh_type" onclick="get_list()"> 전체</label>
								<label class="ml10" for="sh_point_recharge"><input type="radio" id="sh_point_recharge" name="sh_type" onclick="get_list(1)"> 포인트충전</label>
								<label class="ml10" for="sh_point_use"><input type="radio" id="sh_point_use" name="sh_type" onclick="get_list(2)"> 포인트사용</label>
							</div>
						</div>
						<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb10" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="80px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="100px" />
							</colgroup>

							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">날짜</th>
									<th scope="col">구분</th>
									<th scope="col">R;포인트</th>
									<th scope="col">사용내역</th>
									<th scope="col">잔여R;포인트</th>
								</tr>
							</thead>

							<tbody id="ajax-result-list">
								
								
							</tbody>
						</table>

						<div style="position: absolute;z-index:999">
							<a href="/member/my-point-payment" class="hgbtn grey01 btn_smit">포인트 결제 취소</a>
							<a href="/member/my-point-refund" class="hgbtn org01 btn_smit">포인트 환불요청</a>
						</div>


						<div class="pagenate">
							<div class="page">
								
							</div>
						</div>

						

					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>

			<form method="post" action="" id="ajax-list">
				<input type="hidden" name="action" value="rh_get_point_list">
				<input type="hidden" name="page" value="1">
				<input type="hidden" name="c_type" value="">
			</form>

			

								

		
		<script type="text/javascript">
			<!--
				function get_list(c_type){
					$("#ajax-list").find("input[name=c_type]").val(c_type);
					get_point_list(1);
				}
				function get_point_list(page){
					
					$("#ajax-list").find("input[name=page]").val(page);

					$.ajax({
							type: 'POST',
							dataType: 'json',
							url: ajax_url,
							data: $( "#ajax-list" ).serialize(),
							success: function(data){
									
									var list_html = "";
									var rowIndex = data.page.total * ((data.page - 1)*data.page.per_page) ;
									for(var i = 0 ; i < data.list.length ; i++){
										
										list_html += '<tr>'
											+'<td class="">'+data.list[i].rowIndex+'</td>'
											+'<td class="">'+data.list[i].date+'</td>'
											+'<td class="">'+data.list[i].gubun+'</td>'
											+'<td class="point number_'+data.list[i].c_type+'">'+data.list[i].giho+''+data.list[i].amount+'</td>'
											+'<td class="">'+data.list[i].usedetail+'</td>'
											+'<td class="point">'+data.list[i].rest+'</td>'
										+'</tr>';									

									}

									$("#ajax-result-list").html(list_html);
									$("#ajax-result-list").data("page",page);
									set_list_page(data.page);
								
							}});

				}
				( function( $ ) {

					

					$(document).ready(function(){	
						get_point_list(1);

						$( ".pagenate" ).on( "click", "a", function(e) {
							e.preventDefault();
							var _page = $(this).data("page");
							if( _page ){
								if( $("#ajax-result-list").data("page") != _page ) get_point_list(_page);
							}
						});

					});
				
				} )( jQuery );

			//-->
			</script>

			<!-- //sub_article -->
		<?php get_footer(); ?>