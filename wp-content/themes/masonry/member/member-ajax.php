<?php


function ajax_login_init(){

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/ajax-login-script.js', array('jquery') ); 
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}

function ajax_logout(){

	session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);

	wp_clear_auth_cookie();
	wp_logout();
	
	wp_redirect( '/' );

	die();
}

add_action( 'wp_ajax_ajax_logout', 'ajax_logout' );
add_action( 'wp_ajax_nopriv_ajax_logout', 'ajax_logout' );

function ajax_login(){
	global $wpdb;
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );
	

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
  //  $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
	
    if ( is_wp_error($user_signon) ){       
	    echo json_encode(array('loggedin'=>false, 'message'=>__('아이디 패스워드가 일치하지 않습니다.')));
    } else {
		
		if($_POST['sns']){
			update_user_meta( $user_signon->ID , $_POST['sns']."_sns_id" , $_POST['sns_id'] );
		}

		set_session_cookie( $user_signon->ID );

		$wpdb->update( 'rm_member_mst' , array( 'last_login_time'=> current_time('mysql') ) , array( 'c_idx_member' => $user_signon->ID ) );

        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...') , 'sns_user' => $user_signon->ID ));
    }

    die();
}


function rh_ajax_register(){	
	global $wpdb;
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['user_login'] = sanitize_user($_POST['user_id']) ;
    $info['user_pass'] = sanitize_text_field($_POST['user_pass']);
	$info['user_email'] = sanitize_email( $_POST['email']);
	$info['display_name'] = $_POST['display_name'];

	

	// Register the user
	$user_register = wp_insert_user( $info );
	
	if ( is_wp_error($user_register) ){	
		$error  = $user_register->get_error_codes()	;
		
		if(in_array('empty_user_login', $error))
			echo json_encode(array('loggedin'=>false, 'message'=>__($user_register->get_error_message('empty_user_login'))));
		elseif(in_array('existing_user_login',$error))
			echo json_encode(array('loggedin'=>false, 'message'=>__('아이디가 이미 사용중입니다.')));
		elseif(in_array('existing_user_email',$error))
		echo json_encode(array('loggedin'=>false, 'message'=>__('이메일이 이미 사용중입니다.')));
	} else {
		
		$wpdb->delete( 'rm_member_mst', array( 'user_login' => $info['user_nicename'] ) );

		$wpdb->insert( 'rm_member_mst' , array( 'c_idx_member' => $user_register ,'user_login' => $info['user_nicename'] , 'c_email_yn' => $_POST['c_email_yn'] , 'c_reg_date' => current_time('mysql') , 'last_login_time'=> current_time('mysql') ) );


		$loginInfo = array();
		$loginInfo['user_login'] = $_POST['user_id'];
		$loginInfo['user_password'] = $_POST['user_pass'];
		$loginInfo['remember'] = true;

		if($_POST['sns']){
			update_user_meta( $user_register , $_POST['sns']."_sns_id" , $_POST['sns_id'] );
		}

		$user_signon = wp_signon( $loginInfo, false );

		set_session_cookie( $user_signon->ID );

		sendEmail("3" , array("USER_ID"=>$_POST['user_id'] , "USER_NAME" => $info['display_name'] , "REG_DATE" => date("Y.m.d") , "receiveMailAddr" => $info['user_email'] ));

		echo json_encode(array('loggedin'=>true, 'message'=>__('회원가입 성공.'), 'redirect'=>'/member/join_ok' ));

	  //auth_user_login($info['nickname'], $info['user_pass'], 'Registration');       
	}
	
	
 
    die();
}


add_action( 'wp_ajax_nopriv_ajax_register', 'rh_ajax_register' );

function rh_ajax_user_update(){	
	global $wpdb;	
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-user_update-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['user_login'] = sanitize_user($_POST['user_id']) ;
    $info['user_pass'] = sanitize_text_field($_POST['user_pass']);
	$info['user_email'] = sanitize_email( $_POST['email']);
	$info['display_name'] = $_POST['display_name'];

	
	$update_info['ID'] = get_current_user_id();
		
	if($_POST['user_pass'])$update_info['user_pass'] = sanitize_text_field($_POST['user_pass']);
	if($_POST['email']){
		$update_info['user_email'] = sanitize_email( $_POST['email']);
		//$wpdb->update( 'wp_users' , array( 'user_email' => $update_info['user_email'] ) , array( 'ID' => $update_info['ID'] ) );
	}
	
	$user_ext_data = array();
	if($_POST['c_sms_yn'])$user_ext_data['c_sms_yn'] = $_POST['c_sms_yn'];
	if($_POST['c_email_yn'])$user_ext_data['c_email_yn'] = $_POST['c_email_yn'];
	$user_ext_data['c_modi_date'] = current_time('mysql');

	$wpdb->update( 'rm_member_mst' , $user_ext_data , array( 'c_idx_member' => $update_info['ID'] ) );

	$user_register = wp_update_user( $update_info );

	echo json_encode($user_register);

	die();
}	


add_action('wp_ajax_rh_user_update', 'rh_ajax_user_update');


function rh_user_id_exists()
{
    global $wpdb;

	$user = $_POST[user_id];
	
    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE user_login = %s", $user));
	$result = array();
	$result['user_id'] = $user;
    if($count == 1){ 
		$result['result'] = '1';
		echo json_encode($result); 
	}else{ 
		$result['result'] = '0';
		echo json_encode($result); 
	}
    die;

}

add_action('wp_ajax_nopriv_user_id_exists', 'rh_user_id_exists');



function rh_ajax_user_cert(){
	global $wpdb;	
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-user_update-nonce', 'security' );
    // Nonce is checked, get the POST data and sign user on

	$bResult = $wpdb->update( 'rm_member_mst' , array( 'c_birth' => $_POST['u_birth'] , 'c_gender' => $_POST['u_gender'] , 'c_hp' => $_POST['utel0']."-".$_POST['utel1']."-".$_POST['utel2'], 'c_sms_yn' => $_POST['sms_check'], 'c_state' => 2, 'c_ci' => $_POST['cert_ci'], 'c_modi_date' => current_time('mysql')) , array( 'c_idx_member' => wp_get_current_user()->ID ) );
	
	if($bResult == true)
		echo json_encode(array('result'=>true, 'message'=>__('완료되었습니다.')));
	else
		echo json_encode(array('result'=>false, 'message'=>__('인증에 실패하였습니다.')));
   /*		

	$user_register = wp_update_user( $update_info );

	echo json_encode(array('result'=>true, 'message'=>__('인증되었습니다.')));
 */

	die();
}	


add_action('wp_ajax_rh_user_cert', 'rh_ajax_user_cert');



function rh_user_dropout()
{
	global $wpdb;
	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-user_update-nonce', 'security' );
	// Nonce is checked, get the POST data and sign user on
	$user_id = get_current_user_id();

	$current_user_extra = $wpdb->get_row( "SELECT a.*, if(b.c_idx_relay is null, -1, b.c_idx_relay) as c_idx_relay, b.c_idx_resi, b.c_level, if(b.c_level is null, null, b.c_level ) as c_level_str, b.c_state as c_accept_state 
											FROM rm_member_mst a 
											LEFT OUTER JOIN rm_relay_mst b ON a.c_idx_member = b.c_idx_member 
											WHERE a.c_idx_member = '".$user_id."'" );

	$member_cnt = -1;

	if($current_user_extra->c_idx_resi != null)
	{
		$member_cnt = $wpdb->get_var( "SELECT	count(a.c_idx_relay) as cnt
									   FROM		rm_relay_mst a
									   WHERE		a.c_idx_resi = '".$current_user_extra->c_idx_resi."' AND 
													a.c_idx_member !='".$user_id."'");
	}


	if($current_user_extra->c_level == "0" && $member_cnt > 0 )
		echo json_encode(array('result'=>false, 'message'=>__('대표회원은 대표권한을 위임 후 탈퇴하실 수 있습니다.'), 'loc'=>'comp'));
	else
	{
		try
		{
			$wpdb->query('START TRANSACTION');

			//---------------------------------------------------------------------------------------------------
			// 회원 개인이력의 delete 테이블 임시저장
			//---------------------------------------------------------------------------------------------------
			$bResult = $wpdb->query("INSERT INTO wp_users_delete SELECT d.*, SYSDATE() FROM wp_users d WHERE d.ID='".$user_id."'");

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 0: 회원 이력 오류발생'), 'loc'=>'mem'));
				exit();
			}

			$bResult = $wpdb->query("INSERT INTO rm_order_mst_delete SELECT a.*, SYSDATE() FROM rm_order_mst a WHERE a.c_idx_member='".$user_id."'");

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 1: 회원 이력 오류발생'), 'loc'=>'mem'));
				exit();
			}


			$bResult = $wpdb->query("INSERT INTO rm_pay_mst_delete SELECT b.*, SYSDATE() FROM rm_pay_mst b WHERE b.c_idx_member='".$user_id."'");

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 2: 회원 이력 오류발생'), 'loc'=>'mem'));
				exit();
			}

			$bResult = $wpdb->query("INSERT INTO rm_point_mst_delete SELECT c.*, SYSDATE() FROM rm_point_mst c WHERE c.c_idx_member='".$user_id."'");

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 3: 회원 이력 오류발생'), 'loc'=>'mem'));
				exit();
			}

			//---------------------------------------------------------------------------------------------------
			// 회원 개인이력의 삭제
			//---------------------------------------------------------------------------------------------------
			$bResult = $wpdb->delete( 'rm_order_mst', array( 'c_idx_member' => $user_id ) );

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 1: 이력 삭제 중 오류발생'), 'loc'=>'mem'));
				exit();
			}

			$bResult = $wpdb->delete( 'rm_pay_mst', array( 'c_idx_member' => $user_id ) );

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 2: 이력 삭제 중 오류발생'), 'loc'=>'mem'));
				exit();
			}

			$bResult = $wpdb->delete( 'rm_point_mst', array( 'c_idx_member' => $user_id ) );

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 3 이력 삭제 중 오류발생'), 'loc'=>'mem'));
				exit();
			}


			//---------------------------------------------------------------------------------------------------
			// 회원 정보의 삭제
			//---------------------------------------------------------------------------------------------------
			//회사 탈퇴
			$bResult = $wpdb->delete( 'rm_relay_mst', array( 'c_idx_member' => $user_id ) );

			if($bResult === false)
			{
				$wpdb->query('ROLLBACK');
				echo json_encode(array('result'=>false, 'message'=>__('Error 1: 회원 기업 탈퇴중 오류'), 'loc'=>'mem'));
			}
			else
			{
				//회원 상세정보 탈퇴
				$bResult = $wpdb->delete( 'rm_member_mst', array( 'c_idx_member' => $user_id ) );

				if($bResult === false)
				{
					$wpdb->query('ROLLBACK');
					echo json_encode(array('result'=>false, 'message'=>__('Error 2: 회원 탈퇴중 오류'), 'loc'=>'mem'));
				}
				else
				{
					//회원 기본정보 탈퇴 1
					$bResult = $wpdb->delete( 'wp_usermeta', array( 'user_id' => $user_id ) );

					if($bResult === false)
					{
						$wpdb->query('ROLLBACK');
						echo json_encode(array('result'=>false, 'message'=>__('Error 3: 기본회원 탈퇴중 오류'), 'loc'=>'mem'));
					}
					else
					{
						//회원 기본정보 탈퇴 2
						$bResult = $wpdb->delete( 'wp_users', array( 'ID' => $user_id ) );

						if($bResult === false)
						{
							$wpdb->query('ROLLBACK');
							echo json_encode(array('result'=>false, 'message'=>__('Error 3: 기본회원 탈퇴중 오류'), 'loc'=>'mem'));
						}
						else
						{
							//임직원 없을 시 회사삭제
							if($member_cnt == 0)
							{
								$bResult = $wpdb->delete( 'rm_resicomp_mst', array( 'c_idx_resi' => $current_user_extra->c_idx_resi ) );
							}

							if($bResult === 0 || $bResult > 0)
							{
								$wpdb->query('COMMIT');
								echo json_encode(array('result' => true, 'message' => __('회원탈퇴가 정상적으로 처리되었습니다.')));
							}
						}
					}
				}
			}

		}
		catch(Exception $e)
		{
			$wpdb->query('ROLLBACK');
			echo json_encode(array('result'=>false, 'message'=>__('잠시 에러가 발생했습니다.'.$bResult)));
		}

	}
	die();
}

add_action('wp_ajax_rh_user_dropout', 'rh_user_dropout');




function get_user_info(){
	
	global $wpdb, $current_user , $current_user_extra;
	$user_id = get_current_user_id();
	
	$current_user = wp_get_current_user();
	
	$current_user_extra = $wpdb->get_row( "SELECT a.*, if(b.c_idx_relay is null, -1, b.c_idx_relay) as c_idx_relay, b.c_idx_resi, b.c_level, if(b.c_level is null, null, b.c_level ) as c_level_str, b.c_state as c_accept_state 
											FROM rm_member_mst a 
											LEFT OUTER JOIN rm_relay_mst b ON a.c_idx_member = b.c_idx_member 
											WHERE a.c_idx_member = '".$user_id."'" );

	$user = array();
	$user[ID] = $user_id;
	$user[user_id] = $current_user->user_login;
	$user[name] = $current_user->user_nicename;
	$user[email] = $current_user->user_email;
	$user[birth] = $current_user_extra->c_birth ;
	$user[hp] = $current_user_extra->c_hp;
	$user[point] = $current_user_extra->c_point;
	$user[point2] = $current_user_extra->c_last_charge_point_sum;
	$user[state] = $current_user_extra->c_state;
	$user[ci] = $current_user_extra->c_ci?true:false;

	return $user;
	
}


function rh_user_page_init(){

	global $wpdb, $current_user , $current_user_extra;
	$user_id = get_current_user_id();
	if(!$user_id){
		wp_redirect( '/login/' );
		exit;
	}else{
		get_user_info();
	}
}

function wp_check_sns_user(){

	global $wpdb;
		
	$sns = $_POST['sns'];	
	$meta_value = $_POST['sns_id'];
	$meta_key = $sns."_sns_id";	

	if($sns == 'fb'){

		$sns_user = $wpdb->get_row( "SELECT * FROM {$wpdb->usermeta} WHERE meta_key = '".$meta_key."' and meta_value = '".$meta_value."' order by umeta_id desc limit 1");	
		
		if( $sns_user && get_user_by('id', $sns_user->user_id)){
			wp_set_auth_cookie($sns_user->user_id);
			set_session_cookie($sns_user->user_id);
			$wpdb->update( 'rm_member_mst' , array( 'last_login_time'=> current_time('mysql') ) , array( 'c_idx_member' => $sns_user->user_id ) );
			wp_redirect( '/' );
			exit;
		}
	}
}

function get_center_list($nIdx = null){

	global $wpdb;

	$arrCenterList = $wpdb->get_results("SELECT * FROM rm_center_mst WHERE c_del_date IS NULL");
	$htmlCenterSelectBox  = "<select id='sel_center' name='sel_center' class='sel01' style='width:200px;'>";
	$htmlCenterSelectBox .= "<option value=''>선택해 주세요 </option>";
		
	if(count($arrCenterList) > 0)
	{
		foreach($arrCenterList as $key => $rows)
		{
			if($nIdx != null)
				$htmlCenterSelectBox .= "<option value='".$rows->c_idx_center."' ".($rows->c_idx_center == $nIdx  ? 'selected' : '' ). ">".$rows->c_nm_center."</option>";
			else
				$htmlCenterSelectBox .= "<option value='".$rows->c_idx_center."'>".$rows->c_nm_center."</option>";
		}
	}

	$htmlCenterSelectBox .= "</select>";

 
	echo $htmlCenterSelectBox;


}

function rh_my_company_init(){

	global $wpdb, $my_company;
	
	
	$user_id = get_current_user_id();
	if( $user_id )
	{
		$my_company = $wpdb->get_row( "	SELECT		a.*
									FROM		rm_resicomp_mst a
									INNER JOIN	rm_relay_mst b ON a.c_idx_resi = b.c_idx_resi
									WHERE		b.c_level = 0 AND 
												a.c_del_date IS NULL AND
												b.c_del_date IS NULL AND
												b.c_idx_member = '".$user_id."'" );
	}

	return $my_company;
}

function check_ci_exists($ci){
	global $wpdb;
	$user = $wpdb->get_row( "SELECT * FROM rm_member_mst WHERE c_ci = '".$ci."'");	

	return $user;
}

function check_user_level(){
	global $wpdb;
	$owner = 0;
	$user_id = get_current_user_id();
	
	if($user_id){

	$row = $wpdb->get_row( "	SELECT		a.* , b.c_idx_member , b.c_level , m.c_ci , 
									FROM		rm_resicomp_mst a
									INNER JOIN	rm_relay_mst b ON a.c_idx_resi = b.c_idx_resi
									INNER JOIN	rm_member_mst m ON b.c_idx_member = m.c_idx_member

									WHERE		a.c_del_date IS NULL AND
												b.c_del_date IS NULL AND
												b.c_idx_member = '".$user_id."'" );
		//var_dump($row);

	}
	
	return $owner;

}

