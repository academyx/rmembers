<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header(); 
?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">회</span>원정보 수정</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1">기본정보</h3>
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
								<col width="380px" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="uid">아이디</label></th>
									<td class="" colspan="2">
										<input type="text" name="" id="uid" class="ip01 readonly1" readonly="readonly" value="slsl" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="upw">비밀번호</label></th>
									<td class="" colspan="2">
										<input type="password" name="" id="upw" class="ip01" style="width:265px;" />
										<span class="ta_stxt1 ml10">영문,숫자포함 8~12자리 입력해주세요</span>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="upw_chck">비밀번호 확인</label></th>
									<td class="" colspan="2">
										<input type="password" name="" id="upw_chck" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="uname">이름</label></th>
									<td class="">
										<input type="text" name="" id="uname" class="ip01 readonly1" readonly="readonly" value="홍길동" />
									</td>
									<td rowspan="3" class="cc_area">
										<!-- 본인인증 -->
										<p>전화번호 변경, 개명등으로 수정이 필요하신 분은<br />다시한번 본인인증을 거쳐서 수정하실수 있습니다.</p>
										<button type="button" class="hgbtn grey01 hsize48" style="width:98%;">본인인증하기</button>
										<!-- //본인인증 -->
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="utel">연락처</label></th>
									<td class="">
										<input type="text" name="" id="utel" class="ip01 readonly1" readonly="readonly" value="010-1234-5678" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="u_birth">생년월일</label></th>
									<td class="">
										<input type="text" name="" id="u_birth" class="ip01 readonly1" readonly="readonly" value="2010.01.01" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="umail1">이메일</label></th>
									<td class="" colspan="2">
										<input type="text" name="" id="umail1" class="ip01" style="width:265px;" />
										@
										<input type="text" name="" id="umail2" class="ip01" style="width:170px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal">이메일수신여부</th>
									<td class="" colspan="2">
										<label for="mail_yes"><input type="radio" name="mail_check" id="mail_yes" class="radio_size14" /> 수신</label>
										<label for="mail_no" class="ml10"><input type="radio" name="mail_check" id="mail_no" class="radio_size14" /> 수신 거부</label>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal">SMS수신여부</th>
									<td class="" colspan="2">
										<label for="sns_yes"><input type="radio" name="sns_check" id="sns_yes" class="radio_size14" /> 수신</label>
										<label for="sns_no" class="ml10"><input type="radio" name="sns_check" id="sns_no" class="radio_size14" /> 수신 거부</label>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="join_agr_box m0 mb15"><label for="cp_info_check"><input type="checkbox" name="" id="cp_info_check" /> 기업정보 입력하기</label></div>

						<!-- txtBox1 -->
						<div class="txt_box1">
							<p class="txt1">기업정보를 입력해 주시면 포인트를 이용한 충전및 할인의 다양한 혜택을 이용해 주실수 있습니다.</p>
						</div>
						<!-- //txtBox1 -->

						<div class="ta_btn_area">
							<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">신규등록</button>
							<button type="button" class="hgbtn org01 hsize48 ml10" style="width:225px;">기존회원 추가</button>
						</div>

						<!-- 컷트라인 -->
						<div class="cut_line"><span class="ico_cut"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_cut.png" /></span></div>
						<!-- //컷트라인 -->
						<!-- 신규회원 등록 -->
						<div class="pl35 pr35">
							<h3 class="subj_tit1 mb10">신규회원 등록하기</h3>
							<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

							<table cellpadding="0" cellspacing="0" border="0" class="type1 mb35" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="200" />
									<col width="" />
								</colgroup>

								<tbody>
									<tr>
										<th scope="row" class="tal"><label for="uid">아이디</label></th>
										<td class="">
											<label for="id_type1"><input type="radio" name="id_type" id="id_type1" class="radio_size14" /> 법인사업자</label>
											<label for="id_type2" class="ml10"><input type="radio" name="id_type" id="id_type2" class="radio_size14" /> 개인사업자</label>
											<label for="id_type3" class="ml10"><input type="radio" name="id_type" id="id_type3" class="radio_size14" /> 프리랜서</label>
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal"><label for="cp_name">회사명</label></th>
										<td class="">
											<input type="test" name="" id="cp_name" class="ip01" style="width:265px;" />
											<span class="ta_stxt1 ml10">사업자 등록증에 기재된 회사명을 입력해 주세요</span>
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal">사업자번호</th>
										<td class="">
											<label for="cp_num1"><input type="text" name="" id="cp_num1" class="ip01" style="width:133px;" /></label>
											<label for="cp_num2"><input type="text" name="" id="cp_num2" class="ip01" style="width:133px;" /></label>
											<label for="cp_num3"><input type="text" name="" id="cp_num3" class="ip01" style="width:133px;" /></label>
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal"><label for="ceo">대표자</label></th>
										<td class="">
											<input type="text" name="" id="ceo" class="ip01" style="width:265px;" />
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal"><label for="sel_center">입주센터선택</label></th>
										<td class="">
											<select name="" class="sel01" style="width:200px;">
												<option value="">선택해 주세요 </option>
											</select>
											<input type="text" name="" id="sel_center" class="ip01"  style="width:265px;" />
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- //신규회원 등록 -->
						<!-- 컷트라인 -->
						<div class="cut_line mb50"></div>
						<!-- //컷트라인 -->

						<!-- 컷트라인 -->
						<div class="cut_line"><span class="ico_cut"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_cut.png" /></span></div>
						<!-- //컷트라인 -->
						<!-- 기존회원 추가하기 -->
						<div class="pl35 pr35">
							<h3 class="subj_tit1 mb10">기존회원 추가하기</h3>
							<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

							<!-- 검색 박스 -->
							<div class="sh_box1">
								<table cellpadding="0" cellspacing="0" border="0" class="" summary="" style="width:100%;">
									<caption></caption>
									<colgroup>
										<col width="120px" />
										<col width="300px" />
										<col width="" />
									</colgroup>

									<tbody>
										<tr>
											<th scope="row">기업검색</th>
											<td>
												<label for="ch_cp_name"><input type="radio" name="sh_type" id="ch_cp_name" class="radio_size14" /> 회사명</label>
												<label for="ch_ceo" class="ml10"><input type="radio" name="sh_type" id="ch_ceo" class="radio_size14" /> 대표자명</label>
												<label for="ch_email" class="ml10"><input type="radio" name="sh_type" id="ch_email" class="radio_size14" /> 이메일</label>
											</td>
											<td>
												<input type="text" name="" id="" class="ip01" style="width:300px;" />
												<button type="button" class="hgbtn grey01 btn_smit">검색</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //검색 박스 -->

							<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="70px" />
									<col width="" />
									<col width="" />
									<col width="" />
									<col width="" />
									<col width="" />
									<col width="" />
									<col width="100px" />
								</colgroup>

								<thead>
									<tr>
										<th scope="col">No</th>
										<th scope="col">아이디(이메일)</th>
										<th scope="col">회사명/구분</th>
										<th scope="col">대표자명</th>
										<th scope="col">연락처</th>
										<th scope="col">사업자번호</th>
										<th scope="col">센터구분<br /><span class="fs14">(동/호수)</span></th>
										<th scope="col">승인요청</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>1</td>
										<td class="">아이디(이메일)</td>
										<td class="">회사명/구분</td>
										<td class="">대표자명</td>
										<td class="">연락처</td>
										<td class="">사업자번호</td>
										<td class="">홍대센터 201동/11호</td>
										<td class=""><button type="button" class="hgbtn grey01">승인요청</button></td>
									</tr>
									<tr>
										<td>2</td>
										<td class="">아이디(이메일)</td>
										<td class="">회사명/구분</td>
										<td class="">대표자명</td>
										<td class="">연락처</td>
										<td class="">사업자번호</td>
										<td class="">홍대센터 201동/11호</td>
										<td class=""><button type="button" class="hgbtn grey01">승인요청</button></td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- //기존회원 추가하기 -->
						<!-- 컷트라인 -->
						<div class="cut_line"></div>
						<!-- //컷트라인 -->

						<div class="join_agr_box mb10"><label for="priv_check"><input type="checkbox" name="" id="priv_check" /> <span class="fc_org1">제3자 정보제공 이용동의서에</span> 동의합니다</label></div>

						<div class="ta_btn_area">
							<button type="button" class="hgbtn org01 hsize48" style="width:332px;">수정</button>
						</div>

					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->
<?php get_footer(); ?>