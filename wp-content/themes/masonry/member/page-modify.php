<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header();

?>	


	<!-- sub_article -->
	<section id="sub_article">
		<!-- 페이지 타이틀 -->
		<div class="page_tit_area">
			<h2 class="sub_tit1"><span class="fc_org1">회</span>원정보 수정</h2>
		</div>
		<!-- //페이지 타이틀 -->

		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">기본정보</h3>
			
			<form id="registerForm" action="" method="post">
			
			<!-- article inner -->
			<article class="inner">
				<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="" />
						<col width="380px" />
					</colgroup>

					<tbody>
						<tr>
							<th scope="row" class="tal"><label for="uid">아이디</label></th>
							<td class="" colspan="2">
								<input type="text" name="uid" id="uid" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->user_login?>" style="width:265px;" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="password">비밀번호</label></th>
							<td class="" colspan="2">
								<input type="password" name="password" id="password" class="ip01" style="width:265px;" MaxLength="12"/>
								<span class="ta_stxt1 ml10">영문,숫자포함 8~12자리 입력해주세요</span>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="password_re">비밀번호 확인</label></th>
							<td class="" colspan="2">
								<input type="password" name="password_re" id="password_re" class="ip01" style="width:265px;" MaxLength="12"/>
							</td>
						</tr>
				<?php 
				// 인증된 회원인경우
				if ( $current_user_extra->c_ci )
				{
				?>
						<tr>
							<th scope="row" class="tal"><label for="uname">이름</label></th>
							<td class=""  colspan="2">
								<input type="text" name="uname" id="uname" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->display_name?>" />
							</td>
							<!--<td rowspan="3" class="cc_area">
								
								<p>전화번호 변경, 개명등으로 수정이 필요하신 분은<br />다시한번 본인인증을 거쳐서 수정하실수 있습니다.</p>
								<button type="button" class="hgbtn grey01 hsize48" style="width:98%;">본인인증하기</button>
								
							</td>-->
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="utel">연락처</label></th>
							<td class=""  colspan="2">
								<input type="text" name="utel" id="utel" class="ip01 readonly1" readonly="readonly" value="<?=$current_user_extra->c_hp?>" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="u_birth">생년월일</label></th>
							<td class=""  colspan="2">
								<input type="text" name="u_birth" id="u_birth" class="ip01 readonly1" readonly="readonly" value="<?=$current_user_extra->c_birth?>" />
							</td>
						</tr>
				<?php
				}
				else
				{
				?>
						<tr>
							<th scope="row" class="tal"><label for="uname">이름</label></th>
							<td class="bln" colspan="2">
								<input type="text" name="uname" id="uname" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->display_name?>" />
							</td>
						</tr>

				<?php
				}
				?>
						<tr>
						<?
						$email = explode("@",$current_user->user_email);
						?>
							<th scope="row" class="tal"><label for="email_id">이메일</label></th>
							<td class="" colspan="2">
								<input type="text" name="email_id" id="email_id" class="ip01" style="width:265px;" value="<?=$email[0]?>" MaxLength="20"/>
								@
								<input type="text" name="email_addr" id="email_addr" class="ip01" style="width:170px;" value="<?=$email[1]?>" MaxLength="20"/>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal">이메일수신여부</th>
							<td class="" colspan="2">
								<label for="mail_yes"><input type="radio" name="c_email_yn" id="mail_yes" value="Y" class="radio_size14" <? if($current_user_extra->c_email_yn == 'Y'){?>checked<?}?>/> 수신</label>
								<label for="mail_no" class="ml10"><input type="radio" name="c_email_yn" id="mail_no" value="N" class="radio_size14" <? if($current_user_extra->c_email_yn == 'N'){?>checked<?}?>/> 수신 거부</label>
							</td>
						</tr>
				<?php 
				// 인증된 회원인경우
				if ( $current_user_extra->c_ci )
				{
				?>
						<tr>
							<th scope="row" class="tal">SMS수신여부</th>
							<td class="" colspan="2">
								<label for="sns_yes"><input type="radio" name="c_sms_yn" id="sns_yes" value="Y" class="radio_size14" <? if($current_user_extra->c_sms_yn == 'Y'){?>checked<?}?>/> 수신</label>
								<label for="sns_no" class="ml10"><input type="radio" name="c_sms_yn" value="N" id="sns_no" class="radio_size14" <? if($current_user_extra->c_sms_yn == 'N'){?>checked<?}?>/> 수신 거부</label>
							</td>
						</tr>
				<?php
				}
				?>
					</tbody>
				</table>

				<div class="ta_btn_area">
					<!--<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>-->
					<button type="button" class="hgbtn grey01 hsize48 ml10" onclick="jsDropout();" style="width:225px;">탈퇴하기</button>
					<button type="submit" class="hgbtn org01 hsize48 ml10"  style="width:225px;">수정하기</button>
				</div>

				<?php wp_nonce_field( 'ajax-user_update-nonce', 'security' ); ?>

			</article>
			<!-- //article inner -->

			</form>
		</div>
		<!-- //white box -->

		
<?php 
//-----------------------------------------------------------------------------------------------	
// 인증된 회원인경우
//-----------------------------------------------------------------------------------------------	
if ( $current_user_extra->c_ci )
{
	//-----------------------------------------------------------------------------------------------	
	// 회원인 경우 
	// 인증만 실행하고 회사등록 또는 회사승인요청 하지 않는 경우 회원의 경우
	//-----------------------------------------------------------------------------------------------	
	if($current_user_extra->c_idx_relay == -1)
	{
?>

		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">입주자 등록/승인</h3>
			<!-- article inner -->
			<article class="inner">
				<!-- txtBox1 -->
				<div class="txt_box1">
					<p class="txt1">기업정보를 입력해 주시면 포인트를 이용한 충전및 할인의 다양한 혜택을 이용해 주실수 있습니다.</p>
				</div>
				<!-- //txtBox1 -->

				<div class="ta_btn_area" style="text-align:left">
					<button type="button" id="btnNewCompany" class="hgbtn grey01 hsize48" style="width:225px; margin-left:40px">기업 관리자 등록</button>
					<span style="margin-left:30px;">본인이 대표 또는 관리자로서 기업 정보를 등록/관리하고 재직중인 임직원을 관리할 수 있습니다.</span>
				</div>
				<div class="ta_btn_area" style="text-align:left">
					<button type="button" id="btnExistCompany" class="hgbtn org01 hsize48" style="width:225px; margin-left:40px">기업 임직원 등록</button>
					<span style="margin-left:30px;">멤버십에 가입한 기업에 재직중인 임직원으로서 기업 관리자로부터 인증 완료 후 서비스를 사용 하실 수 있습니다.</span>
				</div>

				<!-- 신규회원 등록 -->
				<div id="dvNewCompany" class="pl35 pr35 mt70" style="display:none;">
					<!-- 컷트라인 -->
					<div class="cut_line"><span class="ico_cut"></span></div>
					<!-- //컷트라인 -->	

					<form method="post" id="frmNewCompany" name="frmNewCompany">
						<h3 class="subj_tit1 mb10">기업 관리자 등록</h3>
						<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

						<table cellpadding="0" cellspacing="0" border="0" class="type1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="cp_type">회사유형</label></th>
									<td class="">
										<label for="cp_type1"><input type="radio" name="cp_type" id="cp_type1" class="radio_size14" value="1"/> 법인사업자</label>
										<label for="cp_type2" class="ml10"><input type="radio" name="cp_type" id="cp_type2" class="radio_size14" value="2"/> 개인사업자</label>
										<label for="cp_type3" class="ml10"><input type="radio" name="cp_type" id="cp_type3" class="radio_size14" value="3"/> 프리랜서</label>
									</td>
								</tr>
								<tr class="tr_comp">
									<th scope="row" class="tal"><label for="cp_name">회사명</label></th>
									<td class="">
										<input type="test" name="cp_name" id="cp_name" class="ip01" style="width:265px;" MaxLength="20"/>
										<span class="ta_stxt1 ml10">사업자 등록증에 기재된 회사명을 입력해 주세요</span>
									</td>
								</tr>
								<tr class="tr_comp">
									<th scope="row" class="tal">사업자번호</th>
									<td class="">
										<label for="cp_num1"><input type="text" name="cp_num1" id="cp_num1" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/></label>
										<span>-</span>
										<label for="cp_num2"><input type="text" name="cp_num2" id="cp_num2" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="2"/></label>
										<span>-</span>
										<label for="cp_num3"><input type="text" name="cp_num3" id="cp_num3" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="5"/></label>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="ceo">대표자</label></th>
									<td class="">
										<input type="text" name="ceo_name" id="ceo_name" class="ip01" style="width:265px;" MaxLength="10"/>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="ceo">대표자 연락처</label></th>
									<td class="">
										<input type="text" name="ceo_hp0" id="ceo_hp0" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/>
										<span>-</span>
										<input type="text" name="ceo_hp1" id="ceo_hp1" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
										<span>-</span>
										<input type="text" name="ceo_hp2" id="ceo_hp2" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="sel_center">입주센터 등록여부</label></th>
									<td class="">
										<label for="cp_center_y"><input type="radio" name="cp_center_yn" id="cp_center_y" class="radio_size14" value="Y"/> 예</label>
										<label for="cp_center_n" class="ml10"><input type="radio" name="cp_center_yn" id="cp_center_n" class="radio_size14" value="N"/> 아니요</label>
									</td>
								</tr>
								<tr class="tr_CenterAddr" style="display:none">
									<th scope="row" class="tal"><label for="sel_center">입주센터선택</label></th>
									<td class="">
										<!--<select id="sel_center" name="sel_center" class="sel01" style="width:200px;">
											<option value="">선택해 주세요 </option>
										</select>-->
										<?php get_center_list();?>
										<input type="text" name="cp_addr" id="cp_addr" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr class="tr_CompanyAddr" style="display:none">
									<th scope="row" class="tal" rowspan="2"><label for="sel_center">회사 주소</label></th>
									<td class="">
										<input type="text" name="cp_post" id="cp_post" class="ip01" style="width:120px;" MaxLength="8"/>
										<button type="button" class="hgbtn blue01 hsize38 ml30" onclick="jsPostFind();">우편번호찾기</button>
									</td>
								</tr>
								<tr class="tr_CompanyAddr" style="display:none">
									<td class="">
										<input type="text" name="cp_addr1" id="cp_addr1" class="ip01" style="width:265px;"/>
										<input type="text" name="cp_addr2" id="cp_addr2" class="ip01" style="width:265px;" />
									</td>
								</tr>
							</tbody>
						</table>
						
						<div id="dvBtnAfterCert" class="ta_btn_area">
							<button type="button" id="btnAddNewCompany" name="btnAddNewCompany" class="hgbtn grey01 hsize48" style="width:225px;">등록</button>
						</div>
					</form>
					<!-- 컷트라인 -->
					<div class="cut_line mb50"></div>
					<!-- //컷트라인 -->
				</div>
				<!-- //신규회원 등록 -->

				
				
				
				<!-- 기존회원 추가하기 -->
				<div id="dvExistCompany" class="pl35 pr35 mt70" style="display:none;">
					<!-- 컷트라인 -->
					<div class="cut_line"><span class="ico_cut"></span></div>
					<!-- //컷트라인 -->
					
					<form method="post" id="frmExistCompany" name="frmExistCompany">
						<h3 class="subj_tit1 mb10">기업 임직원 등록</h3>
						<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

						<!-- 검색 박스 -->
						<div class="sh_box1">
							<table cellpadding="0" cellspacing="0" border="0" class="" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="120px" />
									<col width="300px" />
									<col width="" />
								</colgroup>

								<tbody>
									<tr>
										<th scope="row">기업검색</th>
										<td>
											<label for="search_type_cp_name"><input type="radio" name="search_type" id="search_type_cp_name" class="radio_size14" value="company"/> 회사명</label>
											<label for="search_type_ceo_name" class="ml10"><input type="radio" name="search_type" id="search_type_ceo_name" class="radio_size14" value="ceo_name"/> 대표자명</label>
											<label for="search_type_email" class="ml10"><input type="radio" name="search_type" id="search_type_email" class="radio_size14" value="email"/> 이메일</label>
										</td>
										<td>
											<input type="text" name="search_val" id="search_val" class="ip01" style="width:300px;" />
											<input type="text" name="nPage" id="nPage" style="width:10px;" />
											<button type="button" id="btnSearchCompany" name="btnSearchCompany" onclick="jsSearchCompany(false);" class="hgbtn grey01 btn_smit">검색</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- //검색 박스 -->

						<table id="tblCompanyList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="70px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="100px" />
							</colgroup>

							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">아이디(이메일)</th>
									<th scope="col">회사명/구분</th>
									<th scope="col">대표자명</th>
									<th scope="col">사업자번호</th>
									<th scope="col">센터구분<br /><span class="fs14">(동/호수)</span></th>
									<th scope="col">승인요청</th>
								</tr>
							</thead>

							<tbody>
							</tbody>
						</table>
					</form>

					<!-- 컷트라인 -->
					<div class="cut_line"></div>
					<!-- //컷트라인 -->
				</div>
				<!-- //기존회원 추가하기 -->			
			</article>
		</div>

<?php
	}
	//-----------------------------------------------------------------------------------------------	
	// 회원인 경우 
	// 회사승인요청 회원의 경우
	//-----------------------------------------------------------------------------------------------	
	else if($current_user_extra->c_idx_relay != -1 && $current_user_extra->c_level != "0")
	{
?>

		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">승인요청 기업정보</h3>

			<!-- article inner -->
			<article class="inner">
				<!-- 기존회원 추가하기 -->
					<table id="tblApplyCompanyList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 " summary="" style="width:100%;">
						<caption></caption>
						<colgroup>
							<col width="70px" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="100px" />
						</colgroup>

						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">아이디(이메일)</th>
								<th scope="col">회사명/구분</th>
								<th scope="col">대표자명</th>
								<th scope="col">사업자번호</th>
								<th scope="col">센터구분<br /><span class="fs14">(동/호수)</span></th>
								<th scope="col">승인요청</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td colspan="7"> Loading ... </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //기존회원 추가하기 -->

			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->

<?php
	}
	else
	{
	}
}

//-----------------------------------------------------------------------------------------------	
// 인증되지 않는 회원
//-----------------------------------------------------------------------------------------------	
else
{
?>
		<!-- white box -->
		<div class="wh_box member_cc">
			<!-- article inner -->
			<article class="inner">
				<!--  join_result_wrap -->
				<div class="join_result_wrap">
					<div class="join_cc">
						<p class="tit"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_setting.png" /> <span><span class="fc_org1">R;</span> 멤버십 본인인증</span></p>
						<p class="txt1">
							R;멤버십 사이트의 포인트를 이용한 충전과 공실예약, 사무실예약 ,제휴몰 구매등을 위해서는<br />
							회원가입후 본인인증을 해주셔야 보다 편리하고 쉽게 이용하실 수 있습니다.
						</p>
					</div>

					<div class="ta_btn_area ">
						<a href="certification"  class="hgbtn org01 hsize48" style="width:332px;">본인인증 하기</a>
					</div>
				</div>
				<!-- //join_result_wrap -->
			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->

<?php
}
?>
	</section>
	<!-- //sub_article -->



<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
<!--
	( function( $ ) {
		$(document).ready(function(){									
			
			
			// 회원정보수정
			$("#registerForm").on("submit" , function(e){	
				
				if(!$('form#registerForm #email_id').val() || !$('form#registerForm #email_addr').val()){
					rh_alert("이메일을 정확히 입력해 주세요.");
					return false;
				}
				var email = $('form#registerForm #email_id').val()+"@"+$('form#registerForm #email_addr').val();

				var user_pass = $('form#registerForm #password').val();
				var user_pass_re = $('form#registerForm #password_re').val();

				
			
				if(user_pass && !user_pass_re){
					rh_alert("비밀번호를 입력해 주세요.");
					return false;
				}

				if(user_pass && user_pass != user_pass_re){
					rh_alert("비밀번호가 일치하지 않습니다.");
					return false;
				}

				if(user_pass && (user_pass.length < 8 || user_pass.length > 12 || jsChkPwd(user_pass) == false))
				{
					rh_alert("비밀번호를 확인해 주세요. (영문,숫자 포함 8~12 자리)");
					return false;
				}

				var c_email_yn = $("form#registerForm [name=c_email_yn]:checked").val();						
				var c_sms_yn = $("form#registerForm [name=c_sms_yn]:checked").val();						

				$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_url,
				data: { 
					'action': 'rh_user_update', //calls wp_ajax_nopriv_ajaxlogin
					'email': email ,
					'user_pass': user_pass ,
					'c_email_yn': c_email_yn ,
					'c_sms_yn': c_sms_yn ,
					'security': $('form#registerForm #security').val() 
				},
				success: function(data){
					rh_alert("수정되었습니다.","reload" );					
				}});

				e.preventDefault();
			});
				
		});


		var strUserJoinState = '<?= $current_user_extra->c_state ?>';
		
		if(strUserJoinState > 2)
			jsLoadCompanyList();




		$("#btnNewCompany").click(function(){
			$("#dvExistCompany").hide();
			$("#dvNewCompany").show();
		});

		$("#btnExistCompany").click(function(){
			$("#dvNewCompany").hide();
			$("#dvExistCompany").show();

			jsSearchCompany(true);
		});

		$("input[name=cp_type]").click(function() {
			if ($(this).val() == "3")
			{
				$("#cp_name").val("");
				$("#cp_num1").val("");
				$("#cp_num2").val("");
				$("#cp_num3").val("");

				$("#cp_name").attr("disabled",true);
				$("#cp_num1").attr("disabled",true);
				$("#cp_num2").attr("disabled",true);
				$("#cp_num3").attr("disabled",true);

				$(".tr_comp").hide();
			} 
			else
			{
				$("#cp_name").attr("disabled",false);
				$("#cp_num1").attr("disabled",false);
				$("#cp_num2").attr("disabled",false);
				$("#cp_num3").attr("disabled",false);

				$(".tr_comp").show();
			}
		});

		$("input[name=cp_center_yn]").click(function() {
			if ($(this).val() == "Y")
			{
				$(".tr_CompanyAddr").hide();
				$(".tr_CenterAddr").show();
			} 
			else
			{
				$(".tr_CenterAddr").hide();
				$(".tr_CompanyAddr").show();
			} 
		});


		$("#btnAddNewCompany").click(function(){
			if($("form#frmNewCompany input:radio[name=cp_type]").is(":checked") == false)
			{
				rh_alert("회사유형을 체크하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_type]:checked").val() != 3 && $.trim($("form#frmNewCompany #cp_name").val()) == "")
			{
				rh_alert("회사명을 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_type]:checked").val() != 3 && ($.trim($("form#frmNewCompany #cp_num1").val()) == "" || $.trim($("form#frmNewCompany #cp_num2").val()) == "" || $.trim($("form#frmNewCompany #cp_num3").val()) == ""))
			{
				rh_alert("사업자번호를 입력하세요.");
				return false;
			}
			else if($.trim($("form#frmNewCompany #ceo_name").val()) == "")
			{
				rh_alert("대표자명을 입력하세요.");
				return false;
			}
			else if($.trim($("form#frmNewCompany #ceo_hp0").val()) == "" || $.trim($("form#frmNewCompany #ceo_hp1").val()) == "" || $.trim($("form#frmNewCompany #ceo_hp2").val()) == "")
			{
				rh_alert("대표자 연락처를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]").is(":checked") == false)
			{
				rh_alert("입주센터 등록여부를 선택하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "Y" && $("form#frmNewCompany #sel_center").val() == "")
			{
				rh_alert("입주센터를 선택하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "Y" && $.trim($("form#frmNewCompany #cp_addr").val()) == "")
			{
				rh_alert("입주센터 나머지주소(동/호수)를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $("form#frmNewCompany #cp_post").val() == "")
			{
				rh_alert("회사 우편번호를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $.trim($("form#frmNewCompany #cp_addr1").val()) == "")
			{
				rh_alert("회사주소를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $.trim($("form#frmNewCompany #cp_addr2").val()) == "")
			{
				rh_alert("회사 상세주소를 입력하세요.");
				return false;
			}
			else
			{
				var formData = $("#frmNewCompany").serialize();
				$.ajax({
						type: 'POST',
						url: '../../CI/Company/funcRegisterCompany',
						dataType: 'json',
						data : formData,
						success:function(data){						
							if(data.result == "success")
							{
								rh_alert(data.html , function(){
									$(location).attr("href", "member/my_company/");
								});
							}
							else
							{
								rh_alert(data.html);
							}
						},
						error: function(request, status, error) {
							rh_alert(request + " " + status + " " + error)
						}                    
					});
			}
		});
	
	} )( jQuery );



	//요청회사목록(사용자)
	function jsLoadCompanyList()
	{
		$("#tblApplyCompanyList tbody").html("");

		$.post('../../CI/Company/funcAppliedCompanyList', 
		null, 
			function(obj) {
				
				var html = "";			
				
				if(obj == null) 
				{
					return false;
				}
				else
				{
					$("#tblApplyCompanyList tbody").append(obj.html);						
				}
		}, 'json')			
	}






	function jsSearchCompany(bLoadState)
	{
		if(bLoadState == false && $("form#frmExistCompany input:radio[name=search_type]").is(":checked") == false)
		{
			rh_alert("검색유형을 체크하세요.");
			return false;
		}
		else if(bLoadState == false && $.trim($("form#frmExistCompany #search_val").val()) == "")
		{
			rh_alert("검색어를 입력하세요.");
			return false;
		}
		else
		{
			$("#tblCompanyList tbody").html("");

			$.post('../../CI/Company/funcSearchCompany', 
				{
					search_type : $("form#frmExistCompany input:radio[name=search_type]:checked").val(), 
					search_val : $("form#frmExistCompany #search_val").val(), 
					nPage : $("form#frmExistCompany #nPage").val()
				}, 
				
				function(obj) {
					
					var html = "";			
					
					if(obj == null) 
					{
						return false;
					}
					else
					{
						$("#tblCompanyList tbody").append(obj.html);						
					}
			}, 'json')			
		}

	}

	function jsApplyToCompany(nCompIdx)
	{
		if(confirm("승인요청을 진행하시겠습니까?")== true)
		{
			$.ajax({
					type: 'POST',
					url: '../../CI/Company/funcApplyToCompany',
					dataType: 'json',
					data : {nCompIdx : nCompIdx},
					success:function(data){						
						if(data.result == "success")
						{
							rh_alert(data.html , function(){
								$(location).attr("href", "member/modify/");
							});
						}
						else
						{
							rh_alert(data.html);
						}
					},
					error: function(request, status, error) {
						rh_alert(request + " " + status + " " + error)
					}                    
				});
		}
	}

	function jsDropout()
	{
		if(confirm("회원을 탈퇴하시겠습니까?") == true)
		{
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_url,
				data: {
					'action': 'rh_user_dropout', //calls wp_ajax_nopriv_ajaxlogin
					'security': $('form#registerForm #security').val()
				},
				success: function(data){
					if(data.result == true)
					{
						rh_alert(data.message ,function(){ location.href = "/?logout=true" });
					}
					else{
						rh_alert(data.message , function(){
							if(data.loc == "comp")
								location.href = "/member/my_company/#company_members";
							else
								location.href = "/member/modify";
						});
					}
						
				}});
			
		}
	}

	function jsPostFind()
	{
		new daum.Postcode({
			oncomplete: function(data) {
				$("#cp_post").val(data.zonecode);
				
				if(data.userSelectedType == "R")
					$("#cp_addr1").val(data.roadAddress);
				else
					$("#cp_addr1").val(data.jibunAddress);
			}
		}).open();
	}

	function jsChkPwd(strPwd)
	{
		var strRegPwd = /^.*(?=.{8,12})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(strRegPwd.test(strPwd) == false)
			return false;
		else
			return true;
	}

	function jsOnlyNumber(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
	
		if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 9 || keyID == 46 || keyID == 37 || keyID == 39)
		{
			return;
		}
		else
		{
			return false;
		}
	}
	
	function jsRemoveChar(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 || keyID == 9)
		{
			return;
		}
		else
		{
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
		}
	}

	//-->
</script>

		
			
			
			
<?php get_footer(); ?>