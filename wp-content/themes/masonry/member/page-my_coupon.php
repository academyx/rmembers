<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header();

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">M</span>y 쿠폰</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="40px" />
								<col width="80px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="100px" />
							</colgroup>

							<thead>
								<tr>
									<th scope="col">&nbsp;</th>
									<th scope="col">No</th>
									<th scope="col">쿠폰번호</th>
									<th scope="col">쿠폰명</th>
									<th scope="col">할인헤택</th>
									<th scope="col">적용대상</th>
									<th scope="col">유효기간</th>
								</tr>
							</thead>

							<tbody>
								<!-- <tr>
									<td><input type="checkbox" name="" id="item1" /></td>
									<td class="">1</td>
									<td class="">1234-5678-9012</td>
									<td class="">장기계약할인쿠폰</td>
									<td class="">노트북 10%할인</td>
									<td class="">노트북</td>
									<td class="">2013-07-17 </td>
								</tr> -->
								
							</tbody>
						</table>

						<div class="ta_btn_area tar">
							<!-- <button type="button" class="hgbtn grey01 hsize48" style="width: 175px;">선택 삭제</button> -->
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		<?php get_footer(); ?>