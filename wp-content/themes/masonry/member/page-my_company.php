<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();

//-----------------------------------------------------------------------------------------------	
// 인증된 회원인경우
//-----------------------------------------------------------------------------------------------
if ( !$current_user_extra->c_ci || $current_user_extra->c_level != "0")
{
	//wp_redirect_mgs( '/modify' , '회사 대표계정으로 등록되지 않았습니다.');
	wp_redirect( '/modify' );
}
else
{
	rh_my_company_init();
}

get_header();
?>	


	<!-- sub_article -->
	<section id="sub_article">
		<!-- 페이지 타이틀 -->
		<div class="page_tit_area">
			<h2 class="sub_tit1"><span class="fc_org1">회</span>사정보 수정</h2>
		</div>
		<!-- //페이지 타이틀 -->

		<div class="wh_box member">
			<h3 class="subj_tit1">입주사 정보</h3>
			
			<form id="updateCompanyForm" action="" method="post">
			
			<!-- article inner -->
			<article class="inner">
				<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="" />
						<col width="380px" />
					</colgroup>

					<tbody>
						<tr>
							<th scope="row" class="tal"><label for="strCompType">회사유형</label></th>
							<td class="" colspan="2">
								<label for="strCompType1">
									<input type="radio" name="strCompType" id="strCompType1" class="radio_size14" <? if($my_company->c_co_type == '1'){?>checked<?}?> value="1"/> 법인
								</label>
								<label for="strCompType2" class="ml10">
									<input type="radio" name="strCompType" id="strCompType2" class="radio_size14" <? if($my_company->c_co_type == '2'){?>checked<?}?> value="2"/> 개인
								</label>
								<label for="strCompType3" class="ml10">
									<input type="radio" name="strCompType" id="strCompType3" class="radio_size14" <? if($my_company->c_co_type == '3'){?>checked<?}?> value="3"/> 프리랜서
								</label>
							</td>
						</tr>
						<tr class="tr_comp">
							<th scope="row" class="tal"><label for="strCompName">회사명</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCompName" id="strCompName" class="ip01" value="<?=$my_company->c_nm_comp?>" style="width:265px;" MaxLength="20"/>
							</td>
						</tr>
						<?php
							$arrCompNum = explode("-", $my_company->c_num_comp);
						?>
						<tr>
							<th scope="row" class="tal"><label for="strCompNum">사업자번호</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCompNum0" id="strCompNum0" class="ip01" value="<?=$arrCompNum[0]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/>
								<span>-</span>
								<input type="text" name="strCompNum1" id="strCompNum1" class="ip01" value="<?=$arrCompNum[1]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="2"/>
								<span>-</span>
								<input type="text" name="strCompNum2" id="strCompNum2" class="ip01" value="<?=$arrCompNum[2]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="5"/>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="strCeoName">대표자명</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCeoName" id="strCeoName" class="ip01" value="<?=$my_company->c_nm_ceo?>" style="width:265px;" MaxLength="10"/>
							</td>
						</tr>
						<?php
							$arrCeoHp = explode("-" ,$my_company->c_hp_ceo);
						?>
						<tr>
							<th scope="row" class="tal"><label for="strCompNum">대표자 연락처</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCeoHp0" id="strCeoHp0" class="ip01" value="<?=$arrCeoHp[0]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/>
								<span>-</span>
								<input type="text" name="strCeoHp1" id="strCeoHp1" class="ip01" value="<?=$arrCeoHp[1]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
								<span>-</span>
								<input type="text" name="strCeoHp2" id="strCeoHp2" class="ip01" value="<?=$arrCeoHp[2]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
							</td>
						</tr>
<?php
	if($my_company->c_idx_center)
	{
?>
						<tr>
							<th scope="row" class="tal"><label for="strRCenter">입주센터</label></th>
							<td class="" colspan="2">
								
								<?php	get_center_list($my_company->c_idx_center);?>
								
								<input type="text" name="strCompAddr2" id="strCompAddr2" class="ip01" value="<?=$my_company->c_addr2?>" style="width:265px; margin-left:10px" MaxLength="30"/>

							</td>
						</tr>
<?php
	}
	else
	{
?>
						<tr>
							<th scope="row" class="tal" rowspan="2"><label for="sel_center">회사 주소</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCompPost" id="strCompPost" class="ip01" style="width:120px;" value="<?=$my_company->c_post?>" MaxLength="8"/>
								<button type="button" class="hgbtn blue01 hsize38 ml30" onclick="jsPostFind();">우편번호찾기</button>
							</td>
						</tr>
						<tr>
							<td class="" colspan="2">
								<input type="text" name="strCompAddr1" id="strCompAddr1" class="ip01" style="width:265px;" value="<?=$my_company->c_addr1?>" MaxLength="40"/>
								<input type="text" name="strCompAddr2" id="strCompAddr2" class="ip01" style="width:265px;" value="<?=$my_company->c_addr2?>" MaxLength="30"/>
							</td>
						</tr>
<?php
	}
?>

					</tbody>
				</table>
				<input type="hidden" name="nCompIdx" id="nCompIdx" value="<?=$my_company->c_idx_resi?>" />
				<input type="hidden" name="strCenterYN" id="strCenterYN" value="<?=($my_company->c_idx_center == NULL) ? "N" : "Y"?>" />

				<div class="ta_btn_area">
					<button type="submit" class="hgbtn org01 hsize48 ml10" style="width:225px;">수정하기</button>
					<!--<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>-->
				</div>


			</article>
			<!-- //article inner -->
			<?php wp_nonce_field( 'ajax-user_update-nonce', 'security' ); ?>

			</form>
		</div>
		<!-- //white box -->
		
		
		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1" id="company_members">입주사 회원정보</h3>

			<!-- article inner -->
			<article class="inner">
				<!-- 기존회원 추가하기 -->
					<table id="tblUserList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 " summary="" style="width:100%;">
						<caption></caption>
						<colgroup>
							<col width="60px" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="100px" />
							<col width="" />
							<col width="" />
						</colgroup>

						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">아이디(이메일)</th>
								<th scope="col">실명</th>
								<th scope="col">연락처</th>
								<th scope="col">생년월일</th>
								<th scope="col">상태</th>
								<th scope="col">인증신청일</th>
								<th scope="col">승인처리</th>
								<th scope="col">대표변경</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td colspan="9"> Loading ... </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //기존회원 추가하기 -->

			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->
	</section>




<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
<!--
	( function( $ ) {
		$(document).ready(function(){

			
			$("form#updateCompanyForm input:radio[name=strCompType]").click(function() {

				if ($(this).val() == "3")
				{
					$("#strCompName").val("");
					$("#strCompNum0").val("");
					$("#strCompNum1").val("");
					$("#strCompNum2").val("");

					$("#strCompName").attr("disabled",true);
					$("#strCompNum0").attr("disabled",true);
					$("#strCompNum1").attr("disabled",true);
					$("#strCompNum2").attr("disabled",true);
				} 
				else
				{
					$("#strCompName").attr("disabled",false);
					$("#strCompNum0").attr("disabled",false);
					$("#strCompNum1").attr("disabled",false);
					$("#strCompNum2").attr("disabled",false);
				} 
			});
		
			var nCenter = "<?=$my_company->c_idx_center?>";

			//입주사정보 수정
			$("#updateCompanyForm").on("submit" , function(e){	
				
				if($("form#updateCompanyForm input:radio[name=strCompType]").is(":checked") == false)
				{
					rh_alert("사업자구분을 선택해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm input:radio[name=strCompType]:checked").val() != 3 && $.trim($("form#updateCompanyForm #strCompName").val()) == "")
				{
					rh_alert("회사명을 입력해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm input:radio[name=strCompType]:checked").val() != 3 && ($("form#updateCompanyForm #strCompNum0").val() == "" || $("form#updateCompanyForm #strCompNum1").val() == "" || $("form#updateCompanyForm #strCompNum2").val() == ""))
				{
					rh_alert("사업자번호를 입력해 주세요.");
					return false;
				}
				else if($.trim($("form#updateCompanyForm #strCeoName").val()) == "")
				{
					rh_alert("대표자명을 입력해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm #strCeoHp0").val() == "" || $("form#updateCompanyForm #strCeoHp1").val() == "" || $("form#updateCompanyForm #strCeoHp2").val() == "")
				{
					rh_alert("대표자 연락처를 입력해 주세요.");
					return false;
				}
				else if(nCenter != "" && $("form#updateCompanyForm #sel_center").val() == "")
				{
					rh_alert("입주센터를 선택해 주세요.");
					return false;
				}
				else if(nCenter != "" && $.trim($("form#updateCompanyForm #strCompAddr2").val()) == "")
				{
					rh_alert("나머지주소(동/호수)를 입력해 주세요.");
					return false;
				}
				else if(nCenter == "" && $("form#updateCompanyForm #strCompPost").val() == "")
				{
					rh_alert("우편번호를 입력해 주세요.");
					return false;
				}
				else if(nCenter == "" && $.trim($("form#updateCompanyForm #strCompAddr1").val()) == "")
				{
					rh_alert("회사주소를 입력해 주세요.");
					return false;
				}
				else if(nCenter == "" && $.trim($("form#updateCompanyForm #strCompAddr2").val()) == "")
				{
					rh_alert("상세주소를 입력해 주세요.");
					return false;
				}
				else
				{
					if(confirm("입주사정보를 수정하시겠습니까?") == true)
					{
						var formData = $("#updateCompanyForm").serialize();
						$.ajax({
								type: 'POST',
								url: '../../CI/Company/funcUpdateCompany',
								dataType: 'json',
								data: formData,
								success:function(data){						
									if(data.result == "success")
									{
										rh_alert(data.html , function(){
											window.location.reload();
										});
									}
									else
									{
										rh_alert(data.html);
									}
								},
								error: function(request, status, error) {
									rh_alert(request + " " + status + " " + error)
								}                    
							});
							e.preventDefault();

					}
					else
						return false;
				}
			});
				
		});

		$("input[name=cp_type]").click(function() {
			if ($(this).val() == "3")
			{
				$("#cp_name").val("");
				$("#cp_num1").val("");
				$("#cp_num2").val("");
				$("#cp_num3").val("");

				$("#cp_name").attr("disabled",true);
				$("#cp_num1").attr("disabled",true);
				$("#cp_num2").attr("disabled",true);
				$("#cp_num3").attr("disabled",true);
			} 
			else
			{
				$("#cp_name").attr("disabled",false);
				$("#cp_num1").attr("disabled",false);
				$("#cp_num2").attr("disabled",false);
				$("#cp_num3").attr("disabled",false);
			} 
		});


		jsLoadMemberList();

	
	} )( jQuery );





	//요청회원목록(대표)
	function jsLoadMemberList()
	{
		$("#tblUserList tbody").html("");

		$.post('../../CI/Company/funcAppliedUserList', 
			{
				nCompIdx :  '<?= $current_user_extra->c_idx_resi ?>'
			}, 
			
			function(obj) {
				
				var html = "";			
				
				if(obj == null) 
				{
					return false;
				}
				else
				{
					$("#tblUserList tbody").append(obj.html);						
				}
		}, 'json')			

	}


	function jsUpdateApplyerState(nRelIdx, nUserIdx, strState)
	{
		var strConfirmMsg = "";

		if(strState == "Y")
			strConfirmMsg = "승인 하시겠습니까?";
		else
			strConfirmMsg = "승인취소 하시겠습니까?";
		
		if(confirm(strConfirmMsg)== true)
		{
			$.ajax({
					type: 'POST',
					url: '../../CI/Company/funcAcceptUserState',
					dataType: 'json',
					data : {nRelIdx : nRelIdx, nUserIdx : nUserIdx, strState : strState},
					success:function(data){						
						if(data.result == "success")
						{
							rh_alert(data.html , jsLoadMemberList() );
						}
						else
						{
							rh_alert(data.html);
						}
					},
					error: function(request, status, error) {
						rh_alert(request + " " + status + " " + error);
					}                    
				});
		}			

	}

function jsAppointCeo(obj, nRelIdx, nMemIdx)
{
	var tr = obj.parent().parent();
	var strName = tr.find(".td-user").html();

	if(confirm(strName + "님을 대표자로 변경(지정) 하시겠습니까?")== true)
	{
		$.ajax({
			type: 'POST',
			url: '../../CI/Company/funcAppointCeo',
			dataType: 'json',
			data : {nRelIdx : nRelIdx, nMemIdx : nMemIdx},
			success:function(data){
				if(data.result == "success")
				{
					rh_alert(data.html ,function(){
						$(location).attr("href", "member/modify/");
					});
				}
				else
				{
					rh_alert(data.html);
				}
			},
			error: function(request, status, error) {
				rh_alert(request + " " + status + " " + error)
			}
		});
	}

}


	function jsApplyToCompany(nCompIdx)
	{
		if(confirm("승인요청을 진행하시겠습니까?")== true)
		{
			$.ajax({
					type: 'POST',
					url: '../../CI/Company/funcApplyToCompany',
					dataType: 'json',
					data : {nCompIdx : nCompIdx},
					success:function(data){						
						if(data.result == "success")
						{
							rh_alert(data.html , function(){
								$(location).attr("href", "member/modify/");
							});
						}
						else
						{
							rh_alert(data.html);
						}
					},
					error: function(request, status, error) {
						rh_alert(request + " " + status + " " + error)
					}                    
				});
		}
	}
	
	function jsPostFind()
	{
		new daum.Postcode({
			oncomplete: function(data) {
				$("#strCompPost").val(data.zonecode);
				
				if(data.userSelectedType == "R")
					$("#strCompAddr1").val(data.roadAddress);
				else
					$("#strCompAddr1").val(data.jibunAddress);
			}
		}).open();
	}

	function jsOnlyNumber(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
	
		if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 9 || keyID == 46 || keyID == 37 || keyID == 39)
		{
			return;
		}
		else
		{
			return false;
		}
	}
	
	function jsRemoveChar(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 || keyID == 9)
		{
			return;
		}
		else
		{
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
		}
	}

	//-->
</script>

		
			
			
			
<?php get_footer(); ?>