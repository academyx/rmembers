<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
if(!$_POST[sns] || !$_POST[sns_id] ){
	wp_redirect( '/login' );
	exit;
}
get_header(); 


?>

<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">SNS </span> Login</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- login_wrap -->
				<div class="login_wrap">

					<!-- login_box -->
					<div class="login_box">
						
						<form id="login" action="/wp-login.php" method="post">
						<div class="login_tab_cts login_fm" id="login_fm">
							<ul class="">
								<li class="login_id"><label for="username"><input type="text" name="log" placeholder="아이디" id="username" class="ip01" /></label></li>
								<li class="login_pw"><label for="password"><input type="password" name="pwd" placeholder="********" id="password" class="ip01" /></label></li>
							</ul>
							<p class="txt1">* <span class="fc_org1">아이디/비밀번호</span>를 입력하시면 이후에는 자동 sns 로그인 가능합니다.</p>

							<input type="hidden" name="sns" id="sns" value="<?=$_POST['sns']?>">
							<input type="hidden" name="sns_id" id="sns_id" value="<?=$_POST['sns_id']?>">

							<button type="submit" class="hgbtn org01 btn_smit"><span>완료</span></button>

						</div>
						<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
						</form>
						<div class="login_line"></div>
						
					</div>
					<!-- //login_box -->
				</div>
				<!-- //login_box -->

				

			</section>

		

<?php get_footer(); ?>