<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header();

?>	


	<!-- sub_article -->
	<section id="sub_article">
		<!-- 페이지 타이틀 -->
		<div class="page_tit_area">
			<h2 class="sub_tit1"><span class="fc_org1">회</span>원정보 수정</h2>
		</div>
		<!-- //페이지 타이틀 -->

		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">기본정보</h3>
			
			<form id="registerForm" action="" method="post">
			
			<!-- article inner -->
			<article class="inner">
				<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="" />
						<col width="380px" />
					</colgroup>

					<tbody>
						<tr>
							<th scope="row" class="tal"><label for="uid">아이디</label></th>
							<td class="" colspan="2">
								<input type="text" name="uid" id="uid" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->user_login?>" style="width:265px;" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="password">비밀번호</label></th>
							<td class="" colspan="2">
								<input type="password" name="password" id="password" class="ip01" style="width:265px;" />
								<span class="ta_stxt1 ml10">영문,숫자포함 8~12자리 입력해주세요</span>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="password_re">비밀번호 확인</label></th>
							<td class="" colspan="2">
								<input type="password" name="password_re" id="password_re" class="ip01" style="width:265px;" />
							</td>
						</tr>
				<?php 
				// 인증된 회원인경우
				if ( $current_user_extra->c_ci )
				{
				?>
						<tr>
							<th scope="row" class="tal"><label for="uname">이름</label></th>
							<td class="">
								<input type="text" name="uname" id="uname" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->display_name?>" />
							</td>
							<td rowspan="3" class="cc_area">
								<!-- 본인인증 -->
								<p>전화번호 변경, 개명등으로 수정이 필요하신 분은<br />다시한번 본인인증을 거쳐서 수정하실수 있습니다.</p>
								<button type="button" class="hgbtn grey01 hsize48" style="width:98%;">본인인증하기</button>
								<!-- //본인인증 -->
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="utel">연락처</label></th>
							<td class="">
								<input type="text" name="utel" id="utel" class="ip01 readonly1" readonly="readonly" value="<?=$current_user_extra->c_hp?>" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="u_birth">생년월일</label></th>
							<td class="">
								<input type="text" name="u_birth" id="u_birth" class="ip01 readonly1" readonly="readonly" value="<?=$current_user_extra->c_birth?>" />
							</td>
						</tr>
				<?php
				}
				else
				{
				?>
						<tr>
							<th scope="row" class="tal"><label for="uname">이름</label></th>
							<td class="bln" colspan="2">
								<input type="text" name="uname" id="uname" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->display_name?>" />
							</td>
						</tr>

				<?php
				}
				?>
						<tr>
						<?
						$email = explode("@",$current_user->user_email);
						?>
							<th scope="row" class="tal"><label for="email_id">이메일</label></th>
							<td class="" colspan="2">
								<input type="text" name="email_id" id="email_id" class="ip01" style="width:265px;" value="<?=$email[0]?>" />
								@
								<input type="text" name="email_addr" id="email_addr" class="ip01" style="width:170px;" value="<?=$email[1]?>" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal">이메일수신여부</th>
							<td class="" colspan="2">
								<label for="mail_yes"><input type="radio" name="c_email_yn" id="mail_yes" class="radio_size14" <? if($current_user_extra->c_email_yn == 'Y'){?>checked<?}?>/> 수신</label>
								<label for="mail_no" class="ml10"><input type="radio" name="c_email_yn" id="mail_no" class="radio_size14" <? if($current_user_extra->c_email_yn == 'N'){?>checked<?}?>/> 수신 거부</label>
							</td>
						</tr>
				<?php 
				// 인증된 회원인경우
				if ( $current_user_extra->c_ci )
				{
				?>
						<tr>
							<th scope="row" class="tal">SMS수신여부</th>
							<td class="" colspan="2">
								<label for="sns_yes"><input type="radio" name="sns_check" id="sns_yes" class="radio_size14" <? if($current_user_extra->c_sms_yn == 'Y'){?>checked<?}?>/> 수신</label>
								<label for="sns_no" class="ml10"><input type="radio" name="sns_check" id="sns_no" class="radio_size14" <? if($current_user_extra->c_sms_yn == 'N'){?>checked<?}?>/> 수신 거부</label>
							</td>
						</tr>
				<?php
				}
				?>
					</tbody>
				</table>

				<div class="ta_btn_area">
					<!--<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>-->
					<button type="submit" class="hgbtn org01 hsize48 ml10" style="width:225px;">수정하기</button>
				</div>

				<?php wp_nonce_field( 'ajax-user_update-nonce', 'security' ); ?>

			</article>
			<!-- //article inner -->

			</form>
		</div>
		<!-- //white box -->

		
<?php 
//-----------------------------------------------------------------------------------------------	
// 인증된 회원인경우
//-----------------------------------------------------------------------------------------------	
if ( $current_user_extra->c_ci )
{
	//-----------------------------------------------------------------------------------------------	
	// 대표인 경우 
	// 회원목록 표시
	//-----------------------------------------------------------------------------------------------	
	if ( $current_user_extra->c_level == 0 )
	{
		// 입주사 정보 조회
		rh_my_company_init();
?>
		<div class="wh_box member">
			<h3 class="subj_tit1">입주사 정보</h3>
			
			<form id="updateCompanyForm" action="" method="post">
			
			<!-- article inner -->
			<article class="inner">
				<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="" />
						<col width="380px" />
					</colgroup>

					<tbody>
						<tr>
							<th scope="row" class="tal"><label for="strCompType">회사유형</label></th>
							<td class="" colspan="2">
								<label for="strCompType1">
									<input type="radio" name="strCompType" id="strCompType1" class="radio_size14" <? if($my_company->c_co_type == '1'){?>checked<?}?> value="1"/> 법인
								</label>
								<label for="strCompType2" class="ml10">
									<input type="radio" name="strCompType" id="strCompType2" class="radio_size14" <? if($my_company->c_co_type == '2'){?>checked<?}?> value="2"/> 개인
								</label>
								<label for="strCompType3" class="ml10">
									<input type="radio" name="strCompType" id="strCompType3" class="radio_size14" <? if($my_company->c_co_type == '3'){?>checked<?}?> value="3"/> 프리랜서
								</label>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="strCompName">회사명</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCompName" id="strCompName" class="ip01" value="<?=$my_company->c_nm_comp?>" style="width:265px;" />
							</td>
						</tr>
						<?php
							$arrCompNum = explode("-",$my_company->c_num_comp);
						?>
						<tr>
							<th scope="row" class="tal"><label for="strCompNum">사업자번호</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCompNum" id="strCompNum0" class="ip01" value="<?=$arrCompNum[0]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)"/>
								<span>-</span>
								<input type="text" name="strCompNum" id="strCompNum1" class="ip01" value="<?=$arrCompNum[1]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)"/>
								<span>-</span>
								<input type="text" name="strCompNum" id="strCompNum2" class="ip01" value="<?=$arrCompNum[2]?>" style="width:100px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)"/>
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="strCeoName">대표자명</label></th>
							<td class="" colspan="2">
								<input type="text" name="strCeoName" id="strCeoName" class="ip01" value="<?=$my_company->c_nm_ceo?>" style="width:265px;" />
							</td>
						</tr>
						<tr>
							<th scope="row" class="tal"><label for="strRCenter">입주센터</label></th>
							<td class="" colspan="2">
								
								<?php	get_center_list($my_company->c_idx_center);?>
								
								<input type="text" name="strAddr2" id="strAddr2" class="ip01" value="<?=$my_company->c_addr2?>" style="width:265px; margin-left:10px" />

							</td>
						</tr>

					</tbody>
				</table>
				<input type="hidden" name="nCompIdx" id="nCompIdx" value="<?=$my_company->c_idx_resi?>" />

				<div class="ta_btn_area">
					<button type="button" id="btnUpdateCompany" name="btnUpdateCompany" class="hgbtn org01 hsize48 ml10" style="width:225px;">수정하기</button>
					<!--<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>-->
				</div>


			</article>
			<!-- //article inner -->

			</form>
		</div>
		<!-- //white box -->
		
		
		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">입주사 회원정보</h3>

			<!-- article inner -->
			<article class="inner">
				<!-- 기존회원 추가하기 -->
					<table id="tblUserList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 " summary="" style="width:100%;">
						<caption></caption>
						<colgroup>
							<col width="70px" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="100px" />
							<col width="" />
						</colgroup>

						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">아이디(이메일)</th>
								<th scope="col">실명</th>
								<th scope="col">연락처</th>
								<th scope="col">생년월일</th>
								<th scope="col">상태</th>
								<th scope="col">인증신청일</th>
								<th scope="col">승인처리</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td colspan="8"> Loading ... </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //기존회원 추가하기 -->

			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->
				
<?php
	}
	//-----------------------------------------------------------------------------------------------	
	// 회원인 경우 
	// 요청입주사 표시
	//-----------------------------------------------------------------------------------------------	
	else
	{
?>

		<!-- white box -->
		<div class="wh_box member">
			<h3 class="subj_tit1">승인요청 기업정보</h3>

			<!-- article inner -->
			<article class="inner">
				<!-- 기존회원 추가하기 -->
					<table id="tblApplyCompanyList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 " summary="" style="width:100%;">
						<caption></caption>
						<colgroup>
							<col width="70px" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="" />
							<col width="100px" />
						</colgroup>

						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">아이디(이메일)</th>
								<th scope="col">회사명/구분</th>
								<th scope="col">대표자명</th>
								<th scope="col">연락처</th>
								<th scope="col">사업자번호</th>
								<th scope="col">센터구분<br /><span class="fs14">(동/호수)</span></th>
								<th scope="col">승인요청</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td colspan="8"> Loading ... </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //기존회원 추가하기 -->

			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->

<?php
	}
}

//-----------------------------------------------------------------------------------------------	
// 인증되지 않는 회원
//-----------------------------------------------------------------------------------------------	
else
{
?>
		<!-- white box -->
		<div class="wh_box member_cc">
			<!-- article inner -->
			<article class="inner">
				<!--  join_result_wrap -->
				<div class="join_result_wrap">
					<div class="join_cc">
						<p class="tit"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_setting.png" /> <span><span class="fc_org1">R;</span> 멤버십 본인인증</span></p>
						<p class="txt1">
							R;멤버십 사이트의 포인트를 이용한 충전과 공실예약, 사무실예약 ,제휴몰 구매등을 위해서는<br />
							회원가입후 본인인증을 해주셔야 보다 편리하고 쉽게 이용하실 수 있습니다.
						</p>
					</div>

					<div class="ta_btn_area ">
						<a href="certification"  class="hgbtn org01 hsize48" style="width:332px;">본인인증 하기</a>
					</div>
				</div>
				<!-- //join_result_wrap -->
			</article>
			<!-- //article inner -->
		</div>
		<!-- //white box -->

<?php
}
?>
	</section>
	<!-- //sub_article -->




<script type="text/javascript">
<!--
	( function( $ ) {
		$(document).ready(function(){									
			
			
			// 회원정보수정
			$("#registerForm").on("submit" , function(e){	
				
				if(!$('form#registerForm #email_id').val() || !$('form#registerForm #email_addr').val()){
					rh_alert("이메일을 정확히 입력해 주세요.");
					return false;
				}
				var email = $('form#registerForm #email_id').val()+"@"+$('form#registerForm #email_addr').val();

				var user_pass = $('form#registerForm #password').val();
				var user_pass_re = $('form#registerForm #password_re').val();

				
			
				if(user_pass && !user_pass_re){
					rh_alert("비밀번호를 입력해 주세요.");
					return false;
				}

				if(user_pass && user_pass != user_pass_re){
					rh_alert("비밀번호가 일치하지 않습니다.");
					return false;
				}

				var c_email_yn = $("form#registerForm [name=c_email_yn]:checked").val();						
				
				$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_url,
				data: { 
					'action': 'rh_user_update', //calls wp_ajax_nopriv_ajaxlogin
					'email': email ,
					'user_pass': user_pass ,
					'c_email_yn': c_email_yn ,
					'security': $('form#registerForm #security').val() 
				},
				success: function(data){
					rh_alert("수정되었습니다.");
					//location.reload(true);
				}});

				e.preventDefault();
			});



			$("form#updateCompanyForm input:radio[name=strCompType]").click(function() {

				if ($(this).val() == "3")
				{
					$("#strCompName").val("");
					$("#strCompNum0").val("");
					$("#strCompNum1").val("");
					$("#strCompNum2").val("");

					$("#strCompName").attr("disabled",true);
					$("#strCompNum0").attr("disabled",true);
					$("#strCompNum1").attr("disabled",true);
					$("#strCompNum2").attr("disabled",true);
				} 
				else
				{
					$("#strCompName").attr("disabled",false);
					$("#strCompNum0").attr("disabled",false);
					$("#strCompNum1").attr("disabled",false);
					$("#strCompNum2").attr("disabled",false);
				} 
			});
		
			//입주사정보 수정
			/*$("#btnUpdateCompany").click(function(){	
				
				if($("form#updateCompanyForm input:radio[name=strCompType]").is(":checked") == false)
				{
					alert("사업자구분을 선택해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm input:radio[name=strCompType]:checked").val() != 3 && $.trim($("form#updateCompanyForm #strCompName").val()) == "")
				{
					alert("회사명을 입력해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm input:radio[name=strCompType]:checked").val() != 3 && ($("form#updateCompanyForm #strCompNum0").val() == "" || $("form#updateCompanyForm #strCompNum1").val() == "" || $("form#updateCompanyForm #strCompNum2").val() == ""))
				{
					alert("사업자번호를 입력해 주세요.");
					return false;
				}
				else if($.trim($("form#updateCompanyForm #strCeoName").val()) == "")
				{
					alert("대표자명을 입력해 주세요.");
					return false;
				}
				else if($("form#updateCompanyForm #sel_center").val() == "")
				{
					alert("입주센터를 선택해 주세요.");
					return false;
				}
				else if($.trim($("form#updateCompanyForm #strAddr2").val()) == "")
				{
					alert("나머지주소(동/호수)를 입력해 주세요.");
					return false;
				}
				else
				{
					if(confirm("입주사정보를 수정하시겠습니까?") == true)
					{
						var frmData = $("#updateCompanyForm").serialize();
						$.ajax({
								type: 'POST',
								url: '../../CI/Company/funcUpdateCompany',
								dataType: 'json',
								data: frmData,
								success:function(data){						
									if(data.result == "success")
									{
										alert(data.html);
										window.reload(true);
									}
									else
									{
										alert(data.html);
									}
								},
								error: function(request, status, error) {
									alert(request + " " + status + " " + error)
								}                    
							});

					}
					else
						return false;
				}
			});*/
				
		});


		var strUserCompLevel = '<?= $current_user_extra->c_level ?>';
		var strUserJoinState = '<?= $current_user_extra->c_state ?>';

		if(strUserCompLevel == "0")
		{
			jsLoadMemberList();
		}
		else
		{
			if(strUserJoinState > 2)
				jsLoadCompanyList();
		}
	
	} )( jQuery );



	//요청회사목록(사용자)
	function jsLoadCompanyList()
	{
		$("#tblApplyCompanyList tbody").html("");

		$.post('../../CI/Company/funcAppliedCompanyList', 
		null, 
			function(obj) {
				
				var html = "";			
				
				if(obj == null) 
				{
					return false;
				}
				else
				{
					$("#tblApplyCompanyList tbody").append(obj.html);						
				}
		}, 'json')			
	}

	//요청회원목록(대표)
	function jsLoadMemberList()
	{
		$("#tblUserList tbody").html("");

		$.post('../../CI/Company/funcAppliedUserList', 
			{
				nCompIdx :  '<?= $current_user_extra->c_idx_resi ?>'
			}, 
			
			function(obj) {
				
				var html = "";			
				
				if(obj == null) 
				{
					return false;
				}
				else
				{
					$("#tblUserList tbody").append(obj.html);						
				}
		}, 'json')			

	}


	function jsUpdateApplyerState(nRelIdx, nUserIdx, strState)
	{
		var strConfirmMsg = "";

		if(strState == "Y")
			strConfirmMsg = "승인 하시겠습니까?";
		else
			strConfirmMsg = "승인취소 하시겠습니까?";
		
		if(confirm(strConfirmMsg)== true)
		{
			$.ajax({
					type: 'POST',
					url: '../../CI/Company/funcAcceptUserState',
					dataType: 'json',
					data : {nRelIdx : nRelIdx, nUserIdx : nUserIdx, strState : strState},
					success:function(data){						
						if(data.result == "success")
						{
							rh_alert(data.html);
							jsLoadMemberList();
						}
						else
						{
							rh_alert(data.html);
						}
					},
					error: function(request, status, error) {
						rh_alert(request + " " + status + " " + error)
					}                    
				});
		}			

	}


	function jsOnlyNumber(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
	
		if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
		{
			return;
		}
		else
		{
			return false;
		}
	}
	
	function jsRemoveChar(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
		{
			return;
		}
		else
		{
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
		}
	}

	//-->
</script>

		
			
			
			
<?php get_footer(); ?>