<?include("../inc/head.php");?>

	<?include("../inc/header.php");?>
	<!-- container -->
	<div id="container">
		<!-- contents -->
		<div id="contents">
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">M</span>y 포인트 나눠주기</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box mypoint_result">
					<!-- article inner -->
					<article class="inner">
						<p class="pic"><img src="../images/ico_point.png" alt="" /></p>
						** 님 보유 R;포인트는
						<span class="txt_point fc_org1">15,0000</span>
						R;포인트 입니다
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="ta_top_area">
							<h3 class="subj_tit1 fl">포인트 나눠주기</h3>
							<div class="fr pt5">
								<label class="" for="sh_all"><input type="radio" id="sh_all" name="sh_type"> 전체</label>
								<label class="ml10" for="sh_point_recharge"><input type="radio" id="sh_point_recharge" name="sh_type"> 포인트충전</label>
								<label class="ml10" for="sh_point_use"><input type="radio" id="sh_point_use" name="sh_type"> 포인트사용</label>
							</div>
						</div>
						<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="80px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="130px" />
							</colgroup>

							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">이메일/이름</th>
									<th scope="col">실명</th>
									<th scope="col">연락처</th>
									<th scope="col">생년월일</th>
									<th scope="col">포인트</th>
									<th scope="col">포인트 나눠주기</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td class="">1</td>
									<td class="">aa@aa.co.kr</td>
									<td class="">이름</td>
									<td class="">010-0000-0000</td>
									<td class="">1977-01-02</td>
									<td class="">20,000</td>
									<td class=""><button type="button" class="hgbtn org01 btn_point_share">나눠주기</button></td>
								</tr>
							</tbody>
						</table>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

<!-- 포인트 나눠주기 -->
<div class="lypop normal_pop pop_point_share">
	<div class="pop_wrap">
		<article class="inner">
			<!-- 타이틀 -->
			<div class="pop_tit_area">
				<h3 class="pop_tit1">R;포인트 나눠주기</h3>
			</div>
			<!-- //타이틀 -->

			<!-- 팝업 내용 -->
			<div class="pop_article">
				<div class="ps_top">
					<p class="pic"><img src="../images/ico_point_bk.png" /></p>
					<p class="txt1">***님이 **** 님에게 <span class="fc_org1">R;포인트 나눠주기</span>를 하실수 있습니다.</p>
				</div>

				<!-- 포인트 조정 -->
				<div class="point_info">
					<dl class="my_point">
						<dt>내 포인트</dt>
						<dd>
							<span class="n">50,000</span> <span class="txt_before">나누기전</span>
						</dd>
						<dd>
							<span class="n">30,000</span> <span class="txt_after">나눈 후</span>
						</dd>
					</dl>
					<dl class="other_point">
						<dt>상대 포인트</dt>
						<dd>
							<span class="txt_before">나누기전</span> <span class="n">50,000</span>
						</dd>
						<dd>
							<span class="txt_after">나눈 후</span> <span class="n">30,000</span>
						</dd>
					</dl>
				</div>
				<!-- //포인트 조정 -->

				<!-- 포인트 조정 -->
				<div class="point_setting">
					<button type="button" class="hgbtn btn_minus" title="감소">-</button>
					<button type="button" class="hgbtn btn_plus" title="증가">+</button>

					<div class="point_bar_wrap">
						<div class="point_bar">
							<div class="bar"></div>
						</div>
						<div class="point_num">
							<span class="num fir">0</span>
							<span class="num md">20,000</span>
							<span class="num last">50,000</span>
						</div>
					</div>
				</div>
				<!-- //포인트 조정 -->

				<div class="ta_btn_area">
					<button type="button" class="hgbtn grey01 hsize48" style="width:167px;">신규등록</button>
					<button type="button" class="hgbtn org01 ml10 hsize48" style="width:167px;">기존회원 추가</button>
				</div>
			</div>
			<!-- //팝업 내용 -->
		</article>
	</div>
	<button type="button" class="hgbtn btn_lypop_close" onclick="$.fn.lypop.lyClose(this, false);">팝업 닫기</button>
</div>
<!-- //포인트 나눠주기 -->

<script type="text/javascript">
$(".btn_point_share").on("click", function(){
	// 나눠 주기 팝업 열기
	$(".pop_point_share").lypop({
		type : "normal",
		popWidth : 862
	});
});
// 나눠 주기 팝업 열기
$(".pop_point_share").lypop({
	type : "normal",
	popWidth : 862
});

var $minus = $(".point_setting .btn_minus");
var $plus = $(".point_setting .btn_plus");
var $bar = $(".point_bar .bar");
var min = 0;
var max = 420;
$bar.width(0);

var pt = $bar.outerWidth();

$plus.on("click", function(){

	if (pt >= max) {
		rh_alert("그만");
	}else {
		pt+=10;
		$bar.width(pt);
	}
	console.log(pt);

});
$minus.on("click", function(){
	if (pt <= min) {
		rh_alert("그만");
	}else {
		pt-=10;
		$bar.width(pt);
	}
	console.log(pt);
});
</script>

<?include("../inc/footer.php");?>