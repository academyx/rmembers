<?include("../inc/head.php");?>

	<?include("../inc/header.php");?>
	<!-- container -->
	<div id="container">
		<!-- contents -->
		<div id="contents">
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">승</span>인처리</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="70px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="100px" />
								<col width="100px" />
							</colgroup>

							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">아이디(이메일)</th>
									<th scope="col">실명</th>
									<th scope="col">연락처</th>
									<th scope="col">생년월일</th>
									<th scope="col">상태</th>
									<th scope="col">인증신청일</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td>1</td>
									<td class="">아이디(이메일)</td>
									<td class="">실명</td>
									<td class="">11</td>
									<td class="">11</td>
									<td class=""><button type="button" class="hgbtn grey01">승인취소</button></td>
									<td class="">2013-07-17 </td>
								</tr>
							</tbody>
						</table>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

<?include("../inc/footer.php");?>