<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

get_header(); 

?>

<section id="sub_article">
				<!-- use_guide_wrap -->
				<div class="use_guide_wrap">
					<div class="guide_top">
						<p class="txt1">
							국내 최대<br />
							르호봇 39개 비즈니스 센터<span>에 입주해 있는</span>
						</p>
						<p class="txt2">
							전국 3828개 대규모 입주기업만을 위한<br>
							특별한 프리미업 서비스를 제공합니다.
						</p>
					</div>

					<div class="txtbox">
						<div class="tb1">
							<dl>
								<dt><span class="fc_org1">입</span>주사 만을 위한 R; 포인트 서비스</dt>
								<dd>
									<span class="fc_org1">R;</span>포인트란? 르호봇 비즈니스 센터 입주기업들에게 멤버십 사이트에서 제공하는 현금처럼 사용하실수 있는 멤버십 포인트<br />
									르호봇 멤버십 사이트의  상품구매 및 예약 관련 모든 서비스는 R;포인트로 이루어 지며 멥버십 서비스 혜택도 R;포인트로 부여 됩니다.
								</dd>
							</dl>
						</div>

						<div class="tb2">
							<dl>
								<dt><span class="fc_org1">르</span>호봇 비지니스센터 사무실 예약 서비스</dt>
								<dd>온라인으로 르호봇 비지니스센터의 비어있는 사무실을 알아보고 예약하실 수 있습니다.<br />*서비스 제공 예정입니다</dd>
							</dl>
						</div>
					</div>
					<div class="ta_btn_area">
						<a href="/member/join/" class="hgbtn grey01 wsize2">R;멤버십 가입하기</a>
					</div>
				</div>
				<!-- //use_guide_wrap -->
			</section>
			<!-- //sub_article -->

			

<?php get_footer(); ?>