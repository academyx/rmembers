<?include("../inc/head.php");?>

	<?include("../inc/header.php");?>
	<!-- container -->
	<div id="container">
		<!-- contents -->
		<div id="contents">
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">회</span>원가입</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1">기본정보</h3>
					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="uid">아이디</label></th>
									<td class="">
										<input type="text" name="" id="uid" class="ip01" style="width:265px;" />
										<button type="button" class="hgbtn grey01 ml10" style="width:164px;">중복확인</button>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="upw">비밀번호</label></th>
									<td class="">
										<input type="password" name="" id="upw" class="ip01" style="width:265px;" />
										<span class="ta_stxt1 ml10">영문,숫자포함 8~12자리 입력해주세요</span>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="upw_chck">비밀번호 확인</label></th>
									<td class="">
										<input type="password" name="" id="upw_chck" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="uname">이름</label></th>
									<td class="">
										<input type="text" name="" id="uname" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="umail1">이메일</label></th>
									<td class="">
										<input type="text" name="" id="umail1" class="ip01" style="width:265px;" />
										@
										<input type="text" name="" id="umail2" class="ip01" style="width:170px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal">이메일수신여부</th>
									<td class="">
										<label for="mail_yes"><input type="radio" name="mail_check" id="mail_yes" class="radio_size14" /> 수신</label>
										<label for="mail_no" class="ml10"><input type="radio" name="mail_check" id="mail_no" class="radio_size14" /> 수신 거부</label>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="join_agr_box"><input type="checkbox" name="" id="join_agr_check" /> <span class="fc_org1">회원이용약관</span>과 <span class="fc_org1">개인정보 수집 및 이용동의서</span>에 동의합니다</div>

						<div class="ta_btn_area">
							<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>
							<button type="button" class="hgbtn org01 hsize48 ml10" style="width:225px;">간편가입</button>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

<?include("../inc/footer.php");?>