<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header();

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">R;</span>스토어 구매/취소내역</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box">
					<!-- article inner -->
					<article class="inner">
						<div class="ta_top_area">
							<div class="fr pt5">
								<label class="" for="sh_all"><input type="radio" id="sh_all" name="sh_type"> 전체</label>
								<label class="ml10" for="sh_sell"><input type="radio" id="sh_sell" name="sh_type"> 구매</label>
								<label class="ml10" for="sh_cancel"><input type="radio" id="sh_cancel" name="sh_type"> 취소</label>
							</div>
						</div>
						<table cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="80px" />
								<col width="100px" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="" />
								<col width="100px" />								
							</colgroup>

							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">날짜</th>
									<th scope="col">주문번호</th>	
									<th scope="col">상품명</th>
									<th scope="col">옵션</th>
									<th scope="col">사용포인트</th>
									<th scope="col">상태</th>
								</tr>
							</thead>

							<!-- <tbody>
								<tr>
									<td class="">1</td>
									<td class="">2016.10.10</td>
									<td class="">구매</td>
									<td class="">오피스365 </td>
									<td class="">Business Essiensial/연a간</td>
									<td class="">20,000</td>
									<td class="">신청완료</td>
								</tr>
							</tbody> -->

							<tbody id="ajax-result-list">
								
								
							</tbody>
						</table>

						<div class="pagenate">
							<div class="page">
								
							</div>
						</div>

					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->

			<form method="post" action="" id="ajax-list">
				<input type="hidden" name="action" value="rh_get_order_list">
				<input type="hidden" name="page" value="1">
				<input type="hidden" name="c_type" value="">
			</form>


			<script type="text/javascript">
			<!--
				function get_list(c_type){
					$("#ajax-list").find("input[name=c_type]").val(c_type);
					get_order_list(1);
				}
				
				function get_order_list(page){
					
					$("#ajax-list").find("input[name=page]").val(page);

					$.ajax({
							type: 'POST',
							dataType: 'json',
							url: ajax_url,					
							data: $( "#ajax-list" ).serialize(),
							success: function(data){
									
									var list_html = "";
									var rowIndex = data.page.total * ((data.page - 1)*data.page.per_page) ;
									for(var i = 0 ; i < data.list.length ; i++){
										
										list_html += '<tr>'
											+'<td class="">'+data.list[i].rowIndex+'</td>'
											+'<td class="">'+data.list[i].date+'</td>'
											+'<td class=""><a href="/store/order-complete/?o='+data.list[i].order_no+'">'+data.list[i].order_no+'</a></td>'
											+'<td class=""><a href="/store/order-complete/?o='+data.list[i].order_no+'">'+data.list[i].c_nm_pr+'</a></td>'
											+'<td class="">'+data.list[i].option+'</td>'
											+'<td class="point">'+data.list[i].c_payment_price.format()+'</td>'
											+'<td class="state_'+data.list[i].order_state+'">'+data.list[i].statusLabel+'</td>'
										+'</tr>';									

									}

									$("#ajax-result-list").html(list_html);
									$("#ajax-result-list").data("page",page);
									set_list_page(data.page);
								
							}});

				}
				( function( $ ) {

					

					$(document).ready(function(){	
						get_order_list(1);

						$( ".pagenate" ).on( "click", "a", function(e) {
							e.preventDefault();
							var _page = $(this).data("page");
							if( _page ){
								if( $("#ajax-result-list").data("page") != _page ) get_order_list(_page);
							}
						});

					});
				
				} )( jQuery );

			//-->
			</script>


<?php get_footer(); ?>