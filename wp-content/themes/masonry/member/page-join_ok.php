<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header(); 
?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">회</span>원가입 완료</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<!-- article inner -->
					<article class="inner">
						<!--  join_result_wrap -->
						<div class="join_result_wrap">
							<div class="join_result_top">
								<img src="<?=get_stylesheet_directory_uri();?>/images/ico_doc.png" />
								<p class="txt1">회원가입이 정상적으로 <span class="fc_org1">완료</span>되었습니다</p>
								<p class="txt2">르호봇 멤버십 사이트에 가입해 주셔서 감사합니다. </p>

								<p class="id_box">
									아이디 : <?=$_POST[user_id]?>
								</p>
							</div>

							<!-- <div class="ta_btn_area ">
								<a href="/login"  class="hgbtn grey01 hsize48" style="width:332px;">로그인</a>
							</div> -->
						</div>
						<!-- //join_result_wrap -->
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->

				<!-- white box -->
				<div class="wh_box member_cc">
					<!-- article inner -->
					<article class="inner">
						<!--  join_result_wrap -->
						<div class="join_result_wrap">
							<div class="join_cc">
								<p class="tit"><img src="<?=get_stylesheet_directory_uri();?>/images/ico_setting.png" /> <span><span class="fc_org1">R;</span> 멤버십 본인인증</span></p>
								<p class="txt1">
									R;멤버십 사이트의 포인트를 이용한 충전과 공실예약, 사무실예약 ,제휴몰 구매등을 위해서는<br />
									회원가입후 본인인증을 해주셔야 보다 편리하고 쉽게 이용하실 수 있습니다.
								</p>
							</div>

							<div class="ta_btn_area ">
								<a href="certification"  class="hgbtn org01 hsize48" style="width:332px;">본인인증 하기</a>
							</div>
						</div>
						<!-- //join_result_wrap -->
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->

<?php get_footer(); ?>