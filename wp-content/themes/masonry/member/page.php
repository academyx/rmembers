<?include("../inc/head.php");?>

	<?include("../inc/header.php");?>
	<!-- container -->
	<div id="container">
		<!-- contents -->
		<div id="contents">
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">M</span>ember Login</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- login_wrap -->
				<div class="login_wrap">

					<!-- login_box -->
					<div class="login_box">
						<div class="login_tab">
							<a href="#login_fm" class="on">Login</a><a href="join_fm.php">Join</a>
						</div>
						<div class="login_tab_cts login_fm" id="login_fm">
							<ul class="">
								<li class="login_id"><label for="uid"><input type="text" name="" placeholder="아이디" id="uid" class="ip01" /></label></li>
								<li class="login_pw"><label for="upw"><input type="password" name="" placeholder="********" id="upw" class="ip01" /></label></li>
							</ul>
							<p class="txt1">* <span class="fc_org1">아이디/비밀번호</span>를 잊으셨나요?</p>

							<button type="submit" class="hgbtn org01 btn_smit"><span>Login</span></button>
							<div class="login_check">
								<label for="auto_login"><input type="checkbox" name="" id="auto_login" /> 자동 로그인</label>
							</div>
						</div>
						<div class="login_line"></div>

						<!--  login_type-->
						<div class="login_type">
							<a href="#" class="btn_login_naver"><img src="../images/btn_login_naver.gif" alt="네이버 로그인" title="네이버 로그인" /></a>
							<a href="#" class="btn_login_kakao"><img src="../images/btn_login_kakao.gif" alt="카카오톡  로그인" title="카카오톡  로그인" /></a>
							<a href="#" class="btn_login_fb"><img src="../images/btn_login_fb.gif" alt="페이스북 로그인" title="페이스북 로그인" /></a>
						</div>
						<!--  //login_type-->
					</div>
					<!-- //login_box -->
				</div>
				<!-- //login_box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

	<style type="text/css">
		
		jQuery(document).ready(function($) {

    // Show the login dialog box on click
    $('a#show_login').on('click', function(e){
        $('body').prepend('<div class="login_overlay"></div>');
        $('form#login').fadeIn(500);
        $('div.login_overlay, form#login a.close').on('click', function(){
            $('div.login_overlay').remove();
            $('form#login').hide();
        });
        e.preventDefault();
    });

    // Perform AJAX login on form submit
		$('form#login').on('submit', function(e){
			$('form#login p.status').show().text(ajax_login_object.loadingmessage);
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_login_object.ajaxurl,
				data: { 
					'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
					'username': $('form#login #username').val(), 
					'password': $('form#login #password').val(), 
					'security': $('form#login #security').val() },
				success: function(data){
					$('form#login p.status').text(data.message);
					if (data.loggedin == true){
						document.location.href = ajax_login_object.redirecturl;
					}
				}
			});
			e.preventDefault();
		});

	});

	</style>

<?include("../inc/footer.php");?>