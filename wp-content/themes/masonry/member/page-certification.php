<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();
get_header(); 

?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">본</span>인인증</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<!-- article inner -->
					<article class="inner">
						<div class="certification_wrap">
							<div class="cc_top">
								<img src="<?=get_stylesheet_directory_uri();?>/images/ico_person.png" />
								<p class="">본인확인을 위해 아래의 <span class="fc_org1">I-PIN 인증</span> 또는 <span class="fc_org1">휴대폰 인증</span> 버튼을 클릭해 주십시오.</p>
							</div>

							<div class="ta_btn_area mb80">
								<button type="button" class="hgbtn grey01 hsize48" style="width:332px;" onclick="open_ipin_name_check();return false">I-PIN 인증</button>
								<button type="button" class="hgbtn org01 hsize48 ml10" style="width:332px;" onclick="open_phone_name_check();return false">휴대폰 실명인증</button>
							</div>

							<!-- 유의사항 -->
							<div class="caution_wrap1">
								<p class="tit">본인확인 시 유의사항</p>

								<div class="caution_list">
									<dl>
										<dt>휴대폰 실명인증</dt>
										<dd>
											<ul class="bl_bar">
												<li>휴대폰(본인명의)소지자만 가능합니다.</li>
												<li>본인명의 휴대폰 미소지자의 경우 I-PIN 인증을 이용하시기 바랍니다.</li>
											</ul>
										</dd>
									</dl>
								</div>

								<p class="txt1">본인인증 시 입력되는 정보는 해당 인증기관에서 수집하며 자격시험접수센터는 인증이외의용도로 이용 또는 저장하지 않습니다.</p>
							</div>
							<!-- //유의사항 -->
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

	<script type="text/javascript">
	<!--
		function open_phone_name_check(){
			window.open("", "auth_popup", "width=430,height=590,scrollbar=yes");

			var form1 = document.form1;
			form1.target = "auth_popup";
			form1.submit();

		}
	//-->
	</script>

	<script language="JavaScript">
		//<!--
			function open_ipin_name_check(){	
				var popupWindow = window.open("", "kcbPop", "left=200, top=100, status=0, width=450, height=550");
				var form1 = document.form1_ipin;
				form1.target = "kcbPop";
				form1.submit();

				popupWindow.focus()	;
			}
		//-->
		</script>

	<form name="form1" action="/okname/phone/hs_cnfrm_popup2.php" method="post">
		<input type="hidden" name="rqst_caus_cd" value="00">
		<input type="hidden" name="in_tp_bit" value="0">
	</form>

	<form name="kcbResultForm" method="post" action ="/member/certification_join">
		<input type="hidden" name="cert_mode" 					value="phone" 	/>
		<input type="hidden" name="mem_id" 					value="" 	/>
		<input type="hidden" name="svc_tx_seqno"			value=""	/>
		<input type="hidden" name="rqst_caus_cd"			value="" 	/>
		<input type="hidden" name="result_cd" 				value="" 	/>
		<input type="hidden" name="result_msg" 				value="" 	/>
		<input type="hidden" name="cert_dt_tm" 				value="" 	/>
		<input type="hidden" name="di" 						value="" 	/>
		<input type="hidden" name="ci" 						value="" 	/>
		<input type="hidden" name="uname" 					value="" 	/>
		<input type="hidden" name="birthday" 				value="" 	/>
		<input type="hidden" name="sex" 					value="" 	/>
		<input type="hidden" name="nation" 					value="" 	/>
		<input type="hidden" name="tel_com_cd" 				value="" 	/>
		<input type="hidden" name="tel_no" 					value="" 	/>
		<input type="hidden" name="return_msg" 				value="" 	/>
	</form>  
	
	<form name="form1_ipin" action="/okname/ipin/ipin2.php" method="post">
		
	</form>
	
	<form name="kcbOutForm" method="post">
		<input type="hidden" name="cert_mode" 					value="ipin" 	/>
		<input type="hidden" name="encPsnlInfo" />
		<input type="hidden" name="virtualno" />
		<input type="hidden" name="dupinfo" />
		<input type="hidden" name="uname" />
		<input type="hidden" name="cprequestnumber" />
		<input type="hidden" name="age" />
		<input type="hidden" name="sex" />
		<input type="hidden" name="nationalinfo" />
		<input type="hidden" name="birthday" />
		<input type="hidden" name="ci" />
		<input type="hidden" name="coinfo2" />
		<input type="hidden" name="ciupdate" />
		<input type="hidden" name="cpcode" />
		<input type="hidden" name="authinfo" />
	</form>

<?php get_footer(); ?>