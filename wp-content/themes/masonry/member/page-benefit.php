<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

get_header(); 

?>

<section id="sub_article">
				<!-- benefit_wrap -->
				<div class="benefit_wrap">
					<div class="benefit_top">
						<p class="txt1">R;멤버십 혜택안내</p>
						<p class="txt2">르호봇 비즈니스 입주기업만이 누릴수 있는</p>
						<p class="txt1">R; 멤버십 <span class="b">Premium</span> 혜택</p>
					</div>

					<div class="txtbox">
						<div class="tb1">
							<dl>
								<dt><span class="fc_org1">R;</span>포인트 충전혜택</dt>
								<dd>르호봇 비즈니스 센터 입주기업 임직원에서 일정한 R;포인트를 무료로 제공하고<br>모자라는 금액은 결제를 통해서 충전하여 이용하실 수 있습니다.</dd>
							</dl>
						</div>

						<div class="tb2">
							<dl>
								<dt><span class="fc_org1">R;</span>포인트 사용혜택</dt>
								<dd>R;스토어에서 르호봇 비지니스센터 입주기업만의 특별한 상품을 특별한 가격에  신청 또는 이용 하실 수 있습니다.</dd>
							</dl>
						</div>
					</div>
				</div>
				<!-- //benefit_wrap -->
			</section>
			<!-- //sub_article -->

			

<?php get_footer(); ?>