<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */

if( get_current_user_id() ){
	wp_redirect( '/member/modify/' );
	exit;
}

if($_POST['sns']){
	wp_check_sns_user();
}
get_header(); 

?>
<style type="text/css">
	.pop_agreement p{line-height:25px;}
</style>
<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">회</span>원가입</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<? if($_POST['sns']){ ?>
				<div class="wh_box member_cc">
					<!-- article inner -->
					<article class="inner">
						<!--  join_result_wrap -->
						<div class="join_result_wrap">
							<div class="join_cc">
								
								<p class="txt1">
									이미 가입하신 정보에 SNS로 바로 로그인할 수 있도록 연결하실 수 있습니다.
								</p>
							</div>

							<div class="ta_btn_area ">
								<a href="#" class="hgbtn org01 hsize48" style="width:332px;" onclick="sns_login_link();return false" >SNS 로그인 연결하기</a>
							</div>
						</div>
						<!-- //join_result_wrap -->
					</article>
					<!-- //article inner -->
				</div>

				<script type="text/javascript">
				<!--
					function sns_login_link(){
						var params = {'sns' : "<?=$_POST['sns']?>" , 'sns_id' : "<?=$_POST['sns_id']?>" };
						rh_redirect( '/member/member-sns-login/' , params);
					}
				//-->
				</script>
<? } ?>

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1">기본정보</h3>
					<!-- article inner -->

					<form id="registerForm" action="" method="post">


					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="uid">아이디</label></th>
									<td class="bln">
										<input type="text" name="user_id" id="user_id" class="ip01" style="width:265px;" MaxLength="20"/>
										<button type="button" class="hgbtn grey01 ml10" style="width:164px;" id="btn_id_check">중복확인</button>

										<span class="ta_stxt1 ml10" id="result-id_check"></span>

									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="password">비밀번호</label></th>
									<td class="bln">
										<input type="password" name="password" id="password" class="ip01" style="width:265px;" MaxLength="12"/>
										<span class="ta_stxt1 ml10">영문,숫자포함 8~12자리 입력해주세요</span>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="password_re">비밀번호 확인</label></th>
									<td class="bln">
										<input type="password" name="password_re" id="password_re" class="ip01" style="width:265px;"  MaxLength="12"/>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="display_name">이름</label></th>
									<td class="bln">
										<input type="text" name="display_name" id="display_name" value="<?=$_POST['display_name']?>" class="ip01" style="width:265px;" MaxLength="10"/>
									</td>
								</tr>
								<tr>
								<?

								if($_POST['user_email']){
									$email = explode("@" , $_POST['user_email'] );								
								}

								?>
									<th scope="row" class="tal"><label for="umail1">이메일</label></th>
									<td class="bln">
										<input type="text" name="email_id" id="email_id" class="ip01" style="width:265px;" value="<?=$email[0]?>" MaxLength="20"/>
										@
										<input type="text" name="email_addr" id="email_addr" class="ip01" style="width:170px;" value="<?=$email[1]?>" MaxLength="20"/>
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal" height="40px;">이메일수신여부</th>
									<td class="bln">
										<label for="mail_yes"><input type="radio" name="c_email_yn" id="mail_yes" value="Y" class="radio_size14" checked  /> 수신</label>
										<label for="mail_no" class="ml10"><input type="radio" name="c_email_yn" id="mail_no" value="N" class="radio_size14" /> 수신 거부</label>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="join_agr_box"><input type="checkbox" name="" id="join_agr_check" /> <span class="fc_org1">회원이용약관 <a href="#" onclick="pop_agreement();return false"> <i class="fa fa-file-text-o" aria-hidden="true"></i> </a></span>과 <span class="fc_org1">개인정보 수집 및 이용동의서</span> <a href="#" onclick="pop_privacy();return false"> <i class="fa fa-file-text-o" aria-hidden="true"></i> </a> 에  동의합니다</div>

						<div class="ta_btn_area">
							<button type="button" class="hgbtn grey01 hsize48" style="width:225px;">취소</button>
							<button type="submit" class="hgbtn org01 hsize48 ml10" style="width:225px;">간편가입</button>
						</div>

						<?php wp_nonce_field( 'ajax-register-nonce', 'security' ); ?>
						<input type="hidden" name="id_exists_checked" id="id_exists_checked">

						<input type="hidden" name="sns" id="sns" value="<?=$_POST['sns']?>">
						<input type="hidden" name="sns_id" id="sns_id" value="<?=$_POST['sns_id']?>">



					</article>

					</form>

					<!-- //article inner -->
				</div>
				<!-- //white box -->



			</section>
			<!-- //sub_article -->

			<script type="text/javascript">
			<!--
				( function( $ ) {
					$(document).ready(function(){						
						
						


						$("#btn_id_check").bind("click",function(){
							var user_id = $('form#registerForm #user_id').val();

							if( user_id ) {
							$.ajax({
							type: 'POST',
							dataType: 'json',
							url: ajax_url,
							data: { 
								'action': 'user_id_exists', //calls wp_ajax_nopriv_ajaxlogin
								'user_id': user_id 
							},
							success: function(data){

								if (data.result == '0'){
									$("#id_exists_checked").val("1");
									$("#result-id_check").text("사용하실 수 있는 아이디 입니다.");
								}else{
									$("#id_exists_checked").val("2");
									$("#result-id_check").html("<b style='color:red'>이미 사용중이 아이디 입니다.</b>");
								}
							}							
							});

							

							}else{
								rh_alert("아이디를 입력해 주세요");
							}

						});

						$("#registerForm").on("submit" , function(e){	
							
																		

							if(! $("#join_agr_check:checked").get(0) ){
								rh_alert("회원이용약관과 개인정보 수집 및 이용동의서에 동의해주세요");
								return false;
							}

							var user_id = $('form#registerForm #user_id').val();
							if(!user_id){
								rh_alert("아이디를 입력해 주세요");
								return false;
							}

							var display_name = $('form#registerForm #display_name').val();
							if(!display_name){
								rh_alert("이름을 입력해 주세요");
								return false;
							}
							
							if(!$('form#registerForm #email_id').val() || !$('form#registerForm #email_addr').val()){
								rh_alert("이메일을 정확히 입력해 주세요");
								return false;
							}
							var email = $('form#registerForm #email_id').val()+"@"+$('form#registerForm #email_addr').val();

							var user_pass = $('form#registerForm #password').val();
							var user_pass_re = $('form#registerForm #password_re').val();

							

							if(!user_pass || !user_pass_re){
								rh_alert("비밀번호를 입력해 주세요.");
								return false;
							}

							if(user_pass != user_pass_re){
								rh_alert("비밀번호가 일치하지 않습니다.");
								return false;
							}

							if(user_pass.length < 8 || user_pass.length > 12 || jsChkPwd(user_pass) == false)
							{
								rh_alert("비밀번호를 확인해 주세요. (영문,숫자 포함 8~12 자리)");
								return false;
							}

							var c_email_yn = $("form#registerForm [name=c_email_yn]:checked").val();

							if($("#id_exists_checked").val() != '1'){
								rh_alert("아이디 중복확인을 해주세요.");
								return false;
							}


							if( user_id && display_name && email && user_pass ) {
							$.ajax({
							type: 'POST',
							dataType: 'json',
							url: ajax_url,
							data: { 
								'action': 'ajax_register', //calls wp_ajax_nopriv_ajaxlogin
								'user_id': user_id ,
								'display_name': display_name ,
								'email': email ,
								'user_pass': user_pass ,
								'c_email_yn': c_email_yn ,
								'sns': $("#sns").val() ,
								'sns_id': $("#sns_id").val() ,
								'security': $('form#registerForm #security').val() 
							},
							success: function(data){
								
								if (data.loggedin == true){
									var params = {'user_id' : user_id };
									rh_redirect(data.redirect , params);
								}else{
									rh_alert(data.message);
								}
							}});

							}else{
								rh_alert("아이디를 입력해 주세요");
							}


							e.preventDefault();
						});
							
					});
				
				} )( jQuery );


			function jsChkPwd(strPwd)
			{
				var strRegPwd = /^.*(?=.{8,12})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

				if(strRegPwd.test(strPwd) == false)
					return false;
				else
					return true;
			}

			




			//-->
			</script>

			

			<!-- 회원 이용약관 -->



<?php get_footer(); ?>


