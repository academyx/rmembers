<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
rh_user_page_init();

if(!$_POST[ci] ){
	wp_redirect_mgs( '/login/' , '본인인증 결과값이 정확히 넘어오지 않았습니다.');
}
get_user_info();

if($current_user_extra->c_ci)
	wp_redirect_mgs( '/modify/' , '이미 본인인증을 완료하셨습니다.');

get_header(); 
?>
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">회</span>원가입</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1">기본정보</h3>
					<!-- article inner -->
					<article class="inner">
						<form method="post" id="frmCertInfo" name="frmCertInfo">
							<table cellpadding="0" cellspacing="0" border="0" class="type1 mb35" summary="" style="width:100%;">
								<caption></caption>
								<colgroup>
									<col width="200" />
									<col width="" />
								</colgroup>

								<tbody>
									<tr>
										<th scope="row" class="tal"><label for="uid">아이디</label></th>
										<td class="">
											<input type="text" name="uid" id="uid" class="ip01 readonly1" readonly="readonly" value="<?=$current_user->user_login?>" style="width:265px;" />
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal"><label for="uname">이름</label></th>
										<td class="">
											<input type="text" name="uname" id="uname" class="ip01 readonly1" readonly="readonly" value="<?=$_POST[uname]?>" style="width:265px;" />
										</td>
									</tr>
<?php
	if($_POST[tel_no])
	{
		$arrPhone = explode('-', rh_meta_display('phone',$_POST[tel_no]));
?>
									<tr>
										<th scope="row" class="tal"><label for="utel">연락처</label></th>
										<td class="">
											<input type="text" name="utel0" id="utel0" class="ip01 readonly1" readonly="readonly" value="<?=$arrPhone[0]?>" style="width:65px;" />
											<span>-</span>
											<input type="text" name="utel1" id="utel1" class="ip01 readonly1" readonly="readonly" value="<?=$arrPhone[1]?>" style="width:65px;" />
											<span>-</span>
											<input type="text" name="utel2" id="utel2" class="ip01 readonly1" readonly="readonly" value="<?=$arrPhone[2]?>" style="width:65px;" />
										</td>
									</tr>
<?php
	}
	else
	{
?>
									<tr>
										<th scope="row" class="tal"><label for="utel">연락처</label></th>
										<td class="">
											<input type="text" name="utel0" id="utel0" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/>
											<span>-</span>
											<input type="text" name="utel1" id="utel1" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
											<span>-</span>
											<input type="text" name="utel2" id="utel2" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
										</td>
									</tr>
<?php
	}
?>									<tr>
										<th scope="row" class="tal"><label for="u_birth">생년월일</label></th>
										<td class="">
											<input type="text" name="u_birth" id="u_birth" class="ip01 readonly1" readonly="readonly" value="<?=rh_meta_display('birth',$_POST[birthday])?>" style="width:265px;" />
										</td>
									</tr>
									<tr>
										<th scope="row" class="tal">SMS수신여부</th>
										<td class="">
											<label for="sms_yes"><input type="radio" name="sms_check" id="sms_yes" class="radio_size14" value="Y" checked/> 수신</label>
											<label for="sms_no" class="ml10"><input type="radio" name="sms_check" id="sms_no" class="radio_size14" value="N"/> 수신 거부</label>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" id="u_gender" name="u_gender" value="<?=$_POST[sex]?>">
							<input type="hidden" id="cert_ci" name="cert_ci" value="<?=$_POST[ci]?>"/>

							<!-- txtBox1 -->
							<div class="txt_box1">
								<p class="txt1">기업정보를 입력해 주시면 포인트를 이용한 충전및 할인의 다양한 혜택을 이용해 주실수 있습니다.</p>
							</div>
							<!-- //txtBox1 -->


							<div id="dvChkBeforeCert" class="join_agr_box mb10"><label for="priv_check"><input type="checkbox" name="priv_check" id="priv_check" /> <span class="fc_org1">제3자 정보제공 이용동의서에</span> 동의합니다</label></div>

							<div id="dvBtnBeforeCert" class="ta_btn_area">
								<button type="submit" class="hgbtn org01 hsize48" style="width:332px;">완료</button>
							</div>

							<?php wp_nonce_field( 'ajax-user_update-nonce', 'security' ); ?>
						</form>


						<div id="dvBtnAfterCert1" class="ta_btn_area" style="display:none; text-align:left">
							<button type="button" id="btnNewCompany" class="hgbtn grey01 hsize48" style="width:225px; margin-left:40px">기업 관리자 등록</button>
							<span style="margin-left:30px;">본인이 대표 또는 관리자로서 기업 정보를 등록/관리하고 재직중인 임직원을 관리할 수 있습니다.</span>
						</div>
						<div id="dvBtnAfterCert2" class="ta_btn_area" style="display:none; text-align:left">
							<button type="button" id="btnExistCompany" class="hgbtn org01 hsize48" style="width:225px; margin-left:40px">기업 임직원 등록</button>
							<span style="margin-left:30px;">멤버십에 가입한 기업에 재직중인 임직원으로서 기업 관리자로부터 인증 완료 후 서비스를 사용 하실 수 있습니다.</span>
						</div>

						
						
						
						<!-- 신규회원 등록 -->
						<div id="dvNewCompany" class="pl35 pr35 mt70" style="display:none;">
							<!-- 컷트라인 -->
							<div class="cut_line"><span class="ico_cut"></span></div>
							<!-- //컷트라인 -->	

							<form method="post" id="frmNewCompany" name="frmNewCompany">
								<h3 class="subj_tit1 mb10">기업 관리자 등록</h3>
								<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

								<table cellpadding="0" cellspacing="0" border="0" class="type1 mb35" summary="" style="width:100%;">
									<caption></caption>
									<colgroup>
										<col width="200" />
										<col width="" />
									</colgroup>

									<tbody>
										<tr>
											<th scope="row" class="tal"><label for="cp_type">회사유형</label></th>
											<td class="">
												<label for="cp_type1"><input type="radio" name="cp_type" id="cp_type1" class="radio_size14" value="1"/> 법인사업자</label>
												<label for="cp_type2" class="ml10"><input type="radio" name="cp_type" id="cp_type2" class="radio_size14" value="2"/> 개인사업자</label>
												<label for="cp_type3" class="ml10"><input type="radio" name="cp_type" id="cp_type3" class="radio_size14" value="3"/> 프리랜서</label>
											</td>
										</tr>
										<tr class="tr_comp">
											<th scope="row" class="tal"><label for="cp_name">회사명</label></th>
											<td class="">
												<input type="test" name="cp_name" id="cp_name" class="ip01" style="width:265px;" MaxLength="20"/>
												<span class="ta_stxt1 ml10">사업자 등록증에 기재된 회사명을 입력해 주세요</span>
											</td>
										</tr>
										<tr class="tr_comp">
											<th scope="row" class="tal">사업자번호</th>
											<td class="">
												<label for="cp_num1"><input type="text" name="cp_num1" id="cp_num1" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/></label>
												<span>-</span>
												<label for="cp_num2"><input type="text" name="cp_num2" id="cp_num2" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="2"/></label>
												<span>-</span>
												<label for="cp_num3"><input type="text" name="cp_num3" id="cp_num3" class="ip01" style="width:133px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="5"/></label>
											</td>
										</tr>
										<tr>
											<th scope="row" class="tal"><label for="ceo">대표자</label></th>
											<td class="">
												<input type="text" name="ceo_name" id="ceo_name" class="ip01" style="width:265px;"  MaxLength="10"/>
											</td>
										</tr>
										<tr>
											<th scope="row" class="tal"><label for="ceo">대표자 연락처</label></th>
											<td class="">
												<input type="text" name="ceo_hp0" id="ceo_hp0" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="3"/>
												<span>-</span>
												<input type="text" name="ceo_hp1" id="ceo_hp1" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
												<span>-</span>
												<input type="text" name="ceo_hp2" id="ceo_hp2" class="ip01" style="width:80px;" onkeydown="return jsOnlyNumber(event)" onkeyup="jsRemoveChar(event)" MaxLength="4"/>
											</td>
										</tr>
										<tr>
											<th scope="row" class="tal"><label for="sel_center">입주센터 등록여부</label></th>
											<td class="">
												<label for="cp_center_y"><input type="radio" name="cp_center_yn" id="cp_center_y" class="radio_size14" value="Y"/> 예</label>
												<label for="cp_center_n" class="ml10"><input type="radio" name="cp_center_yn" id="cp_center_n" class="radio_size14" value="N"/> 아니요</label>
											</td>
										</tr>
										<tr class="tr_CenterAddr" style="display:none">
											<th scope="row" class="tal"><label for="sel_center">입주센터선택</label></th>
											<td class="">
												<!--<select id="sel_center" name="sel_center" class="sel01" style="width:200px;">
													<option value="">선택해 주세요 </option>
												</select>-->
												<?php get_center_list();?>
												<input type="text" name="cp_addr" id="cp_addr" class="ip01" style="width:265px;" />
											</td>
										</tr>
										<tr class="tr_CompanyAddr" style="display:none">
											<th scope="row" class="tal" rowspan="2"><label for="sel_center">회사 주소</label></th>
											<td class="">
												<input type="text" name="cp_post" id="cp_post" class="ip01" style="width:120px;" MaxLength="8"/>
												<button type="button" class="hgbtn blue01 hsize38 ml30" onclick="jsPostFind();">우편번호찾기</button>
											</td>
										</tr>
										<tr class="tr_CompanyAddr" style="display:none">
											<td class="">
												<input type="text" name="cp_addr1" id="cp_addr1" class="ip01" style="width:265px;"/>
												<input type="text" name="cp_addr2" id="cp_addr2" class="ip01" style="width:265px;" />
											</td>
										</tr>
									</tbody>
								</table>
								
								<div id="dvBtnAfterCert" class="ta_btn_area">
									<button type="button" id="btnAddNewCompany" name="btnAddNewCompany" class="hgbtn grey01 hsize48" style="width:225px;">등록</button>
								</div>
							</form>
							<!-- 컷트라인 -->
							<div class="cut_line mb50"></div>
							<!-- //컷트라인 -->
						</div>
						<!-- //신규회원 등록 -->

						
						
						
						<!-- 기존회원 추가하기 -->
						<div id="dvExistCompany" class="pl35 pr35 mt70" style="display:none;">
							<!-- 컷트라인 -->
							<div class="cut_line"><span class="ico_cut"></span></div>
							<!-- //컷트라인 -->
							
							<form method="post" id="frmExistCompany" name="frmExistCompany">
								<h3 class="subj_tit1 mb10">기업 임직원 등록</h3>
								<p class="subj_txt1">추후 로그인>> 마이페이지>>회원정보수정에서도 하실수 있습니다.</p>

								<!-- 검색 박스 -->
								<div class="sh_box1">
									<table cellpadding="0" cellspacing="0" border="0" class="" summary="" style="width:100%;">
										<caption></caption>
										<colgroup>
											<col width="120px" />
											<col width="300px" />
											<col width="" />
										</colgroup>

										<tbody>
											<tr>
												<th scope="row">기업검색</th>
												<td>
													<label for="search_type_cp_name"><input type="radio" name="search_type" id="search_type_cp_name" class="radio_size14" value="company"/> 회사명</label>
													<label for="search_type_ceo_name" class="ml10"><input type="radio" name="search_type" id="search_type_ceo_name" class="radio_size14" value="ceo_name"/> 대표자명</label>
													<label for="search_type_email" class="ml10"><input type="radio" name="search_type" id="search_type_email" class="radio_size14" value="email"/> 이메일</label>
												</td>
												<td>
													<input type="text" name="search_val" id="search_val" class="ip01" style="width:300px;" />
													<input type="text" name="nPage" id="nPage" style="width:10px;" />
													<button type="button" id="btnSearchCompany" name="btnSearchCompany" onclick="jsSearchCompany(false);" class="hgbtn grey01 btn_smit">검색</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //검색 박스 -->

								<table id="tblCompanyList" cellpadding="0" cellspacing="0" border="0" class="type1 list1 mb35" summary="" style="width:100%;">
									<caption></caption>
									<colgroup>
										<col width="70px" />
										<col width="" />
										<col width="" />
										<col width="" />
										<col width="" />
										<col width="" />
										<col width="100px" />
									</colgroup>

									<thead>
										<tr>
											<th scope="col">No</th>
											<th scope="col">아이디(이메일)</th>
											<th scope="col">회사명/구분</th>
											<th scope="col">대표자명</th>
											<th scope="col">사업자번호</th>
											<th scope="col">센터구분<br /><span class="fs14">(동/호수)</span></th>
											<th scope="col">승인요청</th>
										</tr>
									</thead>

									<tbody>
									</tbody>
								</table>
							</form>

							<!-- 컷트라인 -->
							<div class="cut_line"></div>
							<!-- //컷트라인 -->
						</div>
						<!-- //기존회원 추가하기 -->



					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

<?php get_footer(); ?>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">

	$(function () {


		$("#frmCertInfo").on("submit" , function(e){			
			
			if($.trim($("form#frmCertInfo #utel0").val()) == "" || $.trim($("form#frmCertInfo #utel1").val()) == "" || $.trim($("form#frmCertInfo #utel2").val()) == "")
			{
				rh_alert("핸드폰 번호를 입력하세요.");
				return false;
			}
			else if($("form#frmCertInfo input:radio[name=sms_check]").is(":checked") == false)
			{
				rh_alert("SMS 수신여부를 체크하세요.");
				return false;
			}
			else if($("form#frmCertInfo #priv_check").is(":checked") == false)
			{
				rh_alert("제3자 정보제공 이용동의서에 동의하세요.");
				return false;
			}
			else
			{	
			
				$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_url,
				data: { 
					'action': 'rh_user_cert',
					'uid': $("form#frmCertInfo #uid").val() ,
					'uname': $("form#frmCertInfo #uname").val() ,
					'utel0': $("form#frmCertInfo #utel0").val() ,
					'utel1': $("form#frmCertInfo #utel1").val() ,
					'utel2': $("form#frmCertInfo #utel2").val() ,
					
					'u_gender': $("form#frmCertInfo #u_gender").val() ,

					'u_birth' : $("form#frmCertInfo #u_birth").val(),
					'sms_check' : $("form#frmCertInfo input:radio[name=sms_check]:checked").val(), 
					'cert_ci' : $("form#frmCertInfo #cert_ci").val(),

					'security': $('form#frmCertInfo #security').val() 
				},
				success: function(data){
						if(data.result == true)
						{
							rh_alert(data.message);
							jsShowAfterCert();
						}
						else
							rh_alert(data.message);
					}
				});

				e.preventDefault();
			}
		
		});


		$("#btnAddNewCompany").click(function(){
			if($("form#frmNewCompany input:radio[name=cp_type]").is(":checked") == false)
			{
				rh_alert("회사유형을 체크하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_type]:checked").val() != 3 && $.trim($("form#frmNewCompany #cp_name").val()) == "")
			{
				rh_alert("회사명을 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_type]:checked").val() != 3 && ($.trim($("form#frmNewCompany #cp_num1").val()) == "" || $.trim($("form#frmNewCompany #cp_num2").val()) == "" || $.trim($("form#frmNewCompany #cp_num3").val()) == ""))
			{
				rh_alert("사업자번호를 입력하세요.");
				return false;
			}
			else if($.trim($("form#frmNewCompany #ceo_name").val()) == "")
			{
				rh_alert("대표자명을 입력하세요.");
				return false;
			}
			else if($.trim($("form#frmNewCompany #ceo_hp0").val()) == "" || $.trim($("form#frmNewCompany #ceo_hp1").val()) == "" || $.trim($("form#frmNewCompany #ceo_hp2").val()) == "")
			{
				rh_alert("대표자 연락처를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]").is(":checked") == false)
			{
				rh_alert("입주센터 등록여부를 선택하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "Y" && $("form#frmNewCompany #sel_center").val() == "")
			{
				rh_alert("입주센터를 선택하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "Y" && $.trim($("form#frmNewCompany #cp_addr").val()) == "")
			{
				rh_alert("입주센터 나머지주소(동/호수)를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $("form#frmNewCompany #cp_post").val() == "")
			{
				rh_alert("회사 우편번호를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $.trim($("form#frmNewCompany #cp_addr1").val()) == "")
			{
				rh_alert("회사주소를 입력하세요.");
				return false;
			}
			else if($("form#frmNewCompany input:radio[name=cp_center_yn]:checked").val() == "N" && $.trim($("form#frmNewCompany #cp_addr2").val()) == "")
			{
				rh_alert("회사 상세주소를 입력하세요.");
				return false;
			}
			else
			{
				var formData = $("#frmNewCompany").serialize();
				$.ajax({
						type: 'POST',
						url: '../../CI/Company/funcRegisterCompany',
						dataType: 'json',
						data : formData,
						success:function(data){						
							if(data.result == "success")
							{
								rh_alert(data.html);
								$(location).attr("href", "member/my_company/");
							}
							else
							{
								rh_alert(data.html);
							}
						},
						error: function(request, status, error) {
							rh_alert(request + " " + status + " " + error);
						}                    
					});
			}
		});



		$("#btnNewCompany").click(function(){
			$("#dvExistCompany").hide();
			$("#dvNewCompany").show();
		});

		$("#btnExistCompany").click(function(){
			$("#dvNewCompany").hide();
			$("#dvExistCompany").show();

			jsSearchCompany(true);
		});

		$("input[name=cp_type]").click(function() {
			if ($(this).val() == "3")
			{
				$("#cp_name").val("");
				$("#cp_num1").val("");
				$("#cp_num2").val("");
				$("#cp_num3").val("");

				$("#cp_name").attr("disabled",true);
				$("#cp_num1").attr("disabled",true);
				$("#cp_num2").attr("disabled",true);
				$("#cp_num3").attr("disabled",true);

				$(".tr_comp").hide();

			} 
			else
			{
				$("#cp_name").attr("disabled",false);
				$("#cp_num1").attr("disabled",false);
				$("#cp_num2").attr("disabled",false);
				$("#cp_num3").attr("disabled",false);

				$(".tr_comp").show();

			} 
		});

		$("input[name=cp_center_yn]").click(function() {
			if ($(this).val() == "Y")
			{
				$(".tr_CompanyAddr").hide();
				$(".tr_CenterAddr").show();
			} 
			else
			{
				$(".tr_CenterAddr").hide();
				$(".tr_CompanyAddr").show();
			} 
		});

	});


	function jsShowAfterCert()
	{
		$("#dvChkBeforeCert").hide();
		$("#dvBtnBeforeCert").hide();
		$("#dvBtnAfterCert1").show();
		$("#dvBtnAfterCert2").show();
	}

	function jsSearchCompany(bLoadState)
	{
		if(bLoadState == false && $("form#frmExistCompany input:radio[name=search_type]").is(":checked") == false)
		{
			rh_alert("검색유형을 체크하세요.");
			return false;
		}
		else if(bLoadState == false && $.trim($("form#frmExistCompany #search_val").val()) == "")
		{
			rh_alert("검색어를 입력하세요.");
			return false;
		}
		else
		{
			$("#tblCompanyList tbody").html("");

			$.post('../../CI/Company/funcSearchCompany', 
				{
					search_type : $("form#frmExistCompany input:radio[name=search_type]:checked").val(), 
					search_val : $("form#frmExistCompany #search_val").val(), 
					nPage : $("form#frmExistCompany #nPage").val()
				}, 
				
				function(obj) {
					
					var html = "";			
					
					if(obj == null) 
					{
						return false;
					}
					else
					{
						$("#tblCompanyList tbody").append(obj.html);						
					}
			}, 'json')			
		}

	}

	function jsApplyToCompany(nCompIdx)
	{
		if(confirm("승인요청을 진행하시겠습니까?")== true)
		{
			$.ajax({
					type: 'POST',
					url: '../../CI/Company/funcApplyToCompany',
					dataType: 'json',
					data : {nCompIdx : nCompIdx},
					success:function(data){						
						if(data.result == "success")
						{
							rh_alert(data.html);
							$(location).attr("href", "member/modify/");
						}
						else
						{
							rh_alert(data.html);
						}
					},
					error: function(request, status, error) {
						rh_alert(request + " " + status + " " + error);
					}                    
				});
		}
	}

	function jsPostFind()
	{
		new daum.Postcode({
			oncomplete: function(data) {
				$("#cp_post").val(data.zonecode);
				
				if(data.userSelectedType == "R")
					$("#cp_addr1").val(data.roadAddress);
				else
					$("#cp_addr1").val(data.jibunAddress);
			}
		}).open();
	}

	
	function jsOnlyNumber(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
	
		if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 9 || keyID == 46 || keyID == 37 || keyID == 39)
		{
			return;
		}
		else
		{
			return false;
		}
	}
	
	function jsRemoveChar(event)
	{
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 || keyID == 9)
		{
			return;
		}
		else
		{
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
		}
	}

</script>