<?include("../inc/head.php");?>

	<?include("../inc/header.php");?>
	<!-- container -->
	<div id="container">
		<!-- contents -->
		<div id="contents">
			<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">본</span>인인증</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1 mb10"><span class="fc_org1">아이디</span> 찾기</h3>
					<p class="subj_txt1">주민번호로 아이디 찾기는 개인정보보호 차원에서 더 이상 지원하지 않습니다.<br />가입시 입력한 이메일과 성함을 입력해 주시면 아이디 뒤의 세자리를 * 처리해서 보여집니다.</p>

					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="umail1">이메일</label></th>
									<td class="">
										<input type="text" name="" id="umail1" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="uname">이름</label></th>
									<td class="">
										<input type="text" name="" id="uname" class="ip01" style="width:265px;" />
									</td>
								</tr>
							</tbody>
						</table>

						<div class="ta_btn_area">
							<button type="button" class="hgbtn grey01 hsize48" style="width:332px;">아이디 찾기</button>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->

				<!-- white box -->
				<div class="wh_box member">
					<h3 class="subj_tit1 mb10"><span class="fc_org1">비밀번호</span> 찾기</h3>
					<p class="subj_txt1">비밀번호는 암호화 되서 관리되며 비밀번호를 분실하신 분은 회원가입시 사용하신 메일주소로 재설정되어서 발송되어 집니다. <br>
이메일 확인후 원하시는 비밀번호로 수정해서 사용하시기 바랍니다.</p>

					<!-- article inner -->
					<article class="inner">
						<table cellpadding="0" cellspacing="0" border="0" class="type1" summary="" style="width:100%;">
							<caption></caption>
							<colgroup>
								<col width="200" />
								<col width="" />
							</colgroup>

							<tbody>
								<tr>
									<th scope="row" class="tal"><label for="pw_uid">아이디</label></th>
									<td class="">
										<input type="text" name="" id="pw_uid" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="pw_umail1">이메일</label></th>
									<td class="">
										<input type="text" name="" id="pw_umail1" class="ip01" style="width:265px;" />
									</td>
								</tr>
								<tr>
									<th scope="row" class="tal"><label for="pw_uname">이름</label></th>
									<td class="">
										<input type="text" name="" id="pw_uname" class="ip01" style="width:265px;" />
									</td>
								</tr>
							</tbody>
						</table>

						<div class="ta_btn_area">
							<button type="button" class="hgbtn org01 hsize48" style="width:332px;">비밀번호 찾기</button>
						</div>
					</article>
					<!-- //article inner -->
				</div>
				<!-- //white box -->
			</section>
			<!-- //sub_article -->
		</div>
		<!-- //contents -->
	</div>
	<!-- //container -->

<?include("../inc/footer.php");?>