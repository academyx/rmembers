<?php
/**
 * The main template file.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Masonry
 */

get_header();
?>
<style type="text/css">
	#container,#contents{margin:0 !important;padding:0 !important;}
</style>

	<!-- main_container -->
	<div id="main_container">
		<!-- 메인 비쥬얼 -->
		<div id="visual">
			<article class="inner">
				<div class="cycle-slideshow main_visual"
					data-cycle-fx=scrollHorz
					data-cycle-swipe=true
					data-cycle-timeout="5000"
					data-cycle-speed="1000"
					data-cycle-overlay-template="0{{slideNum}}"
					data-cycle-caption-template="{{cycleTitle}}"
					data-cycle-pager=".main_visual .cycle-pager"
					data-cycle-slides=".rh_slides li"
					data-cycle-prev=".main_visual .cycle-prev"
					data-cycle-next=".main_visual .cycle-next"
				>
					<ul class="rh_slides">
						<li><a href="/store/detail/?prdNo=10001"><img src="<?=get_stylesheet_directory_uri();?>/images/mv01.jpg" alt="" /></a></li>
						<li><a href="/store/detail/?prdNo=10002"><img src="<?=get_stylesheet_directory_uri();?>/images/mv02.jpg" alt="" /></a></li>
						<li><a href="#"><img src="<?=get_stylesheet_directory_uri();?>/images/mv03.jpg" alt="" /></a></li>
					</ul>
					<div class="cycle-pager"></div>

					<button type="button" class="cycle-prev">Prev</button>
					<button type="button" class="cycle-next">Next</button>
				</div>
			</article>
		</div>
		<!-- //메인 비쥬얼 -->

		<!-- main_contents -->
		<div id="main_contents">
			<article class="inner">
				<!-- 스토어 바로가기 버튼 -->
				<div class="store_dir">
					<a href="/store" class="hgbtn grey01 btn_store"><span class="ico_store">R;스토어 바로가기</span></a>
				</div>
				<!-- //스토어 바로가기 버튼 -->

				<!-- 상품 리스트 -->
				<div class="prd_list" id="main_prd_list">
					<?=get_prd_list_main()?>
				</div>
				<!-- //상품 리스트 -->
			</article>

			<!-- 서비스 안내 -->
			<div id="service_info">
				<article class="inner">
					<section class="section1">
						<h3 class="tit">서비스 안내</h3>
						<ul class="svc_list">
							<li><a href="/member/use_guide/"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico1.png" alt="" /></span>멤버십 안내</a></li>
							<!-- <li><a href="#"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico2.png" alt="" /></span>R;포인트적립</a></li> -->
							<!-- <li><a href="#"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico3.png" alt="" /></span>R;포인트사용</a></li> -->
							<li><a href="/member/benefit/"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico4.png" alt="" /></span>헤택안내</a></li>
							<li><a href="javascript:alert('준비중입니다');"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico5.png" alt="" /></span>공실관리</a></li>
							<li><a href="javascript:alert('준비중입니다');"><span><img src="<?=get_stylesheet_directory_uri();?>/images/svc_ico6.png" alt="" /></span>예약관리</a></li>
						</ul>
					</section>

					<section class="section2">
						<div class="main_use_info">
							<h3 class="tit"><span class="fc_org1">R;멤버십이</span> 처음이세요?</h3>
							<p class="txt">
								편리한 서비스 이용과<br />
								R;멤버십의 다양한 혜택으로<br />
								보다 실속있게 이용하세요
							</p>
							<a href="member/use_guide/" class="hgbtn org01">이용안내 보기 &gt;&gt;</a>
						</div>
					</section>

					<section class="section3">
						<ul class="svc_list2">
							<li><a href="/service/point-charge-step1/">R; 포인트 충전</a></li>
							<li><a href="#">아이디/비밀번호 찾기</a></li>
							<li><a href="#" onclick="pop_agreement();return false">이용약관</a></li>
							<li><a href="#" onclick="pop_privacy();return false">개인정보 취급방침</a></li>
						</ul>
					</section>
				</article>
			</div>
			<!-- //서비스 안내 -->
		</div>
		<!-- //main_contents -->
	</div>
	<!-- //main_container -->

<?php get_footer(); ?>