<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Masonry
 */
if( get_current_user_id() ){
	wp_redirect( '/member/modify/' );
	exit;
}
get_header(); 


?>


<!-- sub_article -->
			<section id="sub_article">
				<!-- 페이지 타이틀 -->
				<div class="page_tit_area">
					<h2 class="sub_tit1"><span class="fc_org1">M</span>ember Login</h2>
				</div>
				<!-- //페이지 타이틀 -->

				<!-- login_wrap -->
				<div class="login_wrap">

					<!-- login_box -->
					<div class="login_box">
						<div class="login_tab">
							<a href="#login_fm" class="on">Login</a><a href="/member/join/">Join</a>
						</div>
						<form id="login" action="/wp-login.php" method="post">
						<div class="login_tab_cts login_fm" id="login_fm">
							<ul class="">
								<li class="login_id"><label for="username"><input type="text" name="log" placeholder="아이디" id="username" class="ip01" /></label></li>
								<li class="login_pw"><label for="password"><input type="password" name="pwd" placeholder="********" id="password" class="ip01" /></label></li>
							</ul>
							<p class="txt1">* <span class="fc_org1">아이디/비밀번호</span>를 잊으셨나요?</p>

							<button type="submit" class="hgbtn org01 btn_smit"><span>Login</span></button>
							<div class="login_check">
								<!-- <label for="auto_login"><input type="checkbox" name="" id="auto_login" /> 자동 로그인</label> -->
							</div>
						</div>
						<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
						</form>
						<div class="login_line"></div>

						<!--  login_type-->
						<div class="login_type">
							<a href="#" class="btn_login_fb" onclick="fb_login();return false"><img src="<?=get_stylesheet_directory_uri();?>/images/btn_login_fb.gif" alt="페이스북 로그인" title="페이스북 로그인" /></a>

							<a href="javascript:rh_alert('준비중입니다');" class="btn_login_kakao"><img src="<?=get_stylesheet_directory_uri();?>/images/btn_login_kakao_disabled.gif" alt="카카오톡  로그인" title="카카오톡  로그인" /></a>
							
							<a href="javascript:rh_alert('준비중입니다');" class="btn_login_naver"><img src="<?=get_stylesheet_directory_uri();?>/images/btn_login_naver_disabled.gif" alt="네이버 로그인" title="네이버 로그인" /></a>
						</div>
						<!--  //login_type-->
					</div>
					<!-- //login_box -->
				</div>
				<!-- //login_box -->

				

			</section>
			<!-- //sub_article -->

		
		<script type="text/javascript">
			<!--

			 window.fbAsyncInit = function() {
				FB.init({
				  appId      : '1774012539532726',
				  oauth   : true,
     			  status  : true, // check login status
				  xfbml      : true,
				  version    : 'v2.8'
				});
				FB.AppEvents.logPageView();
			  };

			function fb_login(){

				FB.login(function(response) {

					if (response.authResponse) {
						console.log('Welcome!  Fetching your information.... ');
						//console.log(response); // dump complete info
						var access_token = response.authResponse.accessToken; //get access token
						var user_id = response.authResponse.userID; //get FB UID

						FB.api('/me?fields=id,email,name', function(response) {
							console.log('Good to see you, ' + response.name + '.' + ' Email: ' + response.email + ' Facebook ID: ' + response.id);

							console.log(response);

							var params = {'sns':'fb', 'sns_id' : response.id  , 'user_email' : response.email , 'display_name' : response.name };
							rh_redirect('/member/join/', params);

					  // you can store this data into your database             
						});

					} else {
						//user hit cancel button
						console.log('User cancelled login or did not fully authorize.');

					}
				}, {
					scope: 'public_profile,email,user_friends'
				});

			}
			
			
			
			(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/en_US/sdk.js";
				 fjs.parentNode.insertBefore(js, fjs);
			   }(document, 'script', 'facebook-jssdk'));


			//-->
		</script>

	
<?php get_footer(); ?>